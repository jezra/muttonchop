using Gst;

//are we going to display video?
#if NOVIDEO
const bool ENABLE_VIDEO = false;
#else
using Gtk;
const bool ENABLE_VIDEO = true;
#endif
private static void main( string[] args ) {
	if (args.length > 1) {
		//loop through the args
		foreach (string arg in args) {
			if (arg == "--verbose") {
				Logger.verbose = true;
			} else if (arg == "--quiet") {
				Logger.quiet = true;
			}
		}
	}
	Logger.print("Heybuddy!");
#if NOVIDEO
	Logger.print("Compiled: NOVIDEO");
#else
	//init gtk
	Gtk.init (ref args);
#endif
	//init gst
	Gst.init (ref args);
	WebServer ws = new WebServer();
	if (ws.all_good) {
		ws.run();
	}
}
