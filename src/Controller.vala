/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
using GLib;
using Json;
using Gst;
using Video;

#if ! NOVIDEO
using Gdk;
#endif

enum PlayType {
  VIDEO,
  AUDIO,
  CAST,
  STREAM
}

public errordomain ControllerError {
	NO_CONFIG
}

class Controller : GLib.Object {
  public Configuration conf;
  public UriPlayer player;
  public Catcher catcher;
  private Regex regex_path_dots;
  private PlayType current_playtype;
  private MainLoop mainloop;
  #if ! NOVIDEO
	private DisplayWindow window;
	private uint* xid;
	private Overlay overlay;
	#endif
  private string last_audio_file_path;
  public Data data;
  public Queue queue;
  private bool audio_play_random;
  private bool audio_updating;
  private bool player_looping;
  private bool overlay_set;
  private bool has_window;
  private string current_stream_id;

	/* timer stuff */
	private Timer timer;

  //we will send events
  public signal void event( string event, string data);

  public Controller() throws ControllerError {
    //compile the regex'
    try{
			regex_path_dots = new Regex("\\/*\\.\\.\\/*");
    } catch (Error e) {
			Logger.error_log(e.message);
		}

		//make the timer
		timer = new Timer();

    conf = new Configuration();
		//does the conf exist?
		if (!conf.conf_exists) {
			/* set up defaults */
			//where is the public dir? default to current dir
			conf.insert("web_server_public_dir",  GLib.Environment.get_current_dir() );
			//where are the video, casts, and audio dir?
			#if ! NOVIDEO
			conf.insert("video_dir", "~/Video");
			#endif
			conf.insert("audio_dir", "~/Music");
			conf.insert("cast_dir", "~/Casts");
			conf.insert("server_port", "2876");
			conf.insert("server_request_timeout", "5");
    }

    // should audio play randomly?
    audio_play_random = conf.get_bool_value("audio_play_random") ;
    //set the default for audio updating
    audio_updating = false;
		//looping is false
		player_looping = false;
		//there is no overlay_set just yet
		overlay_set = false;
		has_window = false;
    //make the database doohicky
    data = new Data();

		#if ! NOVIDEO
		//make the window
		window = new DisplayWindow();
		#endif
    //make the player
    player = new UriPlayer();
    //connect the prepare window signal
    player.prepare_video_window.connect( cb_prepare_video_window );
    player.finished_playing.connect( cb_finished_playing );
    player.playing_stopped.connect( cb_playing_stopped );
    player.volume_muted.connect( cb_volume_muted );

    //handle the updates
    player.update.connect( (p, o, v) => {
			event(o,v);
		});

    // are we setting the default volume of the player?
    string s_vol =  conf.get_value("start_up_volume");
    int vol;
    if (s_vol == "") {
			vol = 50;
		} else {
			vol = int.parse(s_vol);
		}
    player.volume(vol);

    //are we disabling video subtitles?
    bool disable_subs = conf.get_bool_value("video_disable_subtitles");
    player._display_subtitles = !disable_subs;

    //create the catcher
    catcher = new Catcher( data, conf.get_value("cast_dir"), conf.get_int_value("days_to_save_played_casts") );

    //create a queue
    queue = new Queue(data);

    //create the main loop
    mainloop = new MainLoop();
  }

  //we want to clean out the old casts in a semi-timely manner
  //TODO:	 create a timer to run this command every hour?
  private bool catcher_clean() {
		catcher.clean( conf.get_int_value("days_to_save_played_casts", 7) );
		return true;
	}

  public Response player_subtitle(int i) {
		player.set_subtitle(i);
		Response response = Response();
		response.text = "OK";
		response.content_type = "text/plain";
		response.status_code = StatusCode.OK;
		return response;
	}

  public Response player_status() {
		Response response = Response();
		size_t size;
		var status = player.status();
		//TODO: move playtype to a new method for getting system info
		status.play_type = current_playtype.to_string();

		//TODO: move audio_play_random to a new method for getting system info
		status.audio_play_random = audio_play_random;

		//TODO: move audio_updating to a new method for getting system info
		status.audio_updating = audio_updating;
    status.last_audio_file_path = (audio_updating) ? last_audio_file_path : null;
		response.text = Json.gobject_to_data(status, out size);
		response.content_type = "application/json";
		response.status_code = StatusCode.OK;
		return response;
	}

	public float player_volume( float vol ) {
		float new_vol = player.volume( vol );
		event("volume", @"$new_vol");
		return new_vol;
	}

	public Response player_loop( string? loop ) {
		Response response = Response();
		if (loop != null) {
		 player_looping = bool.parse(loop);
		 event("loop", @"$player_looping");
		}

		response.text = @"$player_looping";
		response.content_type = "application/json";
		response.status_code = StatusCode.OK;
		return response;
	}

	public float player_speed( float speed ) {
		float s = player.set_playback_speed( speed );
		event("player_speed", @"$s");
		return s;
	}

	public float player_volume_change( string? vol ) {
		if (vol==null) {
			return player.volume(null);
		}

		float v = player.volume(null);
		v += (float)double.parse(vol);
		if (v < 0) v=0;
		if (v > 100) v=100;
		return player_volume(v);
	}

	public bool audio_random(string? r) {
		switch(r.down()) {
			case "true":
			case "false":
				audio_play_random = bool.parse(r);
				event("audiorandom", r);
				break;
			case "toggle":
				audio_play_random = !audio_play_random;
				event("audiorandom", audio_play_random.to_string());
				break;
		}
		return audio_play_random;
	}

  private string build_path(string? r, string? dp, string? d, string? f) {
    dp = dp ?? "";
    d = d ?? "";
    f = f ?? "";
    r = r ?? "";
    return GLib.Path.build_filename(r, dp, d, f);
  }

  #if ! NOVIDEO
  //hide this method if passed the NOVIDEO flag
  public DirectoryInfo video_directory_info(string? p){
		string sp = sanitize_path(p);
		DirectoryInfo di = new DirectoryInfo( conf.get_value("video_dir"), sp);
		return di;
	}
	//remove ../ from the string
  private string sanitize_path(string? s) {
		if (s==null){
			return "";
		}
		try {
			return regex_path_dots.replace(s, s.length, 0,"");
		}catch(Error e) {
			Logger.error_log(e.message);
			return s;
		}
  }
	#endif

	public bool video_play( string f) {
		//what should the full base_dir to the file be?
		string filepath = build_path(conf.get_value("video_dir"), null, null, f);
		// does the file exist?
		if (FileUtils.test(filepath,GLib.FileTest.IS_REGULAR) ) {
			//play the file
			player_play_uri("file://"+filepath);
			//set the current playtype
			set_current_playtype( PlayType.VIDEO );
			return true;
		} else {
			return false;
		}
	}
	
	public PlayType get_current_playtype() {
		return current_playtype;
	}

  private void set_current_playtype(PlayType pt) {
		if (current_playtype != pt ) {
			current_playtype = pt;
			event("playtypechange", pt.to_string() );
		}
	}

  public AudioList audio_list(string? artist=null) {
		var list = data.get_audio_list(artist);
		return list;
	}

	public AudioFileInfo audio_track(string? id) {
		AudioFileInfo afi = new AudioFileInfo();
		if (id != null ) {
			afi = data.get_audio_file_for_id(id);
		}
		return afi;
	}

	public AudioList audio_search(string? search_string=null) {
		return data.get_audio_search(search_string);
	}

  public void handle_previous() {
    switch (current_playtype) {
      case PlayType.VIDEO :
        player.jump_percent(-10);
        break;
      case PlayType.AUDIO :
				play_audio_previous();
				break;
			case PlayType.STREAM :
				play_prev_stream();
				break;
      default:
				player.jump_percent(-10);
        break;
    }
  }

  public void handle_next() {
    switch (current_playtype) {
      case PlayType.AUDIO :
        play_audio();
        break;
      case PlayType.STREAM :
				// if something is queued, we should play next
				if (queue.length() > 0 ) {
					queue_play();
				} else {
					play_next_stream();
				}
				break;
      default:
				if(player.player_status.state!="PLAYING") {
					// if something is queued, we should play next
					if (queue.length() > 0 ) {
						queue_play();
					}
				} else {
					player.jump_percent(10);
				}
        break;
    }
  }

  public void handle_forward() {
		player.jump_percent(1);
	}

	public void handle_reverse() {
		player.jump_percent(-1);
	}

  private void cb_volume_muted( bool muted ) {
		if (muted) {
			event("muted","");
		} else {
			event("unmuted","");
		}
	}

  private void cb_playing_stopped() {
		event("playing_stopped", "");
		#if ! NOVIDEO
		//disconnect the overlay
		hide_video();
		#endif
	}

  private void cb_finished_playing() {
		event("playing_finished", "");
		cb_playing_stopped();
		//are we looping?
		if (player_looping) {
			player.play();
		} else if (queue.length() > 0) { //is there queued stuff to play?
			queue_play();
		} else if (current_playtype == PlayType.AUDIO && conf.get_bool_value("audio_continuous_play") ) {
			// are we currently playing audio?
			play_audio();
		}
	}

  private void queue_play() {
		//get the data from the queue
		string[] item = queue.pop_first();
		//emit an event
		event("queue_remove", item[0]);
		switch(item[1]) {
			case "audio" : //we are going to play audio
				if (!play_audio_for_id( item[2] ) ){
					play_audio();
				}
				break;
			case "video" :
				video_play( item[2] );
				break;
			case "cast" :
				catcher_play( item[2] );
				break;
			case "stream" :
				stream_play( item[2] );
				break;
			default:
				//do nothing
				break;

		}
	}

  private void cb_prepare_video_window(UriPlayer p, Element e) {
	  stdout.printf("prepare video");
		#if ! NOVIDEO
		Idle.add( () => {
			//do this when idle
			window.bigify();
			has_window = true;
			//window.get_window().cursor = blank_cursor;
			xid = (uint*)Gdk.X11Window.get_xid(window.get_window());
			overlay = e as Overlay;
			overlay.set_window_handle(xid);
			return false;
		});
    #endif
	}

  public void audio_update_file_data() {
		//create a tagreader
		TagReader tr = new TagReader(data);
		//tr.track_info.connect( cb_audio_file_data );
		tr.started.connect( ()=>{
			audio_updating=true;
			event("audio_update", "start");
			//start the timer
			timer.start();
		});
		tr.finished.connect( ()=>{

			/* check for removed files */
			AudioList al = data.get_audio_list();
			foreach (AudioFileInfo afi in al.get_files() ) {
				string path = afi.path;
				int id = afi.id;
				//does the file exist?
				if (!file_exists(path) ) {
					//remove the item from the database
					data.remove_audio(id);
				}
			}
			audio_updating=false;
			event("audio_update", "finish");
			//stop the timer
			timer.stop();
			ulong microseconds;
			double seconds;
			seconds = timer.elapsed (out microseconds);
      /* TODO: logger debug this info */
			//stdout.printf ("Total time: %s seconds, %lu\n", seconds.to_string (), microseconds);

		});
		tr.walk( conf.get_value("audio_dir") );
	}

  private async void cb_audio_file_data(string path, string? artist, string? title, string? album, int? track_number) {
		//update the data
		last_audio_file_path = path;
		//emit this event
		event("tagread", path);
	}

	public bool play_audio_for_id(string? id=null) {
		AudioFileInfo af = new AudioFileInfo();
		if(id!=null) {
			af = data.get_audio_file_for_id(id);
		} else {
			if ( audio_play_random ) {
				af = data.get_random_audio_file();
			} else {
				af = data.get_next_audio_file();
			}
		}
		if (af.id!=0) {
			//play the path
			player_play_uri("file://"+af.path, af.id.to_string());
			//be sure to set the play type
			set_current_playtype(PlayType.AUDIO);
			return true;
		} else {
			return false;
		}
	}

	public bool play_audio_delayed() {
		play_audio();
		return false;
	}

  public Response play_audio(string? id=null) {
		Response response;
		response = Response();
		//is there an id to play?
		if (id != null) {
			//play the audio for the id
			play_audio_for_id(id);
		} else {
			//check for some queue
			if (queue.length() > 0) {
				queue_play();
			} else {
				play_audio_for_id();
			}
		}

		response.text = "OK";
		response.content_type = "text/json";
		response.status_code= StatusCode.OK;
		return response;
	}
	
  public Response play_next_stream() {
		Response response;
		var next_id = data.get_next_stream_id(current_stream_id);
		
		response = stream_play(next_id);

		response.text = "OK";
		response.content_type = "text/json";
		response.status_code= StatusCode.OK;
		return response;
	}
	
  public Response play_prev_stream() {
		Response response;
		var prev_id = data.get_prev_stream_id(current_stream_id);
		
		response = stream_play(prev_id);

		response.text = "OK";
		response.content_type = "text/json";
		response.status_code= StatusCode.OK;
		return response;
	}

  public bool play_audio_previous() {
		AudioFileInfo afi = data.get_previous_audio_file();
		play_audio_for_id(afi.id.to_string());
		return false;
	}

  public void run() {
		//if a database was created, this could be the first run
		if (data.create_db) {
			//if there is an audio directory, we should rock a tag reader
			if( conf.get_value("audio_dir") != null ) {
				audio_update_file_data();
			}
		}
		//are going to auto_play?
		if ( conf.get_bool_value("audio_auto_play") ) {
			//are we creating the db?
			if (data.create_db) {
				//start playing in 5 seconds
				Timeout.add(1000 * 5, play_audio_delayed);
			} else {
				play_audio();
			}
		}
    mainloop.run();
  }

  /* methods for interacting with the catcher */

  public string catcher_add( string uri ) {
		EpisodeList episode_list = catcher.add_feed( uri );
		return episode_list.to_json();
	}

	public string catcher_get_update( string id ) {
		EpisodeList episode_list;
		if( id != "all") {
			episode_list = catcher.get_update( id );
		} else {
			episode_list = catcher.get_updates();
		}
		return episode_list.to_json();
	}

	public string catcher_get_episodes(string? id) {
		size_t length;
		Episode[] episodes = catcher.get_episodes(id);
		Json.Builder b = new Json.Builder();
		b.begin_array();
		foreach(Episode episode in episodes) {
			b.begin_object();
				b.set_member_name("id");
				b.add_string_value(episode.id);
				b.set_member_name("title");
				b.add_string_value(episode.title);
				b.set_member_name("description");
				b.add_string_value(episode.description);
				b.set_member_name("pubdate");
				b.add_string_value(episode.pubdate);
				b.set_member_name("played");
				b.add_boolean_value(episode.played);
				b.set_member_name("downloaded");
				b.add_boolean_value(episode.downloaded);
				b.set_member_name("exists");
				b.add_boolean_value(file_exists(episode.local_path));
			b.end_object();
		}
		b.end_array();
		//b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		return g.to_data( out length );
	}

	public string catcher_get_episode(string? id) {
		size_t length;
		Episode episode = catcher.get_episode(id);
		Json.Builder b = new Json.Builder();

		b.begin_object();
		b.set_member_name("id");
		b.add_string_value(episode.id);
		b.set_member_name("title");
		b.add_string_value(episode.title);
		b.set_member_name("description");
		b.add_string_value(episode.description);
		b.set_member_name("pubdate");
		b.add_string_value(episode.pubdate);
		b.set_member_name("played");
		b.add_boolean_value(episode.played);
		b.set_member_name("exists");
		b.add_boolean_value(file_exists(episode.local_path));
		b.set_member_name("downloaded");
		b.add_boolean_value(episode.downloaded);
		b.set_member_name("url");
		b.add_string_value(episode.url);
		b.set_member_name("feed_id");
		b.add_string_value(episode.feed_id);
		b.end_object();

		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		return g.to_data( out length );
	}


	public void catcher_download(string id_string) {
		string[] ids = id_string.split(":");
		foreach (string id in ids ) {
			catcher.download_episode(id);
		}
	}

	public void catcher_stream(string id) {
		//get the path from the db
		Episode e = data.get_episode(id);
		player_play_uri(e.url, null);
	}

	public void catcher_kill( string id ) {
		catcher.kill_download(id);
	}

	public string catcher_get_feeds() {
		size_t length;
		Feed[] feeds = catcher.get_feeds();
		//json this feeds array
		Json.Builder b = new Json.Builder();
		//b.begin_object();
		//b.set_member_name("downloads");
		b.begin_array();
		foreach(Feed feed in feeds) {
			b.begin_object();
				b.set_member_name("id");
				b.add_string_value(feed.id);
				b.set_member_name("source");
				b.add_string_value(feed.source);
				b.set_member_name("title");
				b.add_string_value(feed.title);
				b.set_member_name("description");
				b.add_string_value(feed.description);
			b.end_object();
		}
		b.end_array();
		//b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		return g.to_data( out length );
	}

	public string catcher_get_feed( string id) {
		size_t length;
		Feed feed = catcher.get_feed(id);
		//json this feeds array
		Json.Builder b = new Json.Builder();
		b.begin_object();
		b.set_member_name("id");
		b.add_string_value(feed.id);
		b.set_member_name("source");
		b.add_string_value(feed.source);
		b.set_member_name("title");
		b.add_string_value(feed.title);
		b.set_member_name("description");
		b.add_string_value(feed.description);
		b.end_object();

		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		return g.to_data( out length );
	}

	public void catcher_play(string id) {
		//get the path from the db
		Episode e = data.get_episode(id);
		//play the local path of the episode
		string path = e.local_path;
		//play the path
		player_play_uri("file://"+path, id);
		//set the file as played
		data.set_episode_played( id );
		//emit the event that the episode has been played
		event("episode_played", @"$id");
		//be sure to set the play type
		set_current_playtype(PlayType.CAST);
	}

	//
	private void hide_video() {
		#if ! NOVIDEO
			if (has_window) {
			Idle.add( () => {
			//do this when idle to avoid a race condition
				//reset the overlay
				overlay.set_window_handle((uint*)0);
				//minify the window
				window.smallify();
				has_window = false;
				return false;
			});
		}
		#endif
	}

	private void player_play_uri( string uri, string? id=null ) {
		#if ! NOVIDEO
		hide_video();
		#endif
		//log the URI
		Logger.log( uri );
		Idle.add( () => {
			//do this when idle to avoid a race condition
			player.set_uri( uri );
			player.set_id(id);
			player.play();
			return false;
		});

	}

	//get some system information
	public Response system() {
		Response res = Response();
		res.status_code = StatusCode.OK;
		res.content_type = "text/json";
		//make collect and display information about the system
		Builder b = new Builder();
    b.begin_object();
      //add the message
      //TODO: add hostname information to the output
      b.set_member_name("application");
      b.add_string_value("muttonchop" );
      b.set_member_name("hostname");
      b.add_string_value( Environment.get_host_name() );
      b.set_member_name("has_video");
      b.add_boolean_value( ENABLE_VIDEO );
    b.end_object();
    Generator g = new Generator();
    g.indent = 2;
    g.pretty = true;
    g.set_root( b.get_root() );
    size_t len;
    res.text = g.to_data( out len );
		return res;
	}

	public Response catcher_delete( Request req ) {
		//start building the response code
		Response res = Response();
		res.status_code = StatusCode.OK;
		res.content_type = "text/plain";

		string id = req.val;
		if (id == null) {
			res.status_code = StatusCode.ERROR;
			res.text = "No Feed ID was supplied";
		} else {
			catcher.remove_feed( id );
			res.text = "OK";
		}
		return res;
	}


	public string catcher_download_status() {
		DownloaderInfo[] dlis = catcher.get_download_status();
		uint64 percent;
		//create a json string for the dli information
		Json.Builder b = new Json.Builder();
		b.begin_object();
		b.set_member_name("downloads");
		b.begin_array();
		foreach(DownloaderInfo dli in dlis) {
			b.begin_object();
				b.set_member_name("id");
				b.add_string_value(dli.id);
				b.set_member_name("title");
				b.add_string_value(dli.title);
				b.set_member_name("feed-title");
				b.add_string_value(dli.feed_title);
				b.set_member_name("length");
				b.add_double_value(dli.length);
				b.set_member_name("bytes-downloaded");
				b.add_double_value(dli.bytes_downloaded);
				b.set_member_name("percent");
				if( dli.bytes_downloaded > 0 && dli.length > 0) {
					percent = dli.bytes_downloaded*100/dli.length;
				} else {
					percent = 0;
				}
				b.add_double_value(percent);
			b.end_object();
		}
		b.end_array();
		b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		size_t len;
		return g.to_data( out len );
	}

	private bool file_exists(string? path) {
		if (path==null) {
			return false;
		}
		return FileUtils.test(path, FileTest.EXISTS) ;
	}

	/* stream actions */
	public Response stream_play( string id ) {
			Response res = Response();
			string src = "";
			if ( int.parse(id) > 0 ) {
				//get the source from the db
				src = data.stream_get_source_for_id( id );
				current_stream_id = id;
			} else {
				//the "id" is the src?
				src = id;
			}
			try {
				player_play_uri( src );
				res.status_code = StatusCode.OK;
				res.content_type = "text/plain";
				res.text = "OK";
				//change the stream play state
				set_current_playtype( PlayType.STREAM );
			} catch (Error e) {
				res.status_code = StatusCode.ERROR;
				res.content_type = "text/plain";
				res.text = e.message;
			}
			return res;

	}
	public Response stream_add( Request req ) {
		Response res = Response();
		string source = req.val;
		if (source == null || source == "") {
			res.content_type = "text/plain";
			res.status_code = StatusCode.ERROR;
			res.text = "Stream URL is empty";
			return res;
		}
		string title = req.args["title"] ?? "";
		string id = data.stream_add( source, title);


		res.content_type = "text/plain";
		res.text = id;
		if (int.parse(id) < 1 ) {
			res.status_code = StatusCode.ERROR;
		} else {
			res.status_code = StatusCode.OK;
		}
		return res;
	}
	public Response stream_edit( Request req ) {
		Response res = Response();
		string id = req.val;
		string? title = req.args["title"];
		string? source = req.args["source"];
		res.text = data.stream_update(id, title, source );
		if (int.parse(res.text) < 1 ) {
			//something is wrong
			res.status_code = StatusCode.ERROR;
		} else {
			res.status_code = StatusCode.OK;
		}
		res.content_type = "text/plain";
		return res;
	}
	public Response stream_delete( Request req ) {
		Response res = Response();
		string id = req.val;
		res.text = data.stream_delete( id );
		if (int.parse(res.text) < 1 ) {
			//something is wrong
			res.status_code = StatusCode.ERROR;
		} else {
			res.status_code = StatusCode.OK;
		}
		res.content_type = "text/plain";
		return res;
	}

	public Response stream_list() {
		Response res = Response();
		res.content_type = "text/json";
		res.status_code = StatusCode.OK;
		var sl = data.stream_list();
		res.text = sl.json();
		return res;
	}

	public Response stream_info(Request req) {
		Response res = Response();
		res.content_type = "text/json";
		res.status_code = StatusCode.OK;
		var si = data.stream_info(req.val);
		size_t size;
		res.text = Json.gobject_to_data(si, out size);
		return res;
	}

	public Response queue_list() {
		Response res = Response();
		res.content_type = "text/json";
		res.status_code = StatusCode.OK;
		res.text = queue.list_as_json();
		return res;
	}

	public Response queue_delete(Request req) {
		Response res = Response();
		res.content_type = "text/json";
		res.status_code = StatusCode.OK;
		var val = queue.delete( int.parse(req.val) );
		res.text = @"$val";
		return res;
	}

	public Response queue_add(Request req) {
		Response res = Response();
		res.content_type = "text/json";
		res.status_code = StatusCode.OK;
		QueueItem qi = queue.add( req.val );
		string j = queue.item_to_json( qi );
		//emit the event data (remove carriage returns)
		event("queued", j.replace("\n",""));
		res.text = j;
		return res;
	}

}

