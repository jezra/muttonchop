/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
using GLib;
using Sqlite;

public class Data : Object {
  private Database db;
  public bool create_db;
  private int current_audio_id;
  private string current_audio_artist;
  private string current_audio_album;
  private int current_audio_track_number;
  public Data() {
		current_audio_id=0;
		int rc;
		string db_file = "muttonchop.db";
		string db_dir = Path.build_filename(Environment.get_home_dir(),".config","muttonchop");
		string db_path = Path.build_filename(db_dir,db_file);
		//open the database from the file name
		//does the file exist?
		if(! FileUtils.test( db_path, FileTest.EXISTS) ) {
			//does the db_dir exist?
			if(! FileUtils.test(db_dir, FileTest.EXISTS) ) {
				//make the dir
				DirUtils.create_with_parents(db_dir, 0700);
			}
		}    
		
		rc = Database.open_v2(db_path, out db, OPEN_READWRITE | OPEN_CREATE, null);
		if (rc != Sqlite.OK) {
			Logger.error_log("Can't open database: %d, %s".printf(rc, db.errmsg ()));
			return;
    } 
    //do we need to create the database?
    create_database();
		//we might need to do migrations
		do_migrations();
  }
  
  public string get_variable_value(string key) {
		string ek = sql_escape(key);
		string q = @"SELECT value FROM variables WHERE key ='$ek'";
		string val= single_value_query(q);
		return val;
	}
  
  public void set_variable(string key, string val) {
		string ek = sql_escape(key);
		string ev = sql_escape(val);
		string q = @"INSERT OR REPLACE INTO variables(key, value) values('$ek','$ev')";
		simple_query(q);
	}
  
  public void update_audio_file_data(string path, string artist, string title, string album, int track_number){
		var epath = sql_escape(path);
		var eartist = sql_escape(artist);
		var etitle = sql_escape(title);
		var ealbum = sql_escape(album);
		string q;
		//is this path in the db?
		int id = get_audio_id_for_path(path);
		if ( id > 0 ) {
			q = @"UPDATE audio set path ='$epath', artist='$eartist', 
				title='$etitle', album='$ealbum', track_number=$track_number,
				last_check = strftime('%s','now') WHERE id=$id";
		} else {
			q = @"INSERT INTO audio(path,artist,title,album,track_number,last_check) 
              VALUES('$epath','$eartist','$etitle','$ealbum',$track_number,strftime('%s','now'))";
		}
		//clear the statement bindings
		int rc = db.exec( q );
		if (rc != Sqlite.OK) {
			Logger.error_log(@"Error:: code: $rc\n\t query: $q\n\tid: $id");
		} 
	}
	
	public AudioList get_audio_list(string? target_artist=null) {
		AudioList al = new AudioList();
		int id=0;
		string path="";
		string artist="";
		string title="";
		string album="";
		string q;
		string where;
		int track_number=0;
		//get the info from the database
		if (target_artist!=null) {
			al.set_artist(target_artist);
			string sql_a = sql_escape(target_artist);
			if(target_artist=="UNKNOWN") {
				 where = @"WHERE (artist='$sql_a' OR artist='')";
			} else {
				where = @"WHERE artist='$sql_a'";
			}
			q = @"SELECT * FROM audio $where ORDER BY album, track_number";
		} else {
			q = "SELECT * FROM audio ORDER BY artist, album, track_number";
		}
		int rc = db.exec( q, (n, v, c) => {
			//get the parts of the result
			id = int.parse(v[0]);
			path = v[1];
			artist = v[2];
			album = v[3];
			title = v[4];
			track_number = int.parse(v[5]);
			al.add_file_info(id, path, artist, album, title, track_number);
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return al;
	}
	
	private void SQL_error(int rc, string err, string q) {
		Logger.error_log (@"SQL error: $rc, $err\n$q");
	}
	
	public AudioList get_audio_search(string? search_string=null) {
		AudioList al = new AudioList();
		//set the al message
		al.set_message( @"Search: $search_string" );
		int id=0;
		string path="";
		string artist="";
		string title="";
		string album="";
		string q;
		string where;
		int track_number=0;
		//get the info from the database
		if (search_string == null) {
			return al;
		}
		string s = sql_escape(search_string);
		where = @"WHERE (artist LIKE '%$s%' OR album LIKE '%$s%' OR title LIKE '%$s%')";
		q = @"SELECT * FROM audio $where ORDER BY artist, album, track_number";

		int rc = db.exec( q, (n, v, c) => {
			//get the parts of the result
			id = int.parse(v[0]);
			path = v[1];
			artist = v[2];
			album = v[3];
			title = v[4];
			track_number = int.parse(v[5]);
			al.add_file_info(id, path, artist, album, title, track_number);
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return al;
	}
	
	public AudioFileInfo get_random_audio_file() {
		string q = "SELECT * FROM audio ORDER BY RANDOM() LIMIT 1;";
		return single_file_query( q );
	}
	
	public AudioFileInfo get_audio_file_for_id(string id) {
		int sql_id = int.parse(id);
		string q = @"SELECT * FROM audio WHERE id=$sql_id LIMIT 1;";
		return single_file_query( q );
	}
	
	public int get_audio_id_for_path(string path) {
		string ep = sql_escape(path);
		string q = @"SELECT id FROM audio WHERE path='$ep' LIMIT 1;";
		int id = int.parse( single_value_query(q) );
		return id;
	}
	
	public string single_value_query(string q) {
		string val = "";
		int rc = db.exec( q, (n, v, c) => {
			val = v[0] ?? "";
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return val;
	}
	
	public int get_audio_last_check_for_path(string path) {
		string ep = sql_escape(path);
		string q = @"SELECT last_check FROM audio WHERE path='$ep' LIMIT 1;";
		int last_check = int.parse( single_value_query(q) );
		return last_check;
	}
	
	public AudioFileInfo get_next_audio_file() {
		AudioFileInfo afi = new AudioFileInfo();
		//is there a higher track_number from the same artist/album
		string q = @"SELECT * FROM audio
			WHERE artist=\"$current_audio_artist\"
			AND album=\"$current_audio_album\"
			AND track_number>$current_audio_track_number
			AND NOT(id=$current_audio_id) 
			ORDER BY track_number ASC LIMIT 1;";

		//run the query
		afi = single_file_query(q);
		//is there an id?
		if (afi.id != 0) {
			return afi;
		}
		//check if there is another album
		q = @"SELECT * FROM audio
			WHERE artist=\"$current_audio_artist\"
			AND album > \"$current_audio_album\"
			AND NOT(id=$current_audio_id)
			ORDER BY track_number ASC LIMIT 1;";
		//run the query
		afi = single_file_query(q);
		//is there an id?
		if (afi.id != 0) {
			return afi;
		}
		//alright, check for the next artist
		q = @"SELECT * FROM audio
			WHERE artist>\"$current_audio_artist\"
			AND NOT(id=$current_audio_id)
			ORDER BY artist, album, track_number ASC LIMIT 1;";
		
		//run the query
		afi = single_file_query(q);
		//is there an id?
		if (afi.id != 0) {
			return afi;
		}
		//if we get this far, we should play the first file in the collection
		q = @"SELECT * FROM audio
			ORDER BY artist, album, track_number ASC LIMIT 1;";
		return single_file_query( q );
	}
	
	public AudioFileInfo get_previous_audio_file() {
		AudioFileInfo afi = new AudioFileInfo();
		//is there a higher track_number from the same artist/album
		string q = @"SELECT * FROM audio
			WHERE artist=\"$current_audio_artist\"
			AND album=\"$current_audio_album\"
			AND track_number<$current_audio_track_number
			AND NOT(id=$current_audio_id) 
			ORDER BY track_number DESC LIMIT 1;";

		//run the query
		afi = single_file_query(q);
		//is there an id?
		if (afi.id != 0) {
			return afi;
		}
		//check if there is another album
		q = @"SELECT * FROM audio
			WHERE artist=\"$current_audio_artist\"
			AND album < \"$current_audio_album\"
			AND NOT(id=$current_audio_id)
			ORDER BY track_number DESC LIMIT 1;";
		//run the query
		afi = single_file_query(q);
		//is there an id?
		if (afi.id != 0) {
			return afi;
		}
		//alright, check for the next artist
		q = @"SELECT * FROM audio
			WHERE artist < \"$current_audio_artist\"
			AND NOT(id=$current_audio_id)
			ORDER BY artist, album, track_number DESC LIMIT 1;";
		
		//run the query
		afi = single_file_query(q);
		//is there an id?
		if (afi.id != 0) {
			return afi;
		}
		//if we get this far, we should play the first file in the collection
		q = @"SELECT * FROM audio
			ORDER BY artist, album, track_number ASC LIMIT 1;";
		return single_file_query( q );
	}
	
	private AudioFileInfo single_file_query( string q ) {
		AudioFileInfo af = new AudioFileInfo();
		int rc = db.exec( q, (n, v, c) => {
			af.id = int.parse(v[0]);
			af.path = v[1];
			af.artist = v[2];
			af.album = v[3];
			af.title = v[4];
			af.track_number = int.parse(v[5]);
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		} else if(af.id!=0) {
			current_audio_id = af.id;
			current_audio_artist = af.artist ?? "";
			current_audio_album = af.album ?? "";
			current_audio_track_number = af.track_number;
		}
		return af;
	}
	
	public string[] get_audio_artists() {
		string q = "SELECT artist FROM audio GROUP BY artist ORDER BY artist";
		string[] artists = {};
		int rc = db.exec( q, (n, v, c) => {
			artists += v[0];
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return artists;
	}
	
	private string sql_escape(string? str) {
		string ns = str ?? "";
		ns = ns.replace("'", "''");
		return ns;
	}
  
  
  /* catcher queries */
  public Feed[] get_feeds() {
		Feed[] feeds = {};
		string q = "SELECT * FROM feeds ORDER BY title";
		Feed feed = Feed();
		int rc = db.exec( q, (n, v, c) => {
			feed.id = v[0];
			feed.title = v[1];
			feed.description= v[2];
			feed.source= v[3];
			feed.link= v[4];
			feeds += feed;
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return feeds;
	}
	
	public Feed get_feed(string id ){
		string q = @"SELECT * FROM feeds WHERE id = $id LIMIT 1;";
		return feed_query( q );
	}
	
  public Feed get_feed_for_source(string source ) {
		string e_source = sql_escape(source);
		string q = @"SELECT * FROM feeds WHERE source = '$e_source'";
		return feed_query( q );
	}
	
	public Feed feed_query( string q ) {
		Feed feed = Feed();
		int rc = db.exec( q, (n, v, c) => {
			feed.id = v[0];
			feed.title = v[1];
			feed.description= v[2];
			feed.source= v[3];
			feed.link= v[4];
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return feed;
	}
	
	
	public void update_feed_info(Feed feed) {//int id, string title, string desc, string link) {
		string e_title = sql_escape(feed.title);
		string e_desc = sql_escape(feed.description);
		string e_link = sql_escape(feed.link);
		string id = feed.id;
		string q=@"UPDATE feeds SET title='$e_title', 
		description='$e_desc', link='$e_link', last_check=date('now')
		WHERE id=$id";
		simple_query(q);
	}
	
	public int64 create_feed(Feed feed) {
		string e_title = sql_escape(feed.title);
		string e_desc = sql_escape(feed.description);
		string e_link = sql_escape(feed.link);
		string e_source = sql_escape(feed.source);
		string q=@"INSERT INTO feeds(title, description, source, link, last_check)
		values('$e_title', '$e_desc','$e_source','$e_link', date('now'))";
		int rc = simple_query(q);
		if(rc == Sqlite.OK) {
			return db.last_insert_rowid();
		} else {
			return 0;
		} 
	}
	
	public int64 create_episode(Episode episode) {
		Episode[] e = get_episodes("url", episode.url);
		if (e.length == 0) {
			string t = sql_escape( episode.title);
			string li = sql_escape( episode.link);
			string de = sql_escape( episode.description);
			string u = sql_escape( episode.url);
			string le = sql_escape( episode.length);
			string p = sql_escape( episode.pubdate);
			//convert the pubdate to a less shitty time representation
			GLib.Time tm = GLib.Time ();
			tm.strptime (p, "%a, %d %b %Y %H:%M:%S %Z");
			
			
			string fi = episode.feed_id;
	
			string q = @"INSERT INTO episodes(title, link, description, url, 
				length, pubdate, feed_id, last_check) values('$t', '$li', '$de', '$u', '$le', '$tm', 
				'$fi', date('now') )";
			int rc = simple_query( q );
			if (rc == Sqlite.OK) {
				return db.last_insert_rowid();
			} else {
				return 0;
			}
		} else {
			//we want to update the last check time 
			string q = "UPDATE episodes SET last_check=date('now') WHERE id='"+e[0].id+"';";
			simple_query( q );
			return 0;
		}
	}
	
	public Episode[] get_played_episodes() {
		return get_episodes("played", "true");
	}
	
	public Episode[] get_episodes(string? key=null, string? val=null) {
		string where = "";
		if (key!=null && val!=null) {
			where = @"where `$key`='$val'";
		}
		string q = @"SELECT * FROM episodes $where ORDER BY pubdate DESC";
		return episodes_query( q );
	}
	
	public Episode[] episodes_query( string q ) {
		Episode[] episodes = {};
		Episode ep = Episode();
		int rc = db.exec( q, (n, v, c) => {
			if (v[0] != null) {
				ep.id = v[0];
				ep.title = v[1];
				ep.link = v[2];
				ep.description = v[3];
				ep.url = v[4];
				ep.local_path = v[5];
				ep.length = v[6];
				ep.dl_length = v[7];
				ep.server_length = v[8];
				ep.pubdate = v[9];
				ep.played = bool.parse(v[10]);
				ep.downloaded = bool.parse(v[11]);
				ep.feed_id = v[12];
			
				episodes += ep;
			}
			return Sqlite.OK;
		});
		
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return episodes;
	}
	
	public Episode[] get_unplayed_episodes() {
		string q = "SELECT * FROM episodes where downloaded='true' and played='false' ORDER BY pubdate DESC";
		return episodes_query( q );
	}
	
	public Episode get_episode(string id) {
		Episode[] eps = get_episodes("id", id);
		if (eps.length>0 ) {
			return eps[0];
		} else {
			Episode ep = Episode();
			return ep;
		}
	}
	
	public void set_episode_played(string id ) {
		string q = @"UPDATE episodes SET played='true' WHERE id=$id";
		simple_query( q );
	}
	
	public void remove_feed(string id ){
		//make the id safer
		int i = int.parse( id );
		string q = @"DELETE FROM feeds where id = $i";
		simple_query(q);
		q = @"DELETE FROM episodes where feed_id = $i";
		simple_query(q);
	}
	
	public void remove_audio(int id ){
		string q = @"DELETE FROM audio where id = $id";
		simple_query(q);
	}
	
	public int simple_query( string q ){
		int rc = db.exec(q);
		if (rc != Sqlite.OK) {
			SQL_error( rc, db.errmsg (), q);
		}
		return rc;
	}
	
	public void set_episode_local_path(string id, string path ) {
		int i = int.parse( id );
		string q = @"UPDATE episodes SET local_path='$path' WHERE id=$i";
		simple_query( q );
	}
	
	public void set_episode_downloaded(string id){
		//if this was just downloaded, it is unplayed
		string q = @"UPDATE episodes SET downloaded='true', played='false' WHERE id=$id";
		simple_query( q );
	}
	
	public void clear_episode_local_path(string id ){
		string q = @"UPDATE episodes SET local_path='' WHERE id=$id";
		simple_query( q );
	}
	
	public void clear_episode_downloaded( string id ){
		string q = @"UPDATE episodes SET downloaded='false' WHERE id=$id";
		simple_query( q );
	}
	
	public void clear_old_episodes( string date ) {
		string q = @"DELETE FROM episodes WHERE (local_path='' OR local_path IS NULL ) AND last_check<'$date'"; 
		simple_query( q );
	}
 
	/* methods for queue */
	public int64 queue_add(string type, string reference) {
		var e_type = sql_escape(type);
		var e_reference = sql_escape(reference);
		string q = @"INSERT INTO queue(play_type, reference) values('$e_type', '$e_reference');";
		int rc = db.exec(q);
		if (rc != Sqlite.OK) {
			SQL_error( rc, db.errmsg(), q);
			return 0;
		} else {
			var id = db.last_insert_rowid();
			return id;
		}
	}

	public int queue_length() {
		string q = "SELECT count(id) FROM queue";
		int count = 0;
		int rc = db.exec( q, (n, v, c) => {
			if (v[0] != null) {
				count = int.parse(v[0]);
			}
			return Sqlite.OK;
		});
		
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		} 
		return count;
	}
	
	public string[] queue_get_first() {
		string q = @"SELECT * FROM queue ORDER BY id LIMIT 1";
		string[] row ={"0","0","0"};
		int rc = db.exec( q, (n, v, c) => {
			if (v[0] != null) {
				row[0] = v[0];
				row[1] = v[1];
				row[2] = v[2];
			}
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error( rc, db.errmsg(), q);
		}
		return row;
	}
	
	public int queue_delete(int id_to_delete) {
		string q = @"DELETE FROM queue WHERE id=$id_to_delete";
		return simple_query(q);
	}
	
	public QueueItem[] queue_list() {
		//what are we returning?
		QueueItem[] QueueItems = {};
		//make the query
		string q = "SELECT * FROM queue";
		QueueItem qi = QueueItem();
		int rc = db.exec( q, (n, v, c) => {
			if (v[0] != null) {
				qi.id = v[0];
				qi.play_type = v[1];
				qi.reference = v[2];
				QueueItems += qi;
			}
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg(), q);
		}
		return QueueItems;
	}
 
  /* methods for streams */
  public string stream_add( string source, string title ) {
		var e_source = sql_escape(source);
		var e_title = sql_escape(title);
		string q = @"INSERT INTO streams(source, title) values('$e_source', '$e_title');";
		int rc = db.exec(q);
		if (rc != Sqlite.OK) {
			SQL_error( rc, db.errmsg(), q);
			return db.errmsg();
		} else {
			var id = db.last_insert_rowid();
			return id.to_string();
		}
	}
	
	public string stream_get_source_for_id( string id) {
		var e_id = sql_escape( id );
		string q = @"SELECT source FROM streams WHERE id='$e_id';";
		return single_value_query( q );
	}
	
	public string get_next_stream_id( string id) {
		var e_id = sql_escape( id );
		string q = @"SELECT id FROM streams WHERE id>'$e_id' LIMIT 1;";
		var next_id = single_value_query( q );
		/* print some info; 
		stdout.printf("get_next_stream_id: %s\n", id);
		stdout.printf("q: %s\n", q);
		stdout.printf("next_id: %s\n", next_id);
		*/
		// is there an id?
		if (next_id == "") {
			// get the min id
			q = @"SELECT min(id) FROM streams;";
			next_id = single_value_query( q );		
			/*
			stdout.printf("q: %s\n", q);
			stdout.printf("next_id: %s\n", next_id);	
			*/
		}
		return next_id;
	}
	public string get_prev_stream_id( string id) {
		var e_id = sql_escape( id );
		string q = @"SELECT id FROM streams WHERE id<'$e_id' LIMIT 1;";
		var next_id = single_value_query( q );
		/* // print some info; 
		stdout.printf("get_next_stream_id: %s\n", id);
		stdout.printf("q: %s\n", q);
		stdout.printf("next_id: %s\n", next_id);
		*/
		// is there an id?
		if (next_id == "") {
			// get the min id
			q = @"SELECT max(id) FROM streams;";
			next_id = single_value_query( q );	
			/* todo? use a verbose debug flag here	
			stdout.printf("q: %s\n", q);
			stdout.printf("next_id: %s\n", next_id);	
			*/
		}
		return next_id;
	}
	
	public string stream_update( string id, string? title, string? source) {
		string e_id = sql_escape(id);
		string values = "";
		if (title!=null) {
			values =  "title = '"+ sql_escape(title) +"'";
		} 
		if (source!=null) {
			if(title!=null) {
				values += ", ";
			} 
			values +=  "source = '"+ sql_escape(source) +"'";
		}
		string q = @"UPDATE streams SET $values where id=$e_id";
		int rc = db.exec(q);
		if (rc != Sqlite.OK) {
			SQL_error( rc, db.errmsg(), q);
			return db.errmsg();
		} else {
			return id;
		}		
	}
	
	public string stream_delete(string id) {
		string e_id = sql_escape(id);
		string q = @"DELETE FROM streams WHERE id = $e_id";
		int rc = db.exec(q);
		if (rc != Sqlite.OK) {
			SQL_error( rc, db.errmsg(), q);
			return db.errmsg();
		} else {
			return id;
		}	
	}
  
  public StreamList stream_list() {
		StreamList sl = new StreamList();
		int id=0;
		string source="";
		string title="";
		string q;
		q = "SELECT id, source, title FROM streams ORDER BY title";
		int rc = db.exec( q, (n, v, c) => {
			//get the parts of the result
			id = int.parse(v[0]);
			source = v[1];
			title = v[2];
			
			sl.add_stream_info(id, title, source);
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return sl;
	}
	
  public StreamInfo stream_info(string? val) {
		string e_val = sql_escape(val);
		StreamInfo si = new StreamInfo();
		string q;
		q = @"SELECT id, source, title FROM streams WHERE id = $e_val";
		int rc = db.exec( q, (n, v, c) => {
			//get the parts of the result
			si.id = int.parse(v[0]);
			si.source = v[1];
			si.title = v[2];
			return Sqlite.OK;
		});
		if (rc != Sqlite.OK) {
			SQL_error(rc, db.errmsg (), q);
		}
		return si;
	}
  
  
  /* database methods */
  
	private void create_database() {
		string[] queries = {};
		string errmsg = null;
		int rc;
		//the feeds query
		queries += "CREATE TABLE IF NOT EXISTS 'feeds'
			('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
			 'title' varchar(255),
			 'description' varchar(255),
			 'source' varchar(255),
			 'link' varchar(255),
			 'last_check' datetime);";
		queries+= " CREATE TABLE IF NOT EXISTS 'episodes'
			('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
			 'title' varchar(255),
			 'link' varchar(255),
			 'description' varchar(255),
			 'url' TEXT KEY,
			 'local_path' varchar(255),
			 'length' integer,
			 'dl_length' integer,
			 'server_length' integer,
			 'pubdate' datetime,
			 'played' boolean DEFAULT false,
			 'downloaded' boolean DEFAULT false,
			 'feed_id' INTEGER KEY,
			 'last_check' datetime);";
		queries += "CREATE TABLE IF NOT EXISTS 'audio'
			('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
			 'path' TEXT KEY UNIQUE,
			 'artist' varchar(255),
			 'album' varchar(255),
			 'title' varchar(255),
			 'track_number' INTEGER);";
		queries += "CREATE TABLE IF NOT EXISTS 'variables'
			('key' TEXT KEY UNIQUE,
			 'value' varchar(255) );";
		foreach( string query in queries ) {
			//run the query
			rc = db.exec( query, null, out errmsg );
			if (rc != Sqlite.OK) { 
				SQL_error( rc, db.errmsg (), query);
			}
		}
	}
	
	/*** DataBase Migrations ***/
	
	private void do_migrations() {
		//get the version
		int db_ver = int.parse(get_variable_value("database_version") );
		Logger.log(@"Database Version: $db_ver");
		switch ( db_ver ) {
			case 0 :
				migration_0();
				break;
			case 1 :
				migration_1();
				break;
			case 2 :
				migration_2();
				break;
			default:
				break;
		}
	}
	
	private void migration_0() {
		Logger.log("running database migration 0");
		string q = "ALTER TABLE audio ADD COLUMN 'last_check' integer";
		simple_query( q );
		//update the version
		set_variable("database_version", "1" );
		//perform the next migration
		migration_1();
	}
	
	private void migration_1() {
		Logger.log("running database migration 1");
		string q = "CREATE TABLE IF NOT EXISTS 'streams'
			('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
			 'title' varchar(255),
			 'source' TEXT KEY UNIQUE);";
		simple_query( q );
		//update the version
		set_variable("database_version", "2" );	
		migration_2(); 
	}
	
	private void migration_2() {
		Logger.log("running database migration 2");
		string q = "CREATE TABLE IF NOT EXISTS 'queue'
			('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
			 'play_type' varchar(20),
			 'reference' varchar(255));";
		if ( simple_query( q ) == Sqlite.OK ) {
			//update the version
			set_variable("database_version", "3" );	 
		}
	}
	
}
