/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
using GLib;
using Gst;
using Gst.PbUtils;
const int PROCESS_SPEED=50;

public class TagReader : GLib.Object{
	//public signal void track_info(string path, string artist, string title, string album, int track_number);
	public signal void finished();
	public signal void started();

	private bool idle;
	private string current_path;
	private Data data;
	private bool first_walk;
	private Discoverer discoverer;

	public TagReader(Data data) {
		this.data = data;
		first_walk = true;
		idle = true;
		current_path = "";
		//create the discoverer
		discoverer = new Discoverer(Gst.SECOND);
	}

	private bool good_extension(string f) {
		//TODO: put this list in the config
		string[] exts = {"mp3", "ogg", "aac", "m4a","flac", "oga"};
		string[] bits = f.split(".");
		if (bits.length < 2) {
			return false;
		}
		string ext = bits[bits.length-1].down();
		if( ext in exts ) {
			return true;
		} else {
			return false;
		}
	}

	public void walk(string path) {
		//strings that hold tag data
		string artist = "";
		string title = "";
		string album = "";
		uint	track_number = 0;
		unowned TagList tl;

		bool first_cleanup = false;

		//we are not idle
		idle = false;

		if (first_walk) {
			//let the controller know we started
			started();
			first_walk = false;
			first_cleanup = true;
		}
		//create a directory info
		DirectoryInfo di = new DirectoryInfo(path, null);
		//get the root from the directory info
		string root = di.get_root();
		//loop through the files in the directory
		foreach( string f in di.files ) {
			//get the full path of the file
			string file_path = @"$root/$f";
			//check the extension of the file
			if (good_extension(file_path) ) {
				//get the last_check for this file from the database
				int last_check = data.get_audio_last_check_for_path( file_path );
				//get the last modification date of the file
				uint64 last_mod = get_file_last_mod( file_path );
				//if the file has changed since it was last checked, check the file
				if ( last_check < last_mod ) {
					var shell_path = @"file://$file_path".escape("");
					try {
						var info = discoverer.discover_uri(shell_path);
						tl = info.get_tags();
						tl.get_string("artist", out artist);
						tl.get_string("title", out title);
						tl.get_string("album", out album);
						tl.get_uint("track-number", out track_number);
						artist = artist ?? "";
						title = title ?? "";
						album = album ?? "";
						if (!(track_number >= 0)) {
							track_number = 0;
						}
						data.update_audio_file_data(file_path, artist, title, album, (int)track_number);
					} catch (Error e) {
						Logger.print (@"Unable to discover_uri: file://$shell_path ::" + e.message);
					}
				} else {
					//Thread.usleep(5000);
				}
			}


		}
		//recursively walk directories
		foreach( string d in di.directories) {
			walk(@"$root/$d");
		}
		if (first_cleanup) {
			finished();
			idle = true;
		}
	}

	private uint64 get_file_last_mod(string file_path ) {
		uint64 mtime = 0;
		File f = File.new_for_path( file_path );
		try {
			FileInfo fi = f.query_info("time::modified", FileQueryInfoFlags.NONE);
			mtime = fi.get_attribute_uint64("time::modified");
		} catch {
		}
		return mtime;
	}
}
