/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
using Gst;
using GLib;

/* class: PositionDuration
 * usage: fill class with data and pass it along
 */
public class PositionDuration : GLib.Object {
  public int position{get; set;}
  public int duration{get; set;}
}

// GstPlayFlags flags of playbin2
[Flags]
private enum Gst.PlayFlag {
	VIDEO         = (1 << 0),
	AUDIO         = (1 << 1),
	TEXT          = (1 << 2),
	VIS           = (1 << 3),
	SOFT_VOLUME   = (1 << 4),
	NATIVE_AUDIO  = (1 << 5),
	NATIVE_VIDEO  = (1 << 6),
	DOWNLOAD      = (1 << 7),
	BUFFERING     = (1 << 8),
	DEINTERLACE   = (1 << 9)
}
/* class: PlayerStatus
 * usage: fill class with data and pass it along
 */
public class PlayerStatus :GLib.Object {
  public string state{get; set;}
  public float volume{get; set;}
  public string artist{get; set;}
  public string title{get; set;}
  public string album{get; set;}
  public string id{get; set;}
  public int track_number{get; set;}
  public int position{get; set;}
  public int duration{get; set;}
  public string filename{get; set;}
  public string play_type{get; set;}
  public bool audio_play_random{get; set;}
  public bool muted {get; set;}
  public bool audio_updating {get; set;}
  public string last_audio_file_path {get; set;}
  public int subtitle_count {get; set;}
  public int current_subtitle {get; set;}
  public float playback_speed {get; set;}

  public PlayerStatus() {
    clear();
  }

  public void clear() {
    state="";
    volume=0;
    artist="";
    title="";
    album="";
    track_number=0;
    position=0;
    duration=0;
    filename="";
    subtitle_count = 0;
    current_subtitle = -1;
  }

}

public class UriPlayer : GLib.Object {
	//what signals do we have
  public signal void playing_stopped();
	public signal void finished_playing();
	public signal void playing_error();
  public signal void prepare_video_window(Gst.Element videosink);
  public signal void volume_changed(double vol);
  public signal void volume_muted(bool muted);

	public signal void update( string obj, string val );

	//what variable need to be scoped to this class?
	private Element playbin;
	private Gst.Bus bus;
	private Query duration_query;
	private Query position_query;
	private int64 position;
	private int64 duration;
	private float playback_speed;
	private string track_uri;
	public PlayerStatus player_status;
	private string id;
	private bool do_subtitle_check;
	private bool displaying_subtitles;
	public bool _display_subtitles;
	//start making the player
	public UriPlayer() {
		//set some default values
		playback_speed = 1;
		duration=0;
		position=0;
		do_subtitle_check = true;
		playbin = Gst.ElementFactory.make("playbin","my-playbin");
		// add some buffer to playbin
		playbin.set_property("buffer-size", 560000);
		bus = playbin.get_bus();
		bus.add_signal_watch();
		bus.message.connect( bus_message );
		//define the queries
		duration_query = new Query.duration(Format.TIME);
		position_query = new Query.position(Format.TIME);
		//create the PlayerStatus
		player_status = new PlayerStatus();
		Timeout.add(1000, timed_positionduration );
		display_subtitles(false);
	}

	public float set_playback_speed( float speed ) {
	/*
	http://docs.gstreamer.com/display/GstSDK/Basic+tutorial+13:+Playback+speed
	*/
		update_position_duration();
		/* Create the seek event */
		if (speed > 0) {
			playbin.seek(speed, Format.TIME, SeekFlags.FLUSH | SeekFlags.ACCURATE, Gst.SeekType.SET, this.position, Gst.SeekType.NONE, 0);
			playback_speed = speed;
			update("playback_speed", @"$speed");
		}
		return playback_speed;
	}

	public void set_uri(string uri) {
		//the file will need checking
		do_subtitle_check = true;
		//stop the playbin
		playbin.set_state(Gst.State.READY);
		//take a bit of a nap
		Thread.usleep(500000);
    //clear the collected data
    player_status.clear();
		track_uri = uri;
		player_status.filename = GLib.Path.get_basename( uri );
		//emit an update
		update("newfile", player_status.filename);
		//load the new uri
		playbin.set_property("uri", uri);
		//we don't want to play yet
		playbin.set_state(Gst.State.PAUSED);
		//clear the positionduration
		update("positionduration","0:0");
	}

	~UriPlayer() {
		playbin.set_state(Gst.State.NULL);
	}

	public bool mute() {
		bool muted;
		//is the player muted?
		playbin.get("mute", out muted);
		if (!muted) {
			//the player is muted
			//unmute the player
			playbin.set("mute", true);
			playbin.get("mute", out muted);
			player_status.muted = muted;
		}
		//emit the signal
		volume_muted(muted);
		return player_status.muted == true;
	}

	public bool unmute() {
		bool muted;
		//is the player muted?
		playbin.get("mute", out muted);
		if (muted ) {
			//the player is muted
			//unmute the player
			playbin.set("mute", false);
			playbin.get("mute", out muted);
			player_status.muted = muted;
		}
		//emit the signal
		volume_muted(muted);
		return player_status.muted == false;
	}

	public void handle_tags(Gst.TagList list, string tag) {
		if(tag!=null) {
			string val="Unknown";
			switch (tag) {
				case "artist":
					list.get_string(tag, out val);
					player_status.artist = val;
					update("artist", val);
					break;
				case "title":
					list.get_string(tag, out val);
					player_status.title = val;
					update("title", val);
					break;
				case "album":
          list.get_string(tag, out val);
					player_status.album = val;
					update("album", val);
          break;
				case "channel-mode":
				case "comment":
				case "audio-codec":
					break;
				case "track-number":
          uint tn=0;
          list.get_uint(tag, out tn);
					player_status.track_number = (int)tn;
          break;
				case "bitrate":
					break;
				default:
					break;
			}
		}
	}

	private void bus_message(Gst.Message message) {
		MessageType t = (MessageType)message.type;
		switch(t) {
			case MessageType.ERROR:
				//null the pipeline
				playbin.set_state(Gst.State.NULL);
				this.playing_error();
				Logger.error_log(@"Error playing: $track_uri");
				//send a signal that playing is finished
				this.finished_playing();
				break;
			case MessageType.EOS:
				//null the pipeline
				playbin.set_state(Gst.State.NULL);
				player_status.state="STOPPED";
				//send a signal that playing is finished
				this.finished_playing();
				break;
			case	MessageType.TAG:
				Gst.TagList tag_list;
				message.parse_tag(out tag_list);
				//we need to get the key and value from the tag
				//tag_list.foreach( (l,t)=>{ handle_tags(l,t); }	);
				tag_list.foreach( handle_tags );
				break;
			case MessageType.STATE_CHANGED:
				State oldstate;
				State newstate;
				State pending;
				message.parse_state_changed (out oldstate, out newstate, out pending);
				string s_newstate = simplified_state( newstate.to_string() );
				if ( s_newstate != player_status.state ) {
					player_status.state = s_newstate;
					if (s_newstate == "PLAYING" && do_subtitle_check) {
						do_subtitle_check = false;
						check_for_subtitles();
					}
				}

				break;
			case MessageType.ELEMENT:
				var st = message.get_structure().copy();
				if (st.has_name("prepare-window-handle") ) {
					message.src.set("force-aspect-ratio", true);
					//emit a signal regarding this message src
					prepare_video_window((Element)message.src);
				}

				break;
			default:
				//what is the message type?
				break;
		}
	}

	public string simplified_state(string state) {
		return state.replace("GST_STATE_", "");
	}

	public void check_for_subtitles() {
		int n_text=0;
		int c_text=0;
		playbin.get("n-text", out n_text);
		playbin.get("current-text", out c_text);
		if (n_text > 0) {
			//are there subtitles? ... and should they be hidden?
			if (!_display_subtitles) {
				display_subtitles(false);
			}
			// get the subtitle count and current selected title
			player_status.subtitle_count = n_text;
			player_status.current_subtitle = (displaying_subtitles) ? c_text : -1;
		}
		update("subtitles", "%d:%d".printf(n_text,player_status.current_subtitle));
	}

	public void set_subtitle(int i) {
		player_status.current_subtitle = i;
		Logger.log(@"Player current_subtitle: $i");
		//if -1 we hide the subtitles
		if (i == -1) {
			if (displaying_subtitles) {
				display_subtitles(false);
			}
			return;
		}
		// if there are subtitles available to be displayed
		if ( player_status.subtitle_count > 0) {
			if (!displaying_subtitles) {
				display_subtitles(true);
			}
      //set the "current-text" to i
			playbin.set("current-text", i);
		}
	}

	public void stop() {
		playbin.set_state(Gst.State.READY);
    playing_stopped();
	}

	//when the play command is sent
	public void play() {
		playbin.set_state(Gst.State.PLAYING);
		update("playerstate", "PLAYING");
		//we may have reset the playbin speed to 1, check if playback is not 1
		if (playback_speed != 1) {
			//take a bit of a nap
			Thread.usleep(50000);
			set_playback_speed( playback_speed );
		}
	}

	public void pause() {
		playbin.set_state(Gst.State.PAUSED);
		update("playerstate", "PAUSED");
	}

	public void display_subtitles( bool display ) {
		displaying_subtitles = display;
		//get the current state of the flags
		Gst.PlayFlag flags = (Gst.PlayFlag)0; //from xnoise
		playbin.get("flags", out flags);

		if (display) {
			flags |= Gst.PlayFlag.TEXT ;
		} else {
			flags &= ~Gst.PlayFlag.TEXT;
		}
		playbin.set("flags", flags);
	}

  //set or get the volume
  public float volume(float? val) {
		double vol = 0;
		if (val != null && val>=0 && val<=100 ) {
			vol = val/100;
      playbin.set("volume",vol);
		}
		playbin.get("volume", out vol);
		val = (float)(vol*100);
		return val;
	}

  public void set_id( string? id ){
		this.id = id;
	}

  public PlayerStatus status() {
    //we will be returning a player status
		//what is the position and duration?
		PositionDuration pd = position_duration();
		player_status.position = pd.position;
		player_status.duration = pd.duration;
		//get the volume
		player_status.volume = volume(null);
		player_status.id = id;
    return player_status;
  }

  //jump jp percent relative to current playback
  public void jump_percent( int jp ) {
		//get the current position/duration
		update_position_duration();
		//what will the new offset be?
		int64 offset = (this.duration/100)*jp;
		//the new position is postion + offset
		int64 newpos = (this.position+offset);
		if (newpos < 0) {
			newpos=0;
		}
		if (newpos > this.duration){
			newpos=this.duration;
		}
		//seek to the new position
		playbin.seek(playback_speed, Format.TIME, SeekFlags.FLUSH | SeekFlags.ACCURATE, Gst.SeekType.SET, newpos, Gst.SeekType.NONE, 0);

	}

  //move playback to an absolute percentile position
  public PositionDuration absolute_percent(double? val) {
    if (val != null && val>=0 && val<=100 ) {
      update_position_duration();
      int64 newpos = (int64)(this.duration*val)/100;
      //seek to the new position
			playbin.seek(playback_speed, Format.TIME, SeekFlags.FLUSH | SeekFlags.ACCURATE, Gst.SeekType.SET, newpos, Gst.SeekType.NONE, 0);
    }
    return position_duration();
  }

	//we will need to get the current position and duration of the playbin
	public PositionDuration position_duration() {
    PositionDuration pd = new PositionDuration();
    pd.position = 0;
    pd.duration = 0;
    update_position_duration();
		// oh man, we don't need to be so accurate. using seconds should work
		pd.position = (int)(this.position/1000000000);
		pd.duration = (int)(this.duration/1000000000);

    return(pd);
  }

	public void update_position_duration() {
		bool duration_result, position_result;
		Format format = Format.TIME;
		duration_result = playbin.query(this.duration_query);
		position_result = playbin.query(this.position_query);
		if ( duration_result && position_result	) {
			this.duration_query.parse_duration(out format, out this.duration);
			this.position_query.parse_position(out format, out this.position);
		}
	}

  private bool timed_positionduration() {
		event_positionduration();
		return true;
	}

	private void event_positionduration() {
		State state;
		State pending;
		playbin.get_state(out state, out pending, 1);
		if (player_status.state == "PLAYING") {
			var pd = position_duration();
			update("positionduration", @"%d:%d".printf(pd.position,pd.duration) );
		}
	}
}
