namespace Logger {
	bool verbose = false;
	bool quiet = false;
	
	public void log (string s) {
		if ( verbose ) {
			string time = new DateTime.now_local().to_string();
			print(@"$time: $s");
		}
	}
	
	public void error_log (string s) {
		if ( verbose ) {
			string time = new DateTime.now_local().to_string();
			print(@"$time ERROR : $s");
		}
	}
	public void print (string s) {
		if (quiet) {
			log( s );
		} else {
			stdout.printf(@"$s\n");
		}
	}
}
