/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */

using Json;
public class AudioFileInfo : GLib.Object {
	public int id {get; set;}
	public string path {get; set;}
	public string artist {get; set;}
	public string album {get; set;}
	public string title {get; set;}
	public int track_number {get; set;}
	
	public AudioFileInfo() {
		//default id = 0
		id=0;
	}
}

public class AudioList {
	private string message;
	private string artist;
	private AudioFileInfo[] files;
	
	public AudioList() {
		files = {};
	}
	
	public AudioFileInfo[] get_files() {
		return files;
	}
	
	public void set_message(string s) {
		message = s;
	}
	public void set_artist(string s) {
		artist = s;
	}
	
	public void add_file_info(int id, string path, string artist, string album, string title, int track_number) {
		AudioFileInfo af = new AudioFileInfo();
		af.id = id;
		af.path = path;
		af.artist = artist;
		af.album = album;
		af.title = title;
		af.track_number = track_number;
		files += af;
	}
	
	public string json() {
		Builder b = new Builder();
		b.begin_object();
			//add the message 
			b.set_member_name("message");
			b.add_string_value( message );
			//is there an artist?
			if (artist != null) {
				b.set_member_name("artist");
				b.add_string_value( artist );
			}
			b.set_member_name("files");
			b.begin_array();
			foreach(AudioFileInfo af in files) {
				b.begin_object();
					b.set_member_name("id");
					b.add_int_value(af.id);
					b.set_member_name("path");
					b.add_string_value(af.path);
					
					b.set_member_name("artist");
					b.add_string_value(af.artist);
					b.set_member_name("album");
					b.add_string_value(af.album);
					b.set_member_name("title");
					b.add_string_value(af.title);
					b.set_member_name("track");
					b.add_int_value(af.track_number);
				b.end_object();
			}
			
			b.end_array();
		b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		size_t len;
		return g.to_data( out len );
	}
}
