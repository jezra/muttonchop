/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
using GLib;

public errordomain URLError {
	INVALID
}

public struct DownloaderInfo {
	public string id;
	public string feed_title;
	public string title;
	public uint64 length;
	public uint64 bytes_downloaded;
}

public class ParsedURL {
	public string scheme;
	public string host;
	public int port;
	public string request;
	public string query;
	public bool needs_tls;

	public ParsedURL(){
		//do we need to do any initing?
	}
	public void parse(string url) throws URLError {
		needs_tls = false;
		request = "";
		//parse that shit!
		string[] bits = url.split("://",2);
		//did it work?
		if (bits.length != 2) {
			throw new URLError.INVALID(@"$url is not a valid URL");
		}
		scheme = bits[0];
		if (scheme.down() == "http" )
			port = 80;
    if (scheme.down() == "https" ) {
			port = 443;
			needs_tls = true;
		}
		if (scheme.down() == "ftp" )
			port = 21;
		bits = bits[1].split("/");
		//get the host and port
		string hp = bits[0];
		string[] hpbits = hp.split(":");
		host = hpbits[0];
		if (hpbits.length > 1 ){
			port = int.parse(hpbits[1]);
		}
		if(bits.length > 1) {
			for (int i=1 ; i<bits.length; i++) {
				request+="/"+bits[i];
			}
		} else {
			request = "/";
		}
		//split once more to separate the query
		bits = request.split("?");
		request = bits[0];
		query = bits[1] ?? "";
	}
}

public class Header {
	Regex key_value_regex;
	public string status;
	public string Date;
	public string Server;
	public string Location;
	public uint64 Content_Length;
	public string Content_Type;
	public string Transfer_Encoding;
	public string code;
	public string version;
	public Header() {
		try {
      key_value_regex = new Regex("(?P<key>.*):\\s+(?P<value>.*)");
    } catch (RegexError err) {
      Logger.error_log("bad regex: "+err.message);
    }
	}
	public void add_value( string line ){
		if(status==null) {
			status = line;
			var line_bits = status.split(" ");
			code = line_bits[1];
			version = line_bits[0][-3:line_bits[0].length];
		} else {
			MatchInfo match_info;
			string key,val;
			if( key_value_regex.match(line,GLib.RegexMatchFlags.ANCHORED, out match_info) ) {
				key = match_info.fetch_named("key");
				val = match_info.fetch_named("value");
				switch (key) {
					case "Date" :
						Date = val;
						break;
					case "Server":
						Server = val;
						break;
					case "Location":
					case "location":
						Location = val;
						break;
					case "Content-Length":
						Content_Length = uint64.parse(val);
						break;
					case "Content-Type":
						Content_Type = val;
						break;
					case "Transfer-Encoding":
						Transfer_Encoding = val;
						break;
					default:
						break;
				}
			}
		}
	}
}

public class Downloader : Object {
	private DownloaderInfo dli;
	public uint64 length;
	public uint64 bytes_read;
	public string title;
	private int dl_length;
	private int throttler;
	private bool download_complete;
	private bool downloader_finished;
	private string url;
	private bool kill_download;
	public string local_file_path;
	private bool borked;
	private bool debug;
	public signal void finished();
	public Downloader( string url, string? local_file=null, int? throttler=0) {
		debug = false;
		dli = DownloaderInfo(){length=0};
		borked = false;
		length = 0;
		dl_length = 0;
		this.throttler = throttler;
		download_complete = false;
		downloader_finished = false;
		this.url = url;
		kill_download= false;
		if (local_file != null) {
			local_file_path = local_file;
		}
	}

	public void set_debug(bool val) {
		debug = val;
    Logger.verbose = debug;
	}

	public bool is_borked() {
		return borked;
	}

	public void kill() {
		kill_download = true;
	}

	public DownloaderInfo get_info() {
		return dli;
	}

	public void set_feed_title( string feed_title ){
		dli.feed_title = feed_title;
	}
	public void set_title( string title ){
		dli.title = title;
	}
	public void set_id( string id ){
		dli.id = id;
	}

	public void run() {
		download(this.url);
	}

	public void run_async() {
		try {
        Thread.create<void*>(thread_download, false);
    } catch (ThreadError e) {
        Logger.error_log("failed to create thread: "+e.message);
    }
	}

	void* thread_download() {
		download(this.url);
		return null;
	}

	private void download(string remote, int redirects = 0) {
		Header header = new Header();
		ParsedURL pu = new ParsedURL();

		try {
			pu.parse(remote);
			if (debug) {
        Logger.log(@"remote URL: $remote");
      }
		} catch(Error e) {
			Logger.error_log("bad parse: "+e.message);
			finished();
			return ;
		}
		// do we need to get a local file from the parsed url?
		 if ( local_file_path==null ){
			local_file_path = Path.get_basename(pu.request);
		}
		//where is this thing going?
		string partial_file = local_file_path+".partial";

		try {
			// Connect
			var client = new SocketClient ();
      SocketConnection conn;
			if (pu.needs_tls) {
				client.set_tls(true);
        //this is a gtls thingy
        TcpWrapperConnection tconn = (TcpWrapperConnection)client.connect_to_host(pu.host ,(uint16)pu.port);
        conn = (GLib.SocketConnection)tconn.get_base_io_stream();
			} else {
        conn = client.connect_to_host(pu.host ,(uint16)pu.port);
      }
			// Send HTTP GET request
			string agent = "MuttonChop/0.1.x http://www.jezra.net/project/muttonchop";
			if(pu.query!="") {
				pu.request+="?"+pu.query;
			}
			var message = "GET %s HTTP/1.1\r\n".printf(pu.request);
			message+="Host: %s\r\n".printf(pu.host);
			message+="User-Agent: %s\r\n".printf(agent);
			// we do not want any compression
			message+="Accept-Encoding: identify\r\n";
			message+="\r\n";
			conn.output_stream.write (message.data);
      if (debug) {
        Logger.log(@"$message");
      }
			// Receive response
			var response = new DataInputStream (conn.input_stream);
			size_t length=0;
			string line;
			do {
				line = response.read_line(out length);
				if (debug) {
					Logger.log(@"$line");
				}
				header.add_value(line);
			}while(length > 1 );
			//check and handle redirects
      Logger.log("header.code: %s".printf(header.code) );
			if(header.code == "301" || header.code=="302") {
        Logger.log("301 or 302");
				//how many redirects is this?
				if ( redirects>=10 ) {
					Logger.error_log("Error: Too many Redirects");
					finished();
					return;
				} else {
					Logger.log("Redirect %d: %s".printf(redirects, header.Location) );
					//download from the redirect location
					redirects ++;
					download( header.Location, redirects);
					return;
				}
			}

			var file = File.new_for_path (partial_file);
			// delete file if it already exists
			if (file.query_exists ()) {
					file.delete ();
			}
			var dos = new DataOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));
			//that's it for the headers
			bytes_read = 0;
			size_t len;
			uint8[] buffer = {};
			//is there a content length?
			if (header.Content_Length > 0) {
				//set the downloadinfo length
				dli.length = header.Content_Length;
				uint64 bytes_to_read;
				uint64 bytes_remaining;
				while ( !conn.is_closed () && bytes_read < header.Content_Length ) {
					//clear the bytes array
					bytes_remaining = header.Content_Length - bytes_read;
					bytes_to_read = (bytes_remaining	> 1028) ? 1028 : bytes_remaining;
					buffer.resize((int)bytes_to_read);
					len = response.read(buffer);
					bytes_read += len;
					dli.bytes_downloaded = bytes_read;
					//write the buffer to the stream
					long written = 0;
					while (written < len) {
							// sum of the bytes of 'text' that already have been written to the stream
							written += dos.write (buffer[written:len]);
					}

					//TODO: sleep the thread if throttling
					if (kill_download) {
						//we are finished
						downloader_finished = true;
						//close the connection
						conn.close();
						//remove the temp file
						FileUtils.remove( partial_file );
						//done return
						finished();
						return;
					}
				}
			} else {
				//can we assume this is chunked encoding? http://www.npr.org/rss/podcast.php?id=510005
				//NO. you are making an ass out of you and umption
				/* haha, You called it, *ass*
				 * https://bugs.launchpad.net/muttonchop/+bug/964988
				 */
				//is this chuncked encoding?
				if (header.Transfer_Encoding=="chunked") {
					if(debug) {
						Logger.log("Chunked Encoding");
					}
					bool read = true;
					string length_line;
					string data_line;
					int read_length;
					while ( !conn.is_closed () && read ) {
						length_line = response.read_line(out len).strip();
						read_length = hextoint(length_line);

						if(debug) {
							Logger.log(@"line length: $read_length" );
						}

						if (read_length == 0) {
							//we are finished reading
							read = false;
							break;
						}
						//clear the buffer
						buffer = {};
						//read 'read_length' bytes
						for(int i=0; i < read_length ; i++) {
							//read into the buffer
							buffer+= response.read_byte();
						}
						//write the buffer to the output stream
						dos.write(buffer);
						//finish reading the line
						data_line = response.read_line(out len);
						//TODO: sleep the thread if throttling
						if (kill_download) {
							//we are finished
							downloader_finished = true;
							//close the connection
							conn.close();
							//remove the temp file
							FileUtils.remove( partial_file );
							//done return
							finished();
							return;
						}
					}
				} else {
					//this may be 1.0 but I'm too lazy to check
					while (!response.is_closed () ) {
						//write the byte
						dos.put_byte( response.read_byte() );
					}
				}
			}
			//move the partial file to the finished file
			move_partial( partial_file, local_file_path);
			//emit the signal
			finished();

		} catch (Error e) {
				if (header.version == "1.0" && !(header.Content_Length>0)) {
					move_partial( partial_file, local_file_path);
				} else {
					Logger.error_log ("download failed: "+e.message);
				}
				finished();
				return ;
		}

	}

	private void move_partial(string old_file, string new_file) {
		File f = File.new_for_path(old_file);
		File dest = File.new_for_path(new_file);
		try {
			f.copy(dest,FileCopyFlags.OVERWRITE);
			f.delete();
		} catch (Error e) {
			Logger.error_log("failed to move finished: "+e.message);
		}
		//we are finished
		download_complete = true;
		downloader_finished = true;
	}


	// we need some helper functions
	public int hexval( string c ) {
		switch(c) {
			case "a":
			return 10;

			case "b":
			return 11;

			case "c":
			return 12;

			case "d":
			return 13;

			case "e":
			return 14;

			case "f":
			return 15;

			default:
			return int.parse(c);
		}
	}

	public int hextoint(string hex){
		string dhex = hex.down();
		string c;
		int val = 0;
		int calc;
		int mult;
		int hexlen = hex.length;
		for (int i = 0; i < hexlen ; i++) {
			int inv = (hexlen-1)-i;
			c = dhex[inv:inv+1];
			int cint = hexval(c);

			mult = 1;
			for(int j = 0 ; j < i ; j++) {
				mult *= 16;
			}

			calc = cint * mult;
			val += calc;
		}
		return val;
	}


}

