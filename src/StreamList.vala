/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */

using Json;
public class StreamInfo : GLib.Object {
	public int id {get; set;}
	public string source {get; set;}
	public string title {get; set;}
}

public class StreamList {
	private string artist;
	private StreamInfo[] streams;
	
	public StreamList() {
		streams = {};
	}

	
	public void add_stream_info(int id, string title, string source) {
		StreamInfo si = new StreamInfo();
		si.id = id;
		si.source = source;
		si.title = title;
		streams += si;
	}
	
	public string json() {
		Builder b = new Builder();
		b.begin_object();
			//is there an artist?
			if (artist != null) {
				b.set_member_name("artist");
				b.add_string_value( artist );
			}
			b.set_member_name("streams");
			b.begin_array();
			foreach(StreamInfo si in streams) {
				b.begin_object();
					b.set_member_name("id");
					b.add_int_value(si.id);
					b.set_member_name("source");
					b.add_string_value(si.source);
					b.set_member_name("title");
					b.add_string_value(si.title);
				b.end_object();
			}
			
			b.end_array();
		b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		size_t len;
		return g.to_data( out len );
	}
}
