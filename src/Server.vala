/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */

using GLib;

const string SERVER_NAME = "Muttonchop ValaSocket";
const int SSE_ARRAY_LEN = 10;
//TODO: put this in a user setting for fine tuning on a device
const int SSE_CHECK_NANOSECONDS = 500000;

namespace StatusCode {
	const string FILE_NOT_FOUND = "HTTP/1.1 404 Not Found\n";
	const string OK = "HTTP/1.1 200 OK\n";
	const string ERROR = "HTTP/1.1 500 Internal Server Error\n";
	const string MOVED_PERMANENTLY = "HTTP/1.1 301 Moved Permanently\n";
}

struct Request {
	string full_request;
	string path;
	string query;
	HashTable<string, string> args;
	string object;
	string action;
	string val;
}

struct Response {
	string status_code;
	string content_type;
	string text;
	string location;
	uint8[] data;
}

public class WebServer {
	private ThreadedSocketService tss;
	private string public_dir;
	private Regex ext_reg;
	private Controller controller;
	public bool all_good;
	private uint16 port;
	private int timeout;

	public WebServer() {
		all_good = false;
		try {
			controller = new Controller();

		}catch (Error e) {
			Logger.error_log(e.message);
			return;
		}
		port = (uint16)controller.conf.get_int_value("server_port",2876);
		timeout = controller.conf.get_int_value("server_request_timeout",5);
		public_dir = controller.conf.get_value("web_server_public_dir");
		try {
			ext_reg = new Regex("\\.(?<ext>[a-z]{2,4})$");
		} catch(Error e) {
			Logger.error_log(e.message);
		}
		//make the threaded socket service with hella possible threads
		tss = new ThreadedSocketService(150);
		//create an IPV4 InetAddress bound to no specific IP address
		InetAddress ia = new InetAddress.any(SocketFamily.IPV4);
		//create a socket address based on the netadress and set the port
		InetSocketAddress isa = new InetSocketAddress(ia, port);
		//try to add the addess to the ThreadedSocketService
		try {
			tss.add_address(isa, SocketType.STREAM, SocketProtocol.TCP, null, null);
		} catch(Error e) {
			Logger.error_log(e.message);
			return;
		}
		//connect the 'run' signal that is emitted when there is a connection
		tss.run.connect( connection_handler );
		all_good = true;

	}

	public void run() {
		//start listening
		tss.start();
		Logger.log(@"Serving on port $port");
		//run the controller with the main loop
		controller.run();
	}
	//when a request is made, handle the socket connection
	private	bool connection_handler(SocketConnection connection) {
		//connection.socket.set_blocking(true);
		connection.socket.set_timeout(timeout);
		string first_line ="";
		size_t size = 0;
		Request request = Request();
		//get data input and output strings for the connection
		DataInputStream dis = new DataInputStream(connection.input_stream);
		DataOutputStream dos = new DataOutputStream(connection.output_stream);
		//read the first line from the input stream
		try {
			first_line = dis.read_line( out size );
			request = get_request( first_line );

		} catch (Error e) {
			Logger.error_log(e.message);
		}
		//do stuff based on the request object
		Response response = Response();
		switch( request.object ) {
			case "system" :
				response = controller.system();
				break;
			case "events" :
				Logger.log("sse thread started");
				//increment the thread count
				try {
					dos.put_string("HTTP/1.1 200 OK\n");
					dos.put_string(@"Server: $SERVER_NAME\n");
					dos.put_string("Content-Type: text/event-stream\n");
					dos.put_string("Access-Control-Allow-Origin: *\n");
					dos.put_string("\n");
				} catch(Error e) {
					Logger.error_log(e.message);
					return false;
					break;
				}
				//set up some variable needed for the events
				bool looping = true;
				bool writing = false;
				//connect the controller event
				ulong handler_id = 0;
				handler_id = controller.event.connect( (c, e, d) => {
					//has the connection been broken?
					try {
						while (writing) {
							//sleep for 1/50 of a second
							Thread.usleep(50000);
						}
						//send the event string
						writing = true;
						dos.put_string(@"event: $e\n");
						dos.put_string(@"data: $d\n\n");
						dos.flush();
						writing = false;

					} catch (Error e){
						Logger.error_log( e.message);
						//disconnect the controller
						Logger.log(@"disconnect event handler $handler_id");
						controller.disconnect( handler_id );
						looping = false;
						writing = false;
					}
				});

				//start looping
				while(looping) {
					//sleep half a second
					Thread.usleep(500000);
				}
				Logger.log("sse thread ended");
				return false;
				break;
			case "stream" :
				switch (request.action) {
					case "play":
						response = controller.stream_play( request.val );
					break;
					case "add":
						response = controller.stream_add( request );
					break;
					case "edit":
						response = controller.stream_edit( request );
					break;
					case "delete":
						response = controller.stream_delete( request );
					break;
					case "list":
						response = controller.stream_list();
					break;
					case "info":
						response = controller.stream_info( request );
					break;
				}
				break;

			case "queue" :
				switch (request.action) {
					case "add":
						response = controller.queue_add( request );
					break;
					case "delete":
						response = controller.queue_delete( request );
					break;
					default :
						response = controller.queue_list();
					break;
				}
				break;
			case "audio" :
				switch (request.action) {
					case "artists":
						//TODO: let the controller handle the jsoning of the data
						string[] artists = controller.data.get_audio_artists();
						response.text = Json.gvariant_serialize_data(artists, out size);
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;
					case "play":
						response = controller.play_audio(request.val);
						break;

					case "random":
						response.text = controller.audio_random(request.val).to_string();
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "artist_tracks":
						response.text = controller.audio_list(request.val).json();
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "update":
						controller.audio_update_file_data();
						response.text = "OK";
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "search":
						response.text = controller.audio_search(request.val).json();
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "track":
						AudioFileInfo track_data = controller.audio_track(request.val);
						response.text = Json.gobject_to_data(track_data, out size);

						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					default:
						//get the default list
						response.text = controller.audio_list().json();
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;
				}
				break;
			#if ! NOVIDEO
			//disable video if passed the NOVIDEO flag
			case "video" :
				switch (request.action) {
					case "content" :
						/* we want to get the content of the video dir */
						var path = request.val;
						//get the dirs information
						DirectoryInfo di = controller.video_directory_info(path);
						//json this shit!
						response.text = Json.gobject_to_data(di, out size);
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;

						break;
					case "play" :
						var f = request.val;
						if ( controller.video_play(f) ) {
							response.text = "OK";
							response.content_type = "text/plain";
							response.status_code = StatusCode.OK;
						} else {
							response.text = "FAILURE";
							response.content_type = "text/plain";
							response.status_code = StatusCode.ERROR;
						}

						break;
					default:
						response = get_file_response(request);
						break;
					}
					break;
			#endif

			case "catcher" :
				switch (request.action) {
					case "kill" :
						var id = request.val;
						controller.catcher_kill( id );
						response.text = "OK";
						response.content_type = "text/plain";
						response.status_code= StatusCode.OK;
						break;

					case "add" :
						// we are adding a cast based on uri
						var uri = request.val;
						response.text = controller.catcher_add( uri );
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "update" :
						// we are getting an update for a feed id
						var id = request.val;
						response.text = controller.catcher_get_update( id );
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "play" :
						var f = request.val;
						controller.catcher_play(f);
						response.text = "OK";
						response.content_type = "text/plain";
						response.status_code = StatusCode.OK;
						break;

					case "episodes" :
						// we are episdes based on feed id
						var id = request.val;
						response.text = controller.catcher_get_episodes( id );
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "episode" :
						// we are episdes based on episode id
						var id = request.val;
						response.text = controller.catcher_get_episode( id );
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "feeds" :
						//get the feeds from the database
						response.text = controller.catcher_get_feeds();
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "feed" :
						var id = request.val;
						//get the feed from the database
						response.text = controller.catcher_get_feed(id);
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "download" :
						var ids = request.val;
						controller.catcher_download( ids );
						response.text = "OK";
						response.content_type = "text/plain";
						response.status_code= StatusCode.OK;
						break;

					case "stream" :
						var id = request.val;
						controller.catcher_stream( id );
						response.text = "OK";
						response.content_type = "text/plain";
						response.status_code= StatusCode.OK;
						break;

					case "download_status" :
						//get the download status from the controller
						response.text = controller.catcher_download_status();
						response.content_type = "text/json";
						response.status_code= StatusCode.OK;
						break;

					case "delete":
						response = controller.catcher_delete( request );
						break;

					default :
						response.status_code = StatusCode.FILE_NOT_FOUND;
						response.content_type = "text/plain";
						response.text = "----CATCHER METHODS----\n";
						response.text +=
						response.text += "--add a new feed\n";
						response.text += "HOST:PORT/catcher/add/URLENCODED_FEED_URL\n\n";
						response.text += "--get a list of feeds\n";
						response.text += "HOST:PORT/catcher/feeds\n\n";
						response.text += "--update all feeds\n";
						response.text += "HOST:PORT/catcher/update/all : this will return a list of all new episodes of all feeds\n\n";
						response.text += "--update a single feed\n";
						response.text += "HOST:PORT/catcher/update/FEED_ID\n\n";
						response.text += "--get a list of a feeds episodes\n";
						response.text += "HOST:PORT/catcher/episodes/FEED_ID\n\n";
						response.text += "--get info about a specific episode\n";
						response.text += "HOST:PORT/catcher/episode/EPISODE_ID\n\n";
						response.text += "--play a specific episode\n";
						response.text += "HOST:PORT/catcher/play/EPISODE_ID\n\n";
						response.text += "--download episodes\n";
						response.text += "HOST:PORT/catcher/download/EPISODE_ID1:EPISODE_ID2:EPISODE_ID3\n\n";
						response.text += "--get information about active download\n";
						response.text += "HOST:PORT/catcher/download_status\n\n";
						response.text += "--delete a feed\n";
						response.text += "HOST:PORT/catcher/delete/FEED_ID\n\n";
						break;
				}
				break;

			case "player":
				switch(request.action) {
					case "loop":
						response = controller.player_loop( request.val );
						break;

					case "subtitle":
						response = controller.player_subtitle( int.parse(request.val) );
						break;

					case "status":
						response = controller.player_status();
						break;

					case "volume":
						var new_val = controller.player_volume( (float)double.parse(request.val) );
						response.text = @"$new_val";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;

					case "speed":
						double temp_s = double.parse(request.val);
						var new_val = controller.player_speed( (float)temp_s );
						response.text = @"$new_val";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;

					case "mute":
						var new_val = controller.player.mute();
						response.text = @"$new_val";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;

					case "unmute":
						var new_val = controller.player.unmute();
						response.text = @"$new_val";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;

					case "volume_change":
						var new_val = controller.player_volume_change( request.val );
						response.text = @"$new_val";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "percent":
						var pd = controller.player.absolute_percent( double.parse(request.val) );
						response.text = Json.gobject_to_data(pd, out size);
						response.status_code = StatusCode.OK;
						response.content_type = "text/json";
						//set the sse text
						break;
					case "play" :
						// is a stream being paused?
						var type = controller.get_current_playtype().to_string();
						if (type =="PLAY_TYPE_STREAM") {
							// stop the player to reset the buffer
							controller.player.stop();
						}
						controller.player.play();
				
						response.text = "PLAY";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "pause" :
						controller.player.pause();
						response.text = "PAUSE";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "stop" :
						controller.player.stop();
						response.text = "STOP";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "next" :
						controller.handle_next();
						response.text = "NEXT";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "previous" :
						controller.handle_previous();
						response.text = "PREVIOUS";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "forward" :
						controller.handle_forward();
						response.text = "FORWARD";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					case "reverse" :
						controller.handle_reverse();
						response.text = "REVERSE";
						response.status_code = StatusCode.OK;
						response.content_type = "text/plain";
						break;
					default:
						response = get_file_response(request);
						break;
				}
				break;

			default:
				response = get_file_response(request);
				break;
		}
		//serve the response
		serve_response( response, dos );
		return false;
	}

	private void serve_response(Response response, DataOutputStream dos) {

		try {
			var data = response.data ?? response.text.data;
			//how much data is there?
			long data_length = data.length;
			dos.put_string(response.status_code);
			dos.put_string(@"Server: $SERVER_NAME\n");
			dos.put_string("Access-Control-Allow-Origin: *\n");
			//do we need to redirect?
			if (response.status_code == StatusCode.MOVED_PERMANENTLY) {
				dos.put_string("Location: %s\n".printf(response.location));
			}
			dos.put_string("Content-Type: %s\n".printf(response.content_type));
			dos.put_string(@"Content-Length: $data_length\n");
			dos.put_string("\n");//this is the end of the return headers
			/* For long string writes, a loop should be used,
			 * because sometimes not all data can be written in one run
			 *  see http://live.gnome.org/Vala/GIOSamples#Writing_Data
			 */
			long written = 0;

			if ( data_length > 0 ) {
				while (written < data_length) {
						// sum of the bytes of 'text' that already have been written to the stream
						written += dos.write (data[written:data.length]);
				}
			}
		} catch( Error e ) {
			Logger.error_log(e.message);
		}
	}

	private Response get_file_response(Request request) {
		//start the response
		Response response = Response();
		//is the request path a directory?
		string filepath = Path.build_filename(public_dir, request.path);
		if (FileUtils.test(filepath, GLib.FileTest.IS_DIR) ){
			//does the path end with /?
			if (!filepath.has_suffix("/") ) {
				string new_path = request.path + "/";
				/* redirect to the new path */
				//set the response to moved permanently
				response.status_code = StatusCode.MOVED_PERMANENTLY;
				response.content_type = "text/plain";
				response.text = "Moved Permanently";
				response.location = new_path;
				return response;
			} else {
				filepath = Path.build_filename( filepath, "index.htm");
			}
		}
		Logger.log( filepath );

		response.content_type = "text/plain";
		response.status_code = StatusCode.ERROR;
		//does the file exist?
		if (FileUtils.test(filepath, GLib.FileTest.IS_REGULAR) ) {
			//serve the file
			bool read_failed = true;
			uint8[] data = {};
			 try {
        FileUtils.get_data(filepath, out data);
        response.data = data;
        response.content_type = get_content_type( filepath );
        response.status_code = StatusCode.OK;
        read_failed = false;
      } catch (Error err) {
        response.text = err.message;
        response.status_code = StatusCode.ERROR;
        response.content_type="text/plain";
      }
		} else {
			//file not found
			response.status_code = StatusCode.FILE_NOT_FOUND;
			response.content_type = "text/plain";
			response.text = "File Not Found";
		}
		return response;
	}

	private string get_content_type(string file) {
		//get the extension
		MatchInfo mi;
		ext_reg.match( file, 0, out mi );
		var ext = mi.fetch_named("ext");
		string content_type = "text/plain";
		if (ext!=null) {
			string lower_ext = ext.down();
			switch(lower_ext) {
				case "htm":
				case "html":
					content_type="text/html";
					break;
				case "xml":
					content_type="text/xml";
					break;
				case "js":
				case "json":
					content_type="text/javascript";
					break;
				case "css":
					content_type="text/css";
					break;
				case "ico":
					content_type="image/icon";
					break;
				case "png":
					content_type="image/png";
					break;
				case "jpg":
					content_type="image/jpeg";
					break;
			}
		}
		return content_type;
	}

	// return a Request based on a portion of th line
	private Request get_request(string line) {
		Request r = Request();
		r.args = new HashTable<string, string>(str_hash, str_equal);
		//get the parts from the line
		string[] parts = line.split(" ");
		//how many parts are there?
		if (parts.length == 1) {
			return r;
		}
		//add the path to the Request
		r.full_request = parts[1];
		parts = r.full_request.split("?");
		r.path = parts[0];
		r.query = parts[1] ?? "";
		//get the object and action
		parts = r.path.split("/",4);
		if (parts.length > 1) {
			r.object = parts[1] ?? "";
		}
		if (parts.length > 2) {
			r.action = parts[2] ?? "";
		}
		if (parts.length > 3) {
			r.val = Uri.unescape_string(parts[3]) ?? "";
		}
		//split the query if it exists
		if (r.query != "") {
			string[] query_parts={};
			parts = r.query.split("&");
			foreach( string part in parts ) {
				query_parts = part.split("=");
				if (query_parts.length == 2){
					r.args[query_parts[0]] = Uri.unescape_string(query_parts[1]);
				}
			}
		}
		return r;
	}
}


