public class Catcher {
	private string local_rss_file;
	private Data db;
	private List<Downloader> downloaders;
	private string download_dir;
	private bool episodes_updating;
	private int clean_days;
	public Catcher(Data data, string dir, int clean){
		clean_days = clean;
		episodes_updating = false;
		download_dir = dir;
		downloaders = new List<Downloader>();
		db = data;
	}

	public EpisodeList get_update(string feed_id){
		//get the feed from the database
		Feed f = db.get_feed( feed_id );
		return check_feed( f.source );
	}

	public EpisodeList check_feed(string uri ) {
		//define a local rss file
		local_rss_file = Path.build_filename(Environment.get_tmp_dir(),"mc_"+simple_name(uri));
		EpisodeList episode_list = new EpisodeList();
		//remove the rss file (in case there is an old one )
		FileUtils.remove( local_rss_file);
		//download the feed to the tmp dir and parse it
		Downloader dl = new Downloader( uri, local_rss_file);
		dl.run();

		RSSXMLParser rxp = new RSSXMLParser();
		try {
			rxp.parse_file( local_rss_file );
		} catch (Error e) {

			Logger.error_log(e.message);
			episode_list.error += uri+": "+e.message+"\n";
			return episode_list;
		}

		Feed feed = db.get_feed_for_source( uri );
		//the various info might have updated
		feed.title = rxp.feed_title;
		feed.description = rxp.feed_description;
		feed.link = rxp.feed_link;
		//is there a feed id?
		if(feed.id!=null) {
			db.update_feed_info(feed);
		} else {
			feed.source = uri;
			feed.id = db.create_feed(feed).to_string();
			if (feed.id == "0"){
				//failed to add feed
				FileUtils.remove( local_rss_file);
				episode_list.error = "Failed to add feed to database";
				return episode_list;
			}
		}
		//prepare to de fuckify the pubdate
		GLib.Time tm = GLib.Time ();
		//loop through the episodes
		foreach (Episode episode in rxp.get_feed_episodes()) {
			episode.feed_id = feed.id;
			episode.feed_title = feed.title;
			int64 result = db.create_episode( episode );
			//is the episode valid?
			if (result > 0) {
				//de fuckify the pubdate... if it exists
				if(episode.pubdate!=null) {
					tm.strptime (episode.pubdate, "%a, %d %b %Y %H:%M:%S %Z");
					episode.pubdate = tm.format("%Y-%m-%d");
				}
				episode.id = result.to_string();
				episode_list.append(episode);
			}
		}
		FileUtils.remove( local_rss_file);
		return episode_list;
	}

	public EpisodeList get_updates() {
		EpisodeList episode_list = new EpisodeList();
		if (!episodes_updating ) {
			episodes_updating = true;
			foreach( Feed f in db.get_feeds() ) {
				EpisodeList feed_episodes = check_feed( f.source );
				foreach(Episode episode in feed_episodes.get_episodes()) {
					episode_list.append(episode);
				}
			}
			episodes_updating = false;
		} else {
			episode_list.error = "Update already in progress";
		}
		//clean up
		clean(clean_days);
		return episode_list;
	}

  public Feed[] get_feeds() {
		return db.get_feeds();
	}

	public Feed get_feed( string id ){
		return db.get_feed( id );
	}

	public Episode[] get_episodes(string? feed_id="0") {
		if (feed_id != "0" && feed_id!=null) {
			return db.get_episodes("feed_id", feed_id);
		} else {
			return db.get_unplayed_episodes();
		}
	}

	public void set_episode_played(string id ) {
		db.set_episode_played(id);
	}

	public Episode get_episode( string id ) {
		//get the information from the DB
		Episode episode = db.get_episode(id);
		episode.local_file_size = 0;
		if ( episode.id != null ) {
			return episode;
		} else {
			//get the local file size
			string lf = episode.local_path;
			File f = File.new_for_path( lf );
			if (lf!=null && f.query_exists() ) {
				//get some info about the file
				FileInfo fi;
				try {
					fi = f.query_info("standard::size",FileQueryInfoFlags.NONE);
					episode.local_file_size = fi.get_size();
				} catch (Error e) {
					Logger.error_log(e.message);
				}
			}
			return episode;
		}
	}

	public EpisodeList add_feed(string uri ) {
		//we just need to check the feed
		return check_feed( uri );
	}

	public void remove_feed( string feed_id ) {
		Feed feed = db.get_feed(feed_id);
		//parse the necessary paths
		var path = Path.build_filename(download_dir, simple_name(feed.title));
		recursive_delete(path);
		//remove the DB entries
		db.remove_feed( feed_id );
	}

	private void recursive_delete( string path ) {
		File f;
		//remove the files
		f= File.new_for_path(path);
		if (f.query_file_type(FileQueryInfoFlags.NONE) == FileType.DIRECTORY) {
			//loop through the children
			try {
				var enumerator = f.enumerate_children (FileAttribute.STANDARD_NAME, 0);
				FileInfo file_info;
				while ((file_info = enumerator.next_file ()) != null) {
					var p = Path.build_filename(path, file_info.get_name());
					recursive_delete(p);
				}
			} catch (Error e) {
				Logger.error_log(e.message);
			}
		}
		try {
			f.delete();
		} catch (Error e) {
			Logger.error_log(e.message);
		}
	}

	public string download_episode(string episode_id ) {
		Episode episode = db.get_episode( episode_id );
		//is this a valid episode?
		if (episode.id==null) {
			return @"invalid episode id: $episode_id";
		}
		//does a download for this id already exist?
		//loop through the Downloaders
		foreach (var dl in downloaders) {
			// do the ids match?
			if (dl.get_info().id == episode_id) {
				return @"download in progress for id: $episode_id";
			}
		}
		Feed feed = db.get_feed ( episode.feed_id );
		//parse the necessary paths
		string simple_title = simple_name(feed.title);
		string feed_dir = Path.build_filename( download_dir, simple_title);
		//if the dir doesn't exist, make it
		DirUtils.create_with_parents(feed_dir,0777);
		var file = Path.get_basename(episode.url);
		var simple_file = simple_name(file);
		var local_path = Path.build_filename(feed_dir, simple_file);
		Downloader dl = new Downloader( episode.url, local_path );
		dl.set_title( episode.title);
		dl.set_id(episode.id);
		dl.set_feed_title(feed.title);
		//connect the dl finished signal
		dl.finished.connect(cb_downloader_finished);
		//save the local_path to the episode
		db.set_episode_local_path(episode.id, local_path);
		//add the downloader to the linked list of downloaders
		downloaders.append(dl);
		//start the download
		dl.run_async();
		return @"download episode: $episode_id";
	}

	public DownloaderInfo[] get_download_status() {
		DownloaderInfo[] downloader_infos = {};
		//loop through the dls
		uint len = downloaders.length();
		for(uint i = 0 ; i < len; i++) {
			unowned Downloader? dl = downloaders.nth_data(i);
			downloader_infos += dl.get_info();
			//if percent is 100, remove the downloader
			if (dl.bytes_read == dl.length) {
				//this downloader should be removed
			}
		}
		return downloader_infos;
	}

	public void clean(int days) {
		//what is the "old" time?
		DateTime clean_time = new DateTime.now_local();
		clean_time = clean_time.add_days(-days);
		uint64 clean_unix_time = clean_time.to_unix();
		//remove old shit from the file system and the db
		Episode[] episodes = db.get_played_episodes();
		foreach(Episode e in episodes) {
			if ( e.local_path != "" && e.local_path != null ) {
				File f = File.new_for_path( e.local_path );
				//query the file for the last access time
				try {
					FileInfo fi = f.query_info("time::access", FileQueryInfoFlags.NONE);
					uint64 atime = fi.get_attribute_uint64("time::access");

					if (atime < clean_unix_time) {
						//remove the file
						f.delete();
						//update the database
						db.clear_episode_downloaded( e.id );
						db.clear_episode_local_path( e.id );
					}

				} catch (Error err ) {
					Logger.error_log("Catcher:clean: %s".printf(err.message) );
					//remove this from the episodes list
					db.clear_episode_local_path(e.id);
					db.clear_episode_local_path( e.id );
				}
			}
		}
		//format the clean date as Y-m-d
		int Y,m,d = 0;
		string sm, sd;
		clean_time.get_ymd(out Y, out m, out d);
		sm = (m<10) ? @"0$m": @"$m";
		sd = (d<10) ? @"0$d": @"$d";
		db.clear_old_episodes( @"$Y-$sm-$sd" );
	}

	//we may need to kill unresponsive downloads
	public void kill_download( string episode_id ) {
		DownloaderInfo	di;
		foreach( Downloader dl in downloaders ) {
			di = dl.get_info();
			if (di.id == episode_id ) {
				//stop the downloader
				dl.kill();
				break;
			}
		}
	}


	public string simple_name(string s) {
		string r="";
		try {
			r = /[^A-Za-z0-9_]/.replace(s, -1, 0, "_");
		} catch (Error e) {
			Logger.error_log(e.message);
		}
		return r.down();
	}

	private void cb_downloader_finished( Downloader d ) {
		//get some data from the downloader
		DownloaderInfo	di = d.get_info();
		if (di.length == di.bytes_downloaded) {
			db.set_episode_downloaded(di.id);
		}
		//get rid of the downloader
		Logger.log("remove downloader %s".printf(di.title) );
		downloaders.remove(d);
	}

	public bool print_download_status() {
		DownloaderInfo[] infos = get_download_status();
		foreach( DownloaderInfo info in infos) {
			uint64 p = info.bytes_downloaded*100/info.length;
			Logger.log("%s Percent: %f".printf(info.title, p));
		}
		return true;
	}
}

