public struct Episode {
	public string id;
	public string title;
	public string link;
	public string description;
	public string url;
	public string local_path;
	public string length;
	public string dl_length;
	public int64 local_file_size;
	public string server_length;
	public string pubdate;
	public bool played;
	public bool downloaded;
	public string feed_id;
	public string feed_title;
}

public struct Feed {
	public string id;
	public string title;
	public string description;
	public string source;
	public string link;
}
