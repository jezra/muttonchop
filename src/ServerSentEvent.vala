public class ServerSentEvent {
	public string event;
	public string data;
	public int64 time;
	
	public ServerSentEvent(string e, string d) {
		event = e;
		data = d;
		var dt = new DateTime.now_utc();
		time = dt.to_unix();
	}
}
