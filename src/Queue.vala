using Json;

public struct QueueItem {
	public string id;
	public string play_type;
	public string reference;
}


public class Queue {
	private Data db;
	
	public Queue(Data data) {
		db = data;
	}
	
	public QueueItem add(string val) {
		//split the val at the first '/'
		string[] components = val.split("/",2);		
		int64 id = db.queue_add(components[0], components[1]);
		QueueItem qi = {id.to_string(), components[0], components[1]};
		return qi;
	}
	
	public int delete(int id_to_delete) {
		int id = db.queue_delete(id_to_delete);
		return id;
	}
	
	public QueueItem[] list() {
		return db.queue_list();
	}
	
	public string list_as_json() {
		//get the queue list
		QueueItem[] items = list();
		string json = "[\n";
		foreach (QueueItem item in items) {
			json += "\t" + item_to_json( item );
			if ( item != items[ items.length-1 ]) {
				json += ",";
			}
			json += "\n";
			/*
			
			*/
		}		
		json += "]";
		return json;	
	}
	
	public string item_to_json(QueueItem qi) {
		//make a json builder
		Builder b = new Builder();
		b.begin_object();
		b.set_member_name("id");
		b.add_string_value(qi.id);
		b.set_member_name("play_type");
		b.add_string_value(qi.play_type);
		b.set_member_name("reference");
		b.add_string_value(qi.reference);
		//get some extra data based on the play_type
		switch (qi.play_type) {
			case "audio" :
				AudioFileInfo afi = db.get_audio_file_for_id(qi.reference);
				b.set_member_name("path");
				b.add_string_value(afi.path);
				b.set_member_name("artist");
				b.add_string_value(afi.artist);
				b.set_member_name("title");
				b.add_string_value(afi.title);
				b.set_member_name("album");
				b.add_string_value(afi.album);
				b.set_member_name("track_number");
				b.add_int_value(afi.track_number);
				break;
			case "cast":
				Episode episode = db.get_episode(qi.reference);
				Feed feed = db.get_feed(episode.feed_id);
				b.set_member_name("title");
				b.add_string_value(episode.title);
				b.set_member_name("feed");
				b.add_string_value(feed.title);
				break;
		}
		b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );

		size_t len;
		return g.to_data( out len );
	}
	
	public int length() {
		return db.queue_length();
	}

	public string[] pop_first() {
		//get the first queued item and then delete it from the queue
		string[] row = db.queue_get_first();
		if (row[0]!="0") {
			int id = int.parse(row[0]);
			db.queue_delete(id); 
		}
		return row;
	}
}
