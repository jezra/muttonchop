#if ! NOVIDEO
using Gtk;
using Gdk;

public class DisplayWindow : Gtk.Window {
	Gdk.Cursor blank_cursor;
	public DisplayWindow() {
		//get the screen size
		var screen = get_screen();
		//set selfs size to the screen size
		set_default_size (screen.get_width(), screen.get_height());
    //no decoration
    set_decorated(false);
    //no cursor for the window
    blank_cursor = new Gdk.Cursor(Gdk.CursorType.BLANK_CURSOR);

    //set the backgroundcolor
    Gdk.RGBA black = Gdk.RGBA();
    black.parse("black");
    override_background_color(StateFlags.NORMAL, black);

		//set the icon
		//string icon_path = GLib.Path.build_filename( Config.PACKAGE_DATA_DIR, "icon.png");
		//set_icon_from_file(icon_path);
    show_all();
		fullscreen();
		this.get_window().cursor = blank_cursor;
	}

	/* we will need to show this window */
	public void bigify() {
		show_all();
		fullscreen();
		this.get_window().cursor = blank_cursor;
	}

	public void smallify() {
		unfullscreen();
		unmaximize();
		hide();
	}
}
#endif
