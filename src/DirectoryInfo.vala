/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
public class DirectoryInfo : Object {
  public string? path {get; set;}
  public string dir_separator {get; set;}
  public string[] directories {get; set;}
  public string[] files {get; set;}
  private string root;

  public DirectoryInfo(string root, string? path ) {
		this.root = root;
		//set the dir_separator as a string
		dir_separator = "%c".printf(Path.DIR_SEPARATOR);
		path = path ?? "";
    //set some instance variables
    this.path = path;
   
    //set some vars that will get used multiple times
    string file_name;
    string file_path;
    
    //instantiate our string[]
    string[] d_assets = new string[0];
    string[] f_assets = new string[0];
    
    string search_directory = Path.build_filename(root, path);
    FileEnumerator enumerator = null;
    File d = File.new_for_path(search_directory);
    try {
      enumerator = d.enumerate_children ("standard::*", 0);
      //loop through the files in the directory
      FileInfo file_info;
      while ((file_info = enumerator.next_file()) != null) {
        file_name = file_info.get_name();
        if (file_name=="." || file_name=="..") {
          continue;
        }
        file_path = Path.build_filename(search_directory, file_name);
        if (file_info.get_file_type() == FileType.DIRECTORY) {
          //add this to the directories
          d_assets += file_name;
        } else if (file_info.get_file_type() == FileType.REGULAR) {
          f_assets += file_name;
        }
      }
    } catch (Error err) {
      Logger.error_log( err.message);
    }
    //set the directories and files
    directories = d_assets;
    files = f_assets;
  }
  public string get_root() {
		return root;
	}
  
}
