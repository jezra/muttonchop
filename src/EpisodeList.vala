using Json;

public class EpisodeList {
	public string error{get; set;}
	private Episode[] episodes;
	public EpisodeList() {
		error = "";
		episodes = {};
	}
	public void append( Episode e) {
		episodes += e;
	}
	public Episode[] get_episodes() {
		return episodes;
	}
	
	public string to_json() {
		//we need to convert the episodes data to json
		Json.Builder b = new Json.Builder();
		b.begin_object();
		//add the message 
		b.set_member_name("error");
		b.add_string_value( error );
		b.set_member_name("episodes");
		b.begin_array();
		
		foreach(Episode e in episodes) {
			b.begin_object();
				//for now, lets use the id title and feed title
				b.set_member_name("id");
				b.add_string_value(e.id);
				b.set_member_name("title");
				b.add_string_value(e.title);
				b.set_member_name("feed_title");
				b.add_string_value(e.feed_title);
				b.set_member_name("pubdate");
				b.add_string_value(e.pubdate);
			b.end_object();
		}		
		b.end_array();
		b.end_object();
		Generator g = new Generator();
		g.indent = 2;
		g.pretty = true;
		g.set_root( b.get_root() );
		size_t len;
		return g.to_data( out len );
	}
}
