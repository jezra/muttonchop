/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */

using GLib;
using Xml;

public errordomain XmlError {
	FILE_NOT_FOUND,
	XML_DOCUMENT_EMPTY,
}

public errordomain FeedError {
	MISSING_CHANNEL
}

public class RSSXMLParser : GLib.Object {	
	public string feed_title{get; set;}
	public string feed_description{get; set;}
	public string feed_link{get; set;}
	private bool debug;
	private Episode[] feed_episodes;
	
	//what happens when this class is constructed?
	construct {
		//init the parser
		//initialisation, not instantiation since the parser is a static class
		Parser.init();
	}

	public void clear() {
		feed_title="";
		feed_description="";
		feed_link ="";
		feed_episodes = {};
	}
	
  public void set_debug(bool val){
		debug = val;
  }
  
  
  private void p_debug(string s) {
		if (debug) {
			Logger.log(@"$s");
		}
	}
  
	private void parse_item_node( Xml.Node* the_node ){
		//get the children of the root node as a node
		Xml.Node* node_children = the_node->children;
		Xml.Node* iter = node_children->next;
		Episode fe = Episode();
		string iter_node_name;

		while (iter!=null) {  
		  //spaces btw. tags are also nodes, discard them
		  if (iter->type != ElementType.ELEMENT_NODE) {
			//do nothing
		  } else {
				iter_node_name = iter->name;			
				switch(iter_node_name) {
					case "title":
					fe.title = iter->get_content();
					break;
					
					case "description":
					fe.description = iter->get_content();
					break;
					
					case "enclosure":
					fe.url = iter->get_prop("url");
					fe.length = iter->get_prop("length");
					break;
					
					case "pubDate":
					fe.pubdate = iter->get_content();
					break; 
				}
		  }
		  iter=iter->next;
		}
		//add the episode to the feed episodes array
		feed_episodes+=fe;
	}

	public void parse_file(string file) throws XmlError,FeedError {
		Logger.log(@"parsing: $file"); 
		clear();
		/* parse the XML file */
		//lets do this the shitty way
		string file_text="";
		size_t length=0;
		try {
			if (! FileUtils.get_contents(file, out file_text, out length) ) {
				Logger.error_log(file+" is not readable");
				throw new XmlError.FILE_NOT_FOUND (file+" is not readable");
			}
		} catch (GLib.Error e) {
			Logger.error_log("%s: %s".printf(file, e.message));
			throw new XmlError.FILE_NOT_FOUND (file+" is not readable");
		}
		//replace >< with >\n< in the text
		file_text = file_text.replace("><", ">\n<");
		//Xml.Doc* xml_doc = Parser.read_file(file);
		Xml.Doc* xml_doc = Parser.read_memory(file_text, file_text.length);
		if (xml_doc == null) {
			Logger.error_log(file+" is not readable");
			throw new XmlError.FILE_NOT_FOUND (file+" is not readable");
		}
		
		//get the root node. notice the dereferencing operator -> instead of .
		Xml.Node* root_node = xml_doc->get_root_element();
		//p_debug("root node name: "+root_node->name);
		
		if (root_node == null) {
			//free the document manually before throwing because the garbage collector can't work on pointers
			delete xml_doc;
			Logger.error_log(file+" is empty");
			throw new XmlError.XML_DOCUMENT_EMPTY (file + " is empty");
		}
		
		//get the children of the root node as a node
		Xml.Node* root_children = root_node->children;
		//p_debug("root children name: "+root_children->name);
		
		Xml.Node* root_iter = root_children->next;
		p_debug("root_iter name: "+root_iter->name);
		
		//find the channel iter
		ulong child_count = root_node->child_element_count();
		ulong iter_count = 1;
		while (root_iter->name != "channel" ) {
			root_iter = root_iter->next;
			iter_count++;
		}
		string iter_node_name;
		//rss should be the top node
		if(root_iter->name=="channel") {
		  //get the children of the channel
			Xml.Node* iter = root_iter->children;
		  //get the first node of the channel children
		  iter = iter->next;
		  //loop through the channel children
		  while(iter != null) {  
				//spaces between tags are also nodes, discard them
				if (iter->type == ElementType.ELEMENT_NODE) {
					iter_node_name = iter->name;
					switch (iter_node_name) {
						case "title" :
						feed_title = iter->get_content();
						break;
						
						case "description" :
						feed_description = iter->get_content();
						break;
						
						case "link" :
						if(feed_link.length == 0) {
							feed_link = iter->get_content();
						}
						break;
						
						case "item" :
						parse_item_node(iter);
						break;
						
						default:
						break;
					}
				}
				iter=iter->next;
		  }
		  
		}else{
		  //the first node under root is not the channel
		  throw new FeedError.MISSING_CHANNEL (file+" is missing the 'channel' element");
		}
		//free the document
		delete xml_doc;
	}
	
	public void clean_up()
	{
		//do the parser cleanup to free the used memory
		Parser.cleanup ();
	}
	
	public Episode[] get_feed_episodes() {
		return feed_episodes;
	}

}
