/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
using GLib;

public class Configuration {
	private HashTable<string, string> conf_hash;
	private Regex key_value_regex;
	public bool conf_exists;
	public Configuration() {
    Logger.log("Searching for config file...");
		string config_file = "muttonchop.conf";
		conf_exists = false;
		conf_hash = new HashTable<string, string>(GLib.str_hash, GLib.str_equal);
    //build the regex
    try {
      key_value_regex = new Regex("(?P<key>.*)\\s+(?P<value>.*)");
    } catch (RegexError err) {
      Logger.error_log(err.message);
    }
    string[] possible_configs = {
      /*where *could* the config be?*/
      //local
      GLib.Path.build_filename(
        GLib.Environment.get_current_dir(),
        config_file
      ),
      
      //user
      GLib.Path.build_filename(
        GLib.Environment.get_home_dir(),
        ".config",
        "muttonchop",
        config_file
      ),
      @"/etc/$config_file"
    };

		foreach(string conf in possible_configs ) {
      //string conf = possible_configs[i];
      Logger.log(@"trying $conf");
			//does this conf exist?
			string contents;
			MatchInfo match_info;
			string key,val; 
      //does the file exist?
      if (FileUtils.test(conf, FileTest.EXISTS) ) {
        Logger.log(@"$conf exists");
        //try to read the file
        try {
          if( FileUtils.get_contents(conf, out contents) ) {
            Logger.log(@"reading $conf...");
            // split the file at the new line
            string[] lines = contents.split("\n");
            foreach(string line in lines) {
              //trim the line
              line = line.strip();
              //is there data on the line?
              if ( line.length>0 && line.substring(0,1)!="#") {
                //split the line at the first white space
                if( key_value_regex.match(line,GLib.RegexMatchFlags.ANCHORED, out match_info) ) {
                  key = match_info.fetch_named("key");
                  val = match_info.fetch_named("value");
                  //put the data into the HashTable
                  conf_hash.insert(key,val);
                  Logger.log(@"$key:$val");
                }
              }				
            }
            //data was found.
            conf_exists = true;
            break;
          }		
        
        } catch (GLib.FileError err ) {
					Logger.error_log(@"$conf could not be read");
        }
      }
		}
	
    if (!conf_exists) {
			Logger.error_log(@"$config_file not found");
    }
  }
	
	public int get_int_value(string key, int? default_val=null) {
		int val = int.parse( get_value(key) );
		if (val <= 0) {
			return default_val;
		} else {
			return val;
		}
	}
	
	public string get_value(string key) {
			string? val = conf_hash.lookup(key) ?? "";
			return val;
	}	
	
	public bool get_bool_value(string key) {
		return bool.parse( get_value(key) );
	}

	public void set_value(string key, string val) {
		conf_hash.set(key, val);
	}

	public void insert(string key, string val) {
		conf_hash.insert(key, val);
	}
}
