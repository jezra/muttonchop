//global variables

selected_queue_id = null;
current_selected_queue = null;

$(function() {
	//set up the queue queue buttons
  queue_delete_button = $("#queue_delete_button");
  queue_delete_button.attr('disabled',true);
  queue_delete_button.click( click_queue_delete_button );
});

function get_queue_list() {
	var url = "/queue/list";
	$.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //process the json
			process_queue_data(body);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get/queue/list: "+type);
    } 
  });
}

function process_queue_data(queue_items) {
	$("#queue_list").empty();
	var queue_count = queue_items.length;
	for (var i = 0 ; i < queue_count ; i++ ) {
		var item = queue_items[i];
		var item_text = "";
		var sli = $("<div class='faux_button'>");
		if ( not_empty_value(item['title']) && not_empty_value(item['artist']) ) {
			item_text = item['artist']+" : "+item['title'];
		} else {
			item_text = item['play_type']+" : "+item['reference'];
		}
		//handle clicks on the list_item
		sli.click( click_queue_item );
		sli.attr('queue_id',item['id']);
		sli.html(item_text);
		$("#queue_list").append(sli);		
	}
}
	
function click_delete_queue_button() {
	
}

function click_queue_item() {
	var item = this;
	selected_queue_id = $(item).attr("queue_id");
	if (current_selected_queue) {
		$(current_selected_queue).removeClass('selected_item');
	}
	current_selected_queue = item;
	$(current_selected_queue).addClass('selected_item');
	//enable the playbutton
	delete_queue_button.removeAttr('disabled');       
}
