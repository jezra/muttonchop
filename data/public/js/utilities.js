function value_or_empty_string( variable ) {
  return (variable==undefined) ? "" : variable;
}

function cleaner_time( s ) { 
	var hours=Math.floor(s/3600) ; 
	var minutes=Math.floor(s/60)-(hours*60); 
	var seconds=Math.floor(s-(hours*3600)-(minutes*60) ); 
	var ret ='';
	if (hours>0) {
		if(hours<10) {
			ret+="0"+hours+":";
		} else {
			ret+=hours+":";
		}
	} else {
		ret+="00:";
	} 
	if (minutes>0) {
		if(minutes<10) {
			ret+="0"+minutes+":";
		} else {
			ret+=minutes+":";
		}
	} else {
		ret+="00:";
	} 
	if (seconds>0) {
		if(seconds<10) {
			ret+="0"+seconds;
		} else {
			ret+=seconds;
		}
	} else {
		ret+="00";
	} 
	return ret; 
}

function cleaner_filename( name ) {
  //remove the extension if it exists
  var ret = name.replace(/\.[A-z0-9]{2,4}$/,'');
  //replace . and _ with spaces
  ret = ret.replace(/[\._]/g,' ');
  return ret; 
}

function not_empty_value(test_value, default_value) {
	if(test_value == undefined || test_value == "") {
		return default_value;
	} else {
		return test_value;
	}
}

function rsplit(string, splitter) {
	var ret = ['',''];
	var bits = string.split(splitter);
	if ( bits.length > 1 ) {
		var base = bits.pop();
		ret[0] = bits.join(splitter);
		ret[1] = base;
	}
	return ret;
}


function button_disable( button_id ){
	$(button_id).addClass("faux_button_disabled");
	$(button_id).attr('disabled', true);
}

function button_enable( button_id ) {
	$(button_id).removeClass("faux_button_disabled");
	$(button_id).removeClass("file_button_disabled");
	$(button_id).attr('disabled', false);
}

function button_hide( button_id ) {
	$(button_id).hide();
}

function button_show( button_id ) {
	$(button_id).show();
}

function button_disabled( button_id ) {
	return $(button_id).hasClass( "faux_button_disabled" );
}

function button_select( element ) {
	if ( $(element).hasClass('file_button') ){
		$(element).addClass("file_button_selected");
	} else {
		$(element).addClass("faux_button_selected");
	}
}

function button_deselect( element ) {
	if ( $(element).hasClass('file_button') ){
		$(element).removeClass("file_button_selected");
	} else {
		$(element).removeClass("faux_button_selected");
	}
}
