/* Part of MuttonChop Mobile Web UI
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
$(function(){
	//set some variables
	got_video = false;
	got_audio = false;
	got_casts = false;
	got_queue = false;
	got_streams = false;
	current_page = undefined;
	current_selected_video = undefined;
	current_selected_audio	= undefined;
	current_selected_stream = undefined;
	current_selected_episode = undefined;
	current_selected_feed_id = undefined;
	//the default cast submenu will be 'casts'
	current_cast_submenu = "casts_feeds_submenu";
	selected_queue_id = null;
	current_selected_queue = null;
	current_volume = 0;
	check_downloads = false;
	updating_volume = false;
	player_timer_speed = 2000;
	download_status_speed = 2000;
	has_event_source = false;
	first_player_status = true;
	file_info_is_scrolling = false;

	//TODO: add testing output to the server
	available_ep_data = {
  "error" : "",
  "episodes" : [
    {
      "id" : "6766",
      "title" : "The California Report",
      "feed_title" : "KQED-FM: KQED's The California Report Podcast",
      "pubdate" : "2014-04-10"
    },
    {
      "id" : "6767",
      "title" : "The California Report",
      "feed_title" : "KQED-FM: KQED's The California Report Podcast",
      "pubdate" : "2014-04-09"
    },
    {
      "id" : "6768",
      "title" : "The California Report",
      "feed_title" : "KQED-FM: KQED's The California Report Podcast",
      "pubdate" : "2014-04-08"
    },
    {
      "id" : "6769",
      "title" : "The California Report",
      "feed_title" : "KQED-FM: KQED's The California Report Podcast",
      "pubdate" : "2014-04-07"
    }
  ]
}


	//define some element vars based on id so we don't have to select over and over
	file_info = $("#file_info");
	//TODO: add the rest of the elements

	//what is the last audio function?
	last_audio_view_function = null;

	//check for video
	check_for_video_capabilities();

	//connect some the buttons we will need
	connect_buttons();
	//set up the player
	set_up_player();
	//set up the casts
	set_up_casts();
	//set up the streams
	set_up_streams();
	//create the tabs
	set_up_tabs();
	//start running!
	//set up the queue stuff
	set_up_queue();
	run();
});

function check_for_video_capabilities() {
var url = "/system";
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_system_info(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
		}
	});
}

function check_file_info_for_scrolling() {
	if (file_info_should_scroll()) {
		scroll_file_info();
	} else {
		//reset the file_info
		file_info.css('left','0px');
	}
}

function set_up_tabs() {
	tab_clickables = $("#media_tabs > button");
	tab_contents = new Array();
	for (var i = 0; i < tab_clickables.length; i++) {
		var tab = $(tab_clickables[i]);
		//what happens when a tab is clicked?
		tab.bind('click', click_tab );
		var id = tab.attr('id');
		var tab_content = $('#'+id+"_content");
		var tab_menu = $('#'+id+"_menu");
		tab_content.addClass('tab_content');
		tab_contents[i] = tab_content;
		//get the associated tab content
		tab.addClass('tab');
		if (i==0) {
			tab.addClass('selected_media_tab');
			tab_content.addClass('selected_media_content');
			tab_menu.addClass('selected_media_menu');
			current_selected_media = id;
		}
	}
}

function check_config() {
	var url = "/system";
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			check_config_confirmed();
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			check_config_failed();
		}
	});
}

function check_config_confirmed() {
	//enable the config elements
		//disable the config elements
	$("#config_host").disable = false;
	$("#config_port").disable = false;
	button_enable("#button_config_update");
	//clear any config warning
	$("#config_warning").text("");
	//the config worked!
	//start running
	run();
	switch_to_page("#Player");

}

function check_config_failed() {
	//bummer dude
	var msg = "Failed to connected to "+host+":"+port;
	$("#config_warning").text( msg );
	switch_to_page("#Config");
}

function run() {
	//TODO: use SSE for download status
	timer_get_download_status();

	//can we do SSE?
	if (window.EventSource) {
		has_event_source = true;
		//get the status once
		get_player_status();
		//handle the event source
		set_up_sse();
	} else {
		// bummer dude, use the polling
		timer_get_player_status();
	}
	//load up the default video data
	get_video_content();
}

function set_up_sse() {
	//what is the source of our events?
	var url = "/events";

	var source = new EventSource(url);
	//first things first! Handle the source closing
	source.addEventListener('error', function(e) {
		// event source has failed
		console.log('event source has broken');
		// try to connect again in a bit
		setTimeout(set_up_sse(), 1000);
		if (e.readyState == EventSource.CLOSED) {
			// Connection was closed.
			// reconnect?


		}
	},false);


	//handle position duration
	source.addEventListener("positionduration", function(e) {
		var bits = e.data.split(":");
		update_player_position(bits[0], bits[1]);
	},false);

	//handle volume
	source.addEventListener("volume", function(e) {
		update_volume(e.data);
	},false);

	source.addEventListener("artist", function(e) {
		waiting_for_tags = false;
		$("#artist").text( e.data );
		$("#artist").show();
		$("#filename").hide();
		check_file_info_for_scrolling();
	},false);

	source.addEventListener("title", function(e) {
		waiting_for_tags = false;
		$("#title").text( ": "+e.data );
		$("#title").show();
		$("#filename").hide();
		check_file_info_for_scrolling();
	},false);

	source.addEventListener("newfile", function(e) {
		waiting_for_tags = true;
		$("#filename").text( cleaner_filename( e.data ) );
		//add a timeout for showing the filename
		setTimeout(showfilename, 100);
	},false);

	source.addEventListener("playtypechange", function(e) {
		if (e.data != "PLAY_TYPE_AUDIO") {
			//enable the audio play button
			button_enable(audio_play_button);
		}
	},false);

	source.addEventListener("playerstate", function(e) {
		process_player_state( e.data );
	},false);

	//handle audio updating
	source.addEventListener("audio_update", function(e) {
		switch(e.data) {
			case "start" :
				button_disable(audio_update_button);
			break;
			case "finish" :
				button_enable(audio_update_button);
			break;

		}
	},false);

	//handle player random
	source.addEventListener("audiorandom", function(e) {
		var checked = (e.data=="true") ? true : false;
		update_audio_random_checkbox(checked);
	},false);

	//handle the subtitles
	source.addEventListener("subtitles", function(e) {
		var bits = e.data.split(":");
		process_subtitle_data(parseInt(bits[0]), parseInt(bits[1]));
	},false);

	//handle the speed
	source.addEventListener("player_speed", function(e) {
		speed = e.data;
		//deselect the selected item
		$("#playback_speed").each(function(){
				$("#playback_speed option").removeAttr("selected");
		},false);
		$("#playback_speed option[value='"+speed+"']").attr("selected","selected");
	},false);

	//handle the mute unmute
	source.addEventListener("muted", function() {
		process_mute(true);
	},false);

	source.addEventListener("unmuted", function() {
		process_mute(false);
	},false);

	source.addEventListener("queue_remove", function(e) {
		//remove the id
		remove_queue_id(e.data);
	},false);

	source.addEventListener("queued", function(e) {
		//add the info to the queue
		add_queued(e.data);
	},false);

	source.addEventListener("episode_played", function(e) {
		//remove the id from the "unplayed" casts if it is being viewd
		remove_unplayed_episode(e.data);
	},false);

}

function showfilename() {
	if (waiting_for_tags) {
		$("#artist").hide();
		$("#title").hide();
		$("#filename").show();
	}
}

function get_video_content(path) {
	//disable the video queue button
	button_disable(video_queue_button);
	//do some ajaxy poo to get the episodes
	var url = (path) ? "/video/content/"+encodeURIComponent(path) : "/video/content";

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//process the video data
			process_video_data(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_video_content: "+type);
		}
	});
}

function get_audio_artists() {
	//disable the audio queue button
	button_disable(audio_queue_button);
	last_audio_view_function = "audio_artists";
	current_selected_audio=undefined;
	var url = "/audio/artists";

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//we should have some info about artists
			process_artist_data(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_audio_artists: "+type);
		}
	});
}

function get_player_status() {
	var url = "/player/status";

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			var info_string = "";
			if( body['title']!="") {
				$("#title").text(": "+ body['title'] );
				$("#title").show();
				$("#filename").hide();
				if (body['artist']!="") {
					$("#artist").text( body['artist'] );
					$("#artist").show();
				}
			} else {
				if (body['filename']) {
					$("#filename").text( cleaner_filename( body['filename'] ) );
				} else {
					$("#filename").text( "Welcome to MuttonChop!" );
				}
				$("#filename").show();
				$("#artist").hide();
				$("#title").hide();
			}
			//do we need to scroll?
			check_file_info_for_scrolling();

			//handle the subtitle stuff
			process_subtitle_data( body["subtitle-count"], body["current-subtitle"] );
			//handle mute
			process_mute( body['muted'] );

			update_volume( body['volume'] );

			update_player_position(body['position'], body['duration']);

			//update the random
			update_audio_random_checkbox(body["audio-play-random"]);

			process_player_state( body['state'] );

			//TODO: move the play-type and random logic to their own functino
			//handle the play-type
			switch(body['play-type']) {
				case "PLAY_TYPE_AUDIO":

					break;
				default:
					button_enable(audio_play_button);
			}

			//TODO: this should be in a muttonchop status function
			if (!body['audio-updating']) {
				if ( button_disabled(audio_update_button) && last_audio_view_function =="audio_artists" ) {
					get_audio_artists();
				}
				button_enable(audio_update_button);
				$("#audio_status").text("");
			} else {
				button_disable(audio_update_button);
				$("#audio_status").text("Updating audio database: "+body['last-audio-file-path']);
			}
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_player_status: "+type);
		}
	});
}

function process_artist_data(data) {
	//clear the title
	$("#audio_title").empty();
	//clear the audio header
	$("#audio_header").empty();
	//where do we need to attach the artist data?
	var al = $("#audio_list");
	al.empty();
	var artist_count = data.length;
	for (var i = 0 ; i < artist_count ; i++ ) {
		var alia = $('<button>');
		var artist_name = not_empty_value(data[i],"UNKNOWN");
		//handle clicks on the list_item
		alia.click( click_artist );
		alia.attr('name',artist_name);
		alia.html(artist_name);
		al.append(alia);
	}
	//we should reset the content
	reset_media_content_top();
}

function process_streams_data(data) {
	$("#streams_list").empty();
	var streams = data['streams'];
	var streams_count = streams.length;
	for (var i = 0 ; i < streams_count ; i++ ) {
		var stream = streams[i];
		var stream_text = "";
		var sli = $('<button>');
		if ( not_empty_value(stream['title']) ) {
			stream_text = stream['title'];
		} else {
			stream_text = stream['source'];
		}
		//handle clicks on the list_item
		sli.click( click_stream );
		sli.attr('stream_id',stream['id']);
		sli.html(stream_text);
		$("#streams_list").append(sli);
	}
	reset_media_content_top();
}

function process_artist_tracks(data) {
	//clear the view
	var al = $("#audio_list");
	var ah = $("#audio_header");
	al.empty();
	//set the header
	var abtn = $('<button>');
	abtn.html('<i class="fa fa-music"></i>');
	abtn.click(get_audio_artists);
	ah.append(abtn);
	var alt = $('<span class="label">');
	alt.html(data['artist']);
	ah.append(alt);
	var track_count = data.files.length;
	for (var i = 0 ; i < track_count ; i++ ) {
		file = data.files[i];
		var alia = $('<button>');
		//handle clicks on the list_item
		alia.click( click_audio_track );
		alia.attr('track_id',file['id']);
		//what text should we show?
		//what text should we show?
		var album = not_empty_value( file['album'], "Unknown");
		var title = not_empty_value( file['title'], "Unknown");
		var track_text = album + " / " + title;
		if( album == "Unknown" && title == "Unknown" ) {
			track_text = file['path'];
		}
		alia.html(track_text);
		al.append(alia);
	}
	//we should reset the content
	reset_media_content_top();
}

function process_download_data(data) {
	var len = data['downloads'].length;
	var good_ids = new Array();
	if(len > 0 ) {
		for(var i=0; i<len; i++) {
			dl = data['downloads'][i];
			var id = dl["id"];
			good_ids.push ( id );
			update_download( dl );
		}
	}
	//remove downloads that don't have a status
	$(".download_info").each( function() {
		var click_id =	$(this).attr('click_id');
		if($.inArray(click_id, good_ids)==-1 ){
			//get rid of this div
			$(this).remove();
			//mark as downloaded
			mark_downloaded( click_id );
			//do we need to show the play episode button?
			if (current_selected_episode!=undefined){
				if (click_id == $(current_selected_episode).attr("click_id")) {
					button_enable("#casts_play_episode_button");
				}
			}
		}
	});
}


function process_audio_search_data(data) {
	//clear the audio list
	var al = $("#audio_list");
	al.empty();
	//add the search string as a divider
	al.append('<span class="label label-success">' +data.message+ '</span>');
	var track_count = data.files.length;
	for (var i = 0 ; i < track_count ; i++ ) {
		file = data.files[i];
		var track = $('<button>');

		//handle clicks on the list_item
		track.click( click_audio_track );
		track.attr('track_id',file['id']);
		//what text should we show?
		var track_text = file['artist']+" : "+file['album']+" / "+file['title'];
		track.html(track_text);
		al.append(track);
	}
}

function process_subtitle_data(count, current_selected) {
	ignore_subtitle_select_change = true;
	if (current_selected==undefined) {
		current_selected = 0;
	}
	if (count==0 || count==undefined) {
		//hide the subtitle control block
		$("#subtitle_control_block").hide();
	} else {
		$("#subtitle_select").empty();
		//clear the subtitle select

		//add the subtitle options
		selected = (current_selected==-1) ? "selected" : "";
		$("#subtitle_select").append( $("<option value='-1' "+selected+">None</option>") );
		// make sure the current_selected is selected
		for (var i=0; i < count; i++) {
			//add the subtitle options
			selected = (current_selected==i) ? "selected" : "";
			$("#subtitle_select").append( $("<option value='"+i+"' "+selected+">"+i+"</option>") );
		}
		//show the subtitle control block
		$("#subtitle_control_block").show();
	}
	ignore_subtitle_select_change = false;
}

function process_mute( muted ) {
	if (muted) {
		//show unmute
		$("#button_unmute").show();
		//hide mute
		$("#button_mute").hide();
	} else {
		//show mute
		$("#button_mute").show();
		//hide unmute
		$("#button_unmute").hide();
	}
}

function process_video_data(data) {
	//disable the play button
	var vl = $("#video_list");
	//clear the current_selected_video
	current_selected_video = undefined;
	//clear out the video_list
	vl.empty();
	//clear out the breadcrumbs
	$("#video_breadcrumbs").empty();
	var dir_separator = data['dir-separator'];
	var path = data['path'];
	//if there is a path, separate out the base dir
	var path_parts = rsplit(path, dir_separator);
	var base_dir_path = path_parts[0];
	var base_dir = (path_parts[1])?path_parts[1]:path;
	var dirs = path.split(dir_separator);
	//make a video refresh button
	var video_button = $('<button>');
	video_button.attr('path','');
	//handle clicks on the list_item
	video_button.click( click_video_directory );
	video_button.html( '<i class="fa fa-film"></i>');
	$("#video_breadcrumbs").append(video_button);
	// do we have path parts?
	if (dirs.length > 0 && dirs != "" ) {
		for ( var i=0; i< dirs.length; i++ ){
			var crumb_path = dirs.slice(0,i+1).join(dir_separator);
			var title = dirs[i];
			var crumb_button = $('<button>');
			crumb_button.attr('path',crumb_path);
			//handle clicks on the list_item
			crumb_button.click( click_video_directory );
			crumb_button.html( title );
			$("#video_breadcrumbs").append(crumb_button);

		}
	}

	//reset the content top to header bottom, just in case
	reset_media_content_top();
	$("#video_content").scrollTop(0);

	//handle the directories
	var d = data['directories'];
	var dlen = d.length;
	if(d.length > 0 || path) {
		d.sort();

		var dir_path = (path)? path+dir_separator:"";
		//loop through the directories
		for (var i = 0; i < dlen ; i++) {
			var vldia = $("<button class='directory'>");
			vldia.attr('path', dir_path+d[i]);
			//handle clicks on the list_item
			vldia.click( click_video_directory );
			var title = d[i];
			vldia.html( cleaner_filename( title )+" &raquo;" );
			vl.append(vldia);
		}
	}
	//handle the files
	var files = data['files'];
	var flen = files.length;
	var file_path="";
	//is there a directory path?
	if(path) {
		file_path+=path+dir_separator;
	}
	if(files.length > 0) {
		//sort
		files.sort();
		//loop through the files
		for (var i = 0; i < flen ; i++) {
			var vlfia = $('<button class="file">');
			vlfia.attr('file_path', file_path+files[i]);
			//add the "cleaner" name as the divs text
			vlfia.html( cleaner_filename(files[i]));
			vlfia.click( click_video_file );
			vl.append(vlfia);
		}
	}
}

function click_video_directory() {
	var path = $(this).attr('path');
	button_select(this);
	get_video_content(path);
}

function click_config_update() {
	//disable the config elements
	$("#config_host").disable = true;
	$("#config_port").disable = true;
	button_disable("#button_config_update");
	//get data from the form
	host = $("#config_host").val();
	port = $("#config_port").val();
	//record the data in the local store
	localStorage.setItem("host",host);
	localStorage.setItem("port",port);
	check_config();
}

function click_video_file() {
	if (current_selected_video) {
		//unselect
		button_deselect(current_selected_video);
	}
	current_selected_video = this;
	button_select(current_selected_video);
	//enable the playbutton
	button_enable(video_play_button);
	button_enable(video_queue_button);
}

function click_audio_track() {
	if (current_selected_audio) {
		button_deselect(current_selected_audio);
	}
	current_selected_audio = this;
	button_select(current_selected_audio);
	//enable the playbutton
	button_enable(audio_play_button);
	button_enable(audio_queue_button);
}

function click_stream() {
	if (current_selected_stream) {
		button_deselect(current_selected_stream);
	}
	current_selected_stream = this;
	button_select( this );
	//enable the playbutton
	button_enable(stream_play_button);
	button_enable(stream_edit_button);
	button_enable(stream_delete_button);
}

function click_tab(event) {
	var button = $(this);
	//make things "selected"
	if (current_selected_media != undefined) {
		// if the most clicked tab is the current selected tab, return
		if (current_selected_media == button.attr("id")) {
			return;
		}
		$("#"+current_selected_media).removeClass("selected_media_tab");
		$("#"+current_selected_media+"_content").removeClass("selected_media_content");
		$("#"+current_selected_media+"_menu").removeClass("selected_media_menu");
	}
	current_selected_media = button.attr("id");
	$("#"+current_selected_media).addClass("selected_media_tab");
	$("#"+current_selected_media+"_content").addClass("selected_media_content");
	$("#"+current_selected_media+"_menu").addClass("selected_media_menu");
	switch (current_selected_media) {
		case "audio":
			click_tab_audio();
			break;
		case "video":
			//?
			break;
		case "casts":
			click_tab_casts();
			break;
		case "streams":
			get_streams();
			break;
		case "queue":
			reset_media_content_top();
			break;
	}
}

function click_audio_play_button() {
	if ( button_disabled("#audio_play_button") ) {
		return;
	}
	var track_id;
	var url = "/audio/play";

	if(current_selected_audio){
		track_id = $(current_selected_audio).attr('track_id');
		var url = "/audio/play/"+track_id;

	}

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) {
			//disable the audio play button
			button_disable(audio_play_button);
		},
		error: function(xhr, type) {
			log("error :click_audio_play_button: "+type);
		}
	});
}

function disable_audio_search() {
	audio_search_button.attr('disabled',true);
	audio_search_text.attr('disabled',true);
}

function enable_audio_search() {
	audio_search_button.attr('disabled',false);
	audio_search_text.attr('disabled',false);
}

function click_audio_search_button() {
	last_audio_view_function = "audio_search";
	//disable the search components
	var search_text = prompt("Search:", "");
	if (search_text == null ) {
		return;
	} else {
		search_text = search_text.trim();
		if (search_text == "") {
			return;
		}
	}
	var text = encodeURIComponent(search_text);
	var url = "/audio/search/"+text;


	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) {
			process_audio_search_data( body );
		},
		error: function(xhr, type) {
			log("error :click_audio_search_button: "+type);
		}
	});
}

function click_audio_update_button() {
	//disable the audio play button
	button_disable(audio_update_button);
	var url = "/audio/update";

	//this could take a while, confirm with the user
	var result = confirm("This could take a while. Continue?");
	if( !result ) {
		return;
	}
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) {

		},
		error: function(xhr, type) {
			log("error :click_audio_update_button: "+type);
			audio_update_button.attr("disabled",false);
		}
	});
}

function click_audio_random_checkbox() {
	var checked = audio_random_checkbox.is(":checked");
	var url = "/audio/random/"+checked;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) {
			//disable the audio play button
			button_disable(audio_play_button);
		},
		error: function(xhr, type) {
			log("error :click_audio_random_checkbox: "+type);
		}
	});
}

function click_video_play_button() {
	//what is the current
	var p = value_or_empty_string($(current_selected_video).attr('file_path'));
	//urlencode
	p = encodeURIComponent(p);
	//disable the play button
	button_disable(video_play_button);
	var url = "/video/play/"+p;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anytdirpathing?
		},
		error: function(xdirpathr, type) { // type is a string ('error' for dirpathTTP errors, 'parsererror' for invalid JSON)
			log("error :click_video_play_button: "+type);
		}
	});
}

function click_play() {
	if (button_disabled("#button_play") ){
		return;
	}
	var url = "/player/play";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_play: "+type);
		}
	});
}
function click_pause() {
	var url = "/player/pause";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_pause: "+type);
		}
	});
}

function click_stop() {
	var url = "/player/stop";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_stop: "+type);
		}
	});
}

function click_next() {
	var url = "/player/next";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_next: "+type);
		}
	});
}
function click_previous() {
	var url = "/player/previous";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_previous: "+type);
		}
	});
}

function click_forward() {
	var url = "/player/forward";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_forward: "+type);
		}
	});
}

function click_reverse() {
	var url = "/player/reverse";

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_reverse: "+type);
		}
	});
}

function click_volume_down() {
	volume_change(-2);
}

function click_volume_up() {
	 volume_change(2);
}
function click_volume_down_more() {
	volume_change(-6);
}

function click_volume_up_more() {
	 volume_change(6);
}

function click_mute() {
	var url = "/player/mute";
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_mute: "+type);
		}
	});
}

function click_unmute() {
	var url = "/player/unmute";
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_unmute: "+type);
		}
	});
}

function volume_change(num) {
	var url = "/player/volume_change/"+num;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :volume_change: "+type);
		}
	});
}


/* stream click handlers */
function click_stream_play_button() {
	var id = $(current_selected_stream).attr("stream_id");
	var url = "/stream/play/"+id;

	button_disable(stream_play_button);
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :play stream: "+type);
		}
	});
}

function click_stream_delete_button() {
	var r = confirm("Really delete?");
	if (r == true) {
		var id = $(current_selected_stream).attr("stream_id");
		var url = "/stream/delete/"+id;

		$.ajax({
			url: url,
			dataType: 'text',
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//refresh the streams list
				get_streams();
			},
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :delete stream: "+type);
			}
		});
	}
}

function click_stream_confirm_button() {
	//get some data
	var source = $("#stream_source").val().trim();
	var title = $("#stream_title").val().trim();
	if (source == "") {
		alert( "You must enter a URL" );
		return;
	}
	if (title == "") {
		alert( "You must enter a title" );
		return;
	}
	if (editing_stream) {
		var id = $(current_selected_stream).attr("stream_id");
		//form a URL
		var url = "/stream/edit/"+encodeURIComponent(id);

		url += "?title="+encodeURIComponent(title);
		url += "&source="+encodeURIComponent(source);
	} else {
		var url = "/stream/add/"+encodeURIComponent(source)+"?title="+encodeURIComponent(title);

	}
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//refresh the streams list
			get_streams();
			//hide stuff
			switch_to_page("#Streams");
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			alert(xhr.responseText);
		}
	});
}

function click_stream_new_button() {
	//we are not editing a stream
	editing_stream = false;
	//clear the info on the addedit page
	$("#stream_source").val('');
	$("#stream_title").val('');
	$("#streamAddEdit .title").text("New Stream");
	$("#stream_confirm_button").html("Add");
	switch_to_page("#StreamAddEdit");
}

function click_stream_edit_button() {
	//we are editing
	editing_stream = true;
	//clear the info on the addedit page
	$("#stream_source").val('');
	$("#stream_title").val('');
	$("#streamAddEdit .title").text("New Stream");
	$("#stream_confirm_button").html("Update");


	//get the id of the current_selected_stream
	var id = $(current_selected_stream).attr("stream_id");
	url = "/stream/info/"+id;
		$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			$("#stream_title").val( body['title'] );
			$("#stream_source").val( body['source'] );
			switch_to_page("#StreamAddEdit");
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			alert("Failed to get data for stream "+id);
		}
	});
}

function click_stream_cancel_button() {
	$("#stream_source").val("");
	$("#stream_title").val("");
	$("#stream_add_edit").hide();
	$("#stream_blocker").hide();
	stream_new_button.attr('disabled', false);
}

function get_streams() {
	var url = "/stream/list";

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//process the json
			process_streams_data(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_previous: "+type);
		}
	});

}

function click_tab_audio() {
	if ( got_audio == false ) {
		got_audio = true;
		get_audio_artists();
	}
	return true;
}

function click_tab_casts() {
	//is this the first time this has been clicked?
	if(!got_casts) {
		get_feeds();
		got_casts = true;
	}
}

function connect_buttons() {
	//TODO: move these functions to the respective "set_up"
	$("#tab_audio").click( click_tab_audio );
	video_play_button = $("#video_play_button");
	video_play_button.click( click_video_play_button );
	audio_play_button = $("#audio_play_button");
	audio_play_button.click( click_audio_play_button );
	audio_update_button = $("#audio_update_button");
	audio_update_button.click( click_audio_update_button );
	audio_random_checkbox = $("#audio_random_checkbox");
	audio_random_checkbox.click( click_audio_random_checkbox );
	audio_search_button = $("#audio_search_button");
	audio_search_text = $("#audio_search_text");
	audio_search_text.keydown( function(e){
		var key = e.which;
		if (key == 13) {
			click_audio_search_button();
		}
	});
	audio_search_button.click( click_audio_search_button );
	//disable some buttons
	button_disable(audio_play_button);
	button_disable(audio_update_button);
	button_disable(video_play_button);

	$("#audio_refresh_button").click( get_audio_artists );
	//audio_queue_button is handled by queue_common.js
}

function hide_config() {
	//hide the config button
	$("#nav_config").hide();
	//do we need to hide the config page?
}


function set_up_config() {
	var data_host = localStorage.getItem("host");
	if (data_host != undefined) {
		host = data_host;
	}

	var data_port = localStorage.getItem("port");
	if (data_port != undefined) {
		port = data_port;
	}

	//set the config input values
	$("#config_host").val(host);
	$("#config_port").val(port);

	$("#button_config_update").bind("click", click_config_update);
}

function set_up_player() {
	//hide the pause button
	$("#button_pause").hide();
	//hide the unmute
	$("#button_unmute").hide();
	$("#range_volume").bind('change', change_volume );
	$("#button_play").bind('click', click_play );
	$("#button_pause").bind('click', click_pause );
	$("#button_stop").bind('click', click_stop );
	$("#button_previous").bind('click', click_previous );
	$("#button_next").bind('click', click_next );
	$("#button_forward").bind('click', click_forward );
	$("#button_reverse").bind('click', click_reverse );
	$("#button_volume_up").bind('click', click_volume_up );
	$("#button_volume_down").bind('click', click_volume_down );
	$("#button_volume_up_more").bind('click', click_volume_up_more );
	$("#button_volume_down_more").bind('click', click_volume_down_more );
	$("#button_mute").bind('click', click_mute );
	$("#button_unmute").bind('click', click_unmute );
	$("#subtitle_select").bind('change', change_subtitle_select);
	$("#playback_speed").bind('change', change_playback_speed);
}

function set_up_queue() {
	//get the queue list
	get_queue_list();
	//set up the queue queue buttons
  queue_delete_button = $("#queue_delete_button");
  button_disable(queue_delete_button);
  queue_delete_button.click( click_queue_delete_button );
  //set up the audio queue button
  audio_queue_button = $("#audio_queue_button");
  button_disable(audio_queue_button);
  audio_queue_button.click( click_audio_queue_button );
  //set up the video queue button
  video_queue_button = $("#video_queue_button");
  button_disable(video_queue_button);
  video_queue_button.click( click_video_queue_button );
  //set up the cast queue button
  episode_queue_button = $("#casts_queue_episode_button");
  button_disable(episode_queue_button);
  episode_queue_button.click( click_episode_queue_button );
}

function set_up_swipe() {
//TODO: remove if no swipe is implemented
}

function click_artist() {
	current_selected_audio=undefined;
	var artist = $(this);
	var name = artist.text();
	$("#audio_title").html( name );
	var url = "/audio/artist_tracks/"+name;

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_artist_tracks( body );
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_artist: "+type);
		}
	});
}

function timer_get_player_status() {
	get_player_status();
	setTimeout("timer_get_player_status()", player_timer_speed);
}

function change_subtitle_select() {
	if (ignore_subtitle_select_change ) {
		return;
	}
	var val = $("#subtitle_select").val();
	var url = "/player/subtitle/"+val;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :change_subtitle_select: "+type);
		}
	});
}

function change_playback_speed() {
	if (ignore_subtitle_select_change ) {
		return;
	}
	var val = $("#playback_speed").val();
	var url = "/player/speed/"+val;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :change_subtitle_select: "+type);
		}
	});
}

function change_volume(event) {
	if (!updating_volume) {
		var new_volume = $("#range_volume").val();
		if ( new_volume != current_volume ) {
			current_volume = new_volume;
			set_volume( new_volume );
		}
	}
}
function set_volume( volume ) {
	var url = "/player/volume/"+volume;

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(vol) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//nothing to do
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_volume: "+type);
		}
	});
}

function update_player_position( position, duration) {
	var percent = 0;
	if ( duration ) {
		percent = (position/duration)*100;
		var text = cleaner_time(position) +" / "+cleaner_time(duration);
		$("#progress_bar_text").html( text );
		$("#progress_frame_text").html( text );
	}
	$("#progress_bar").width(percent+"%");
}

function update_volume( vol ) {
	updating_volume = true;
	if (current_volume != vol ) {
		$("#range_volume").val(vol);
		$("#volume_value").html(vol);
		current_volume = vol;
	}
	updating_volume = false;
}

function update_audio_random_checkbox( val ) {
	$("#audio_random_checkbox").attr("checked",val);
	try {
		$("#audio_random_checkbox").checkboxradio("refresh");
	} catch(e) {
		//pfftttt do nothing
	}
}

/* some cast specific stuff */

function set_up_casts(){
	/* connect button clicks */
	$("#casts_add_feed_button").click( clicked_casts_add_feed_button );
	$("#casts_update_feeds_button").click( clicked_casts_update_feeds_button );
	$("#casts_update_feed_button").click( clicked_casts_update_feed_button );
	$("#casts_delete_feed_button").click( clicked_casts_delete_feed_button );
	$("#casts_available_episodes_cancel_button").click(clicked_casts_available_episodes_cancel_button);
	$("#casts_available_episodes_download_button").click(clicked_casts_available_episodes_download_button);

	$("#casts_play_episode_button").click( clicked_casts_play_episode_button );
	$("#casts_stream_episode_button").click( clicked_casts_stream_episode_button );

	$("#casts_download_episode_button").click( clicked_casts_download_episode_button );
	$("#casts_casts_button").click(clicked_casts_casts_button);
	$("#casts_downloads_cancel_button").click(clicked_casts_downloads_cancel_button);

	/* disable buttons */
	button_disable("#casts_update_feeds_button");
	/* hide what needs hiding */
	$("#cast_header").hide();
	$("#casts_feed_submenu").hide();
	$("#casts_available_episodes_submenu").hide();
	$("#casts_downloads_submenu").hide();
	//reset the episode buttons
	reset_cast_submenu_buttons();
}

function set_up_streams() {
	//set up the stream buttons
	$("#stream_play_button").click( click_stream_play_button );
	$("#stream_edit_button").click( click_stream_edit_button );
	$("#stream_delete_button").click( click_stream_delete_button );
	$("#stream_add_button").click( click_stream_new_button );
	$("#stream_cancel_button").click( click_stream_cancel_button );
	$("#stream_confirm_button").click( click_stream_confirm_button );
	//disable some buttons
	button_disable("#stream_edit_button");
	button_disable("#stream_delete_button");
	button_disable("#stream_play_button");
}

function timer_get_download_status() {
	get_download_status();
	setTimeout("timer_get_download_status()", download_status_speed);
}

function clicked_kill() {
	//get the id of the clicked kill
	var id = $(this).attr("click_id");
	//get rid of the download div
	$(".download").each( function() {
		var click_id = $(this).attr('click_id');
		if ( click_id == id) {
			//get rid of this div
			$(this).remove();
		}
	});
	//ajax query to kill the dl
	var url = "/catcher/kill/"+id;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do nothing
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_kill: "+type);
		}
	});
}

function get_feeds() {
	var url = "/catcher/feeds";

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_feed_data( body );
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_feeds: "+type);
		}
	});
}

function mark_played( id ) {
	//what target do we need to change?
	var target_ref = "#episode_"+id+" .played";
	var element = $(target_ref);
	element.html("*");
}

function mark_downloaded( id ) {
	//what target do we need to change?
	var target = $("#episode_"+id+" .downloaded");
	target.html("*");
}

function get_download_status() {
	if (check_downloads == false) {
		return;
	}
	var url = "/catcher/download_status";

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_download_data( body );
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_download_status: "+type);
		}
	});
}

function get_episode(id) {
	var url = "/catcher/episode/"+id;

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			if( body ) {
				//we should always be able to queue
				button_enable(episode_queue_button);
				// determine if this episode exists locally
				if (body['downloaded'] && body['exists']) {
					//enable the play button
					button_show("#casts_play_episode_button");
					button_enable("#casts_play_episode_button");
					button_hide("#casts_stream_episode_button");
					button_disable("#casts_casts_download_episode_button");
				} else {
					//check if the episode is currently being downloaded
					var did = "download_"+id;
					if ( $("#"+did).length ){
						//the episode is being downloaded

					} else {
						button_show("#casts_stream_episode_button");
						button_hide("#casts_play_episode_button");
						button_enable("#casts_casts_download_episode_button");
					}
				}
			}
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_episode: "+type);
		}
	});
}

function get_episodes(id) {
	var url = "/catcher/episodes/"+id;

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_episodes_data( body );
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_episodes: "+type);
		}
	});
}

function clicked_casts_update_feeds_button() {
	button_disable("#casts_update_feeds_button");
	var url = "/catcher/update/all";
	process_update_feed_data(available_ep_data);
	/*
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_update_feed_data(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_update_feeds_button: "+type);
		}
	});
	*/
}
function clicked_casts_update_feed_button() {
	button_disable("#casts_update_feed_button");
	var url = "/catcher/update/"+current_selected_feed_id;

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			button_enable("#casts_update_feed_button");
			process_update_feed_data(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_update_feed_button: "+type);
		}
	});
}

function clicked_casts_add_feed_button() {
	var url = prompt("Enter Feed URL","");

	if (url == null ) {
		return;
	} else {
		url = url.trim();
		if (url == "") {
			return;
		}
	}
	var url = "/catcher/add/"+encodeURIComponent( url );
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_add_feed_data(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_submit_feed_button: "+type);
		}
	});
}

function set_current_cast_submenu(id) {
	//hide the current submenu if it exists
	if (current_cast_submenu != undefined ) {
		$("#"+current_cast_submenu).hide();
	}
	//set the current_cast_submenu to id
	current_cast_submenu = id;
	$("#"+current_cast_submenu).show();
}

function clicked_casts_casts_button() {
	//show the casts submenu
	set_current_cast_submenu("casts_feeds_submenu");
	//hide the feed title
	$("#casts_cast_header").hide();
	//get the feeds
	get_feeds();
}

function clicked_casts_delete_feed_button() {
	var r = confirm("Really delete "+current_selected_feed_title+"?");
	if (r == true) {
		var id = current_selected_feed_id;
		var url = "/catcher/delete/"+id;
		//do a jquery to remove the feed
		$.ajax({
			url: url,
			dataType: 'text',
			success: function(body) {
				//we should reset the feeds list
				get_feeds();
				//switch to the casts page
				switch_to_page("#Casts");
			},
			error: function() {
				//what should be done on error?
			}
		});
	}
}

function clicked_casts_available_episodes_download_button() {
	var dls = new Array();
	$(".available_episode_checkbox:checked").each( function(){
		var name = $(this).attr('name');
		dls.push( name );
	});
	if (dls.length == 0 ) {
		//nothing was selected
		alert("You must select an episode to download");
		return;
	}
	id_string = dls.join(":");
	//make a list of the available episodes checkboxes
	//collect the IDs of the checked boxes
	//send AJAXy data so shit can get downloaded
	var url = "/catcher/download/"+encodeURIComponent(id_string);

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//switch to the downloads page
			show_casts_downloads();
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_available_episodes_cancel_button: "+type);
		}
	});
}

function clicked_casts_available_episodes_cancel_button() {
	//show the previous buttons
	$("#"+current_cast_submenu).show();
	//hide the available episodes buttons
	$("#casts_available_episodes_submenu").hide();
	//show the casts list
	$('#casts_list').show();
	//hide the  available episode list
	$("#available_episodes_list").hide();
}

function clicked_casts_play_episode_button() {
	//disable the button
	button_disable("#casts_play_episode_button");
	var id = $(current_selected_episode).attr("click_id");
	playing_episode_id = id;
	var url = "/catcher/play/"+id;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//if the current_selected_feed_id is 0 remove the selected episode
			if (current_selected_feed_id == 0 ) {
				$(current_selected_episode).remove();
			}
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_play_episode_button: "+type);
		}
	});
}
function clicked_casts_stream_episode_button() {
	var id = $(current_selected_episode).attr("click_id");
	playing_episode_id = id;
	var url = "/catcher/stream/"+id;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do nothing
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_play_episode_button: "+type);
		}
	});
}

function clicked_casts_downloads_cancel_button() {
	alert('cancel');
}

function clicked_casts_download_episode_button() {
	//disable the download button
	button_disable("#casts_download_episode_button");
	var id = current_selected_episode_id;

	var url = "/catcher/download/"+id;

	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
			button_enable("#casts_download_episode_button");
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_download_episode_button: "+type);
		}
	});
}

function process_update_feed_data(data) {
	console.log(data);
	button_enable("#casts_update_feeds_button");
	//are there episodes?
	var episodes = data['episodes'];
	if( episodes.length > 0) {
		//TODO: create a 'popup' of available episodes?
		$("#available_episodes_list").empty();
		for (var i=0; i< episodes.length; i++) {
			var	episode = episodes[i];
			var id = episode['id'];
			var title = episode['title'];
			var date = date_from_pubdate( episode['pubdate'] );
			var epi = $("<div class='faux_button'>");
			var epil = $("<label for='cb_"+id+"'>");
			var epild = $("<div>");
			var cb = $("<input type='checkbox' class='available_episode_checkbox' name='"+id+"' id='cb_"+id+"' value=1>");
			epild.append(cb);
			epil.append(epild);
			var t = $("<span class='episode_title'>");
			t.append( title );
			epild.append( t );
			var d = $("<span class='pub_date'>");
			d.append( date );
			epild.append( d );
			epi.append(epil);
			$("#available_episodes_list").append(epi);
		}
		show_available_episodes();
	} else {
		alert( "No New Episodes" );
	}
	if (data['error'] ) {
		alert( data['error'] );
	}
}

function show_available_episodes() {
	//show the available episodes buttons
	set_current_cast_submenu("casts_available_episodes_submenu");
	//hide the casts list
	$('#casts_list').hide();
	//show the  available episode list
	$("#available_episodes_list").show();
}

function show_casts_downloads() {
	//set the current cast submenu to downloads
	set_current_cast_submenu("casts_downloading_episodes_submenu");
	//show the available episodes buttons
	$("#casts_available_episodes_submenu").show();
	//hide the casts list
	$('#casts_list').hide();
	//hide the  available episode list
	$("#available_episodes_list").hide();
	//show the  available episode list
	$("#dowloading_episodes_list").show();
}

function process_system_info(system) {
	if (!system['has_video']) {
		//we can only play a video's audio track
	}
	document.title += " : "+system['hostname'];

}

function process_player_state( state ) {
	//check a button to see if it is disabled
	if ( button_disabled("#button_play") ) {
			enable_player_buttons();
	}

	switch (state) {
		case "PLAYING" :
			enable_player_buttons();
			$("#button_play").hide();
			$("#button_pause").show();

			break;
		case "PAUSED" :
		case "READY" :
			$("#button_play").show();
			$("#button_pause").hide();
			break;
		default:
			disable_player_buttons();
			$("#button_pause").hide();
			break;
	}
}

function disable_player_buttons() {
	button_disable("#button_play");
	button_disable("#button_pause");
	button_disable("#button_stop");
	button_disable("#button_previous");
	button_disable("#button_next");
	button_disable("#button_reverse");
	button_disable("#button_forward");
}

function enable_player_buttons() {
	button_enable("#button_play");
	button_enable("#button_pause");
	button_enable("#button_stop");
	button_enable("#button_previous");
	button_enable("#button_next");
	button_enable("#button_reverse");
	button_enable("#button_forward");
}

function process_add_feed_data(data) {
	if (data['episodes'].length > 0 ) {
		//the feeds were updated, show the new feeds
		get_feeds();
		process_update_feed_data(data);
	}else{
		alert("Could not process the RSS feed");
	}
}

function process_episodes_data( data ) {
	//clear all data from the feeds list
	$("#casts_list").empty();
	//reset the current_selected_episode_id
	current_selected_episode_id=null;
	len = data.length;
	if(len > 0 ) {
		for(var i=0; i<len; i++) {
			var episode = data[i];
			//start making the episode object
			var id = episode['id'];
			var e = $("<button class='episode'><table><tr>");
			e.attr("id", "episode_"+id);
			e.attr("click_id", id);

			var downloaded = $("<td class='episode_download_status status'>");
			var dled = false;
			if (episode['downloaded']==1) {
				dled = true;
				downloaded.addClass("status_true");
			}
			downloaded.html( "&darr;" );
			e.append(downloaded);

			var played = $("<td class='episode_played_status status'>");
			if (episode['played']==1) {
				played.addClass("status_true");
			}
			played.html( "&rsaquo;" );
			e.append(played);
			var date = $("<td class='pub_date'>");
			date.html( date_from_pubdate(episode['pubdate'])	);
			e.append(date);
			var t = $("<td class='episode_title'>");
			t.html(episode['title']);
			e.append(t);

			//episode might get clicked
			e.click( click_episode );
			//append the info
			$("#casts_list").append( e );
		}
	}
	//we should reset the content
	reset_media_content_top();
}

function process_feed_data( data ) {
	var len = data.length;
	if(len > 0 ) {
		//clear any feeds that may exist
		$("#casts_list").empty();
		button_enable("#casts_update_feeds_button");
		//create and add the 'unplayed'
		var unplayed = {'id':0, 'title':'Unplayed'}
		//connect the unplayed
		connect_feed( unplayed );
		for(var i=0; i<len; i++) {
			feed = data[i];
			connect_feed( feed );
		}
	}
	reset_media_content_top();
}

function connect_feed( feed ) {
	//start making the feed object
	var id = feed['id'];
	var fli = $("<button>");
	fli.attr("id", "feed_"+id);
	fli.attr("click_id", id);
	//create the title
	fli.html(feed['title']);
	fli.click( click_feed );
	//append the feed
	$("#casts_list").append(fli);
}

function reset_cast_submenu_buttons() {
	button_disable("#casts_play_episode_button");
	button_disable("#casts_casts_download_episode_button");
	button_disable("#casts_queue_episode_button");
	button_hide("#casts_stream_episode_button");
}

function click_feed(){
	var feed = $(this);
	current_selected_feed_id = $(feed).attr("click_id");
	//hide the update and delete buttons if the the id is 0
	if( current_selected_feed_id == 0	) {
		$("#casts_update_feed_button").hide();
		$("#casts_delete_feed_button").hide();
	} else {
		$("#casts_update_feed_button").show();
		$("#casts_delete_feed_button").show();
	}
	current_selected_feed_title = $(feed).html();
	//set the title of the episodes
	$("#feed_title").html(current_selected_feed_title);
	//get the episodes for this feed
	get_episodes( current_selected_feed_id );
	//reset the casts_feed_submenu buttons
	reset_cast_submenu_buttons();
	//show the cast menu
	set_current_cast_submenu("casts_feed_submenu");
	$("#casts_cast_header").show();
}

function click_episode() {
	//have we switched buttons?
	if (current_selected_episode != this ) {
		current_selected_episode_id = $(this).attr("click_id");
		//highlight some div
		if (current_selected_episode!=undefined) {
			button_deselect( $(current_selected_episode) );
		}
		//get the episode data
		get_episode( current_selected_episode_id );
		current_selected_episode = this;
		button_select( $(current_selected_episode) );
	}
}

function date_from_pubdate( pubdate ) {
	try{
		var date_parts = pubdate.split(" ");
		return date_parts[0];
	}catch(e){
		return pubdate;
	}
}

function get_queue_list() {
	var url = "/queue/list";
	$.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //process the json
			process_queue_data(body);
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get/queue/list: "+type);
    }
  });
}

function process_queue_data(queue_items) {
	$("#queue_list").empty();
	var queue_count = queue_items.length;
	for (var i = 0 ; i < queue_count ; i++ ) {
		var item = queue_items[i];
		queue_list_add(item);
	}
}

function queue_list_add(item) {
	var item_text = "";
	var sli = $("<button>");
	if ( not_empty_value(item['title']) && not_empty_value(item['artist']) ) {
		item_text = item['artist']+" : "+item['title'];
	} else if(item['play_type']=='cast') {
		item_text = item['feed']+" : "+item['title'];
	} else {
		item_text = item['play_type']+" : "+item['reference'];
	}
	//handle clicks on the list_item
	sli.click( click_queue_item );
	sli.attr('queue_id',item['id']);
	sli.html(item_text);
	$("#queue_list").append(sli);

}

function click_queue_delete_button() {
		var r = confirm("Really delete this queued item?");
	if (r == true) {
		var url = "/queue/delete/"+selected_queue_id;
		$.ajax({
			url: url,
			dataType: 'json',
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//process the json
				remove_queue_id(selected_queue_id);
				//disable the delete button
				button_disable(queue_delete_button);
			},
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :get/queue/list: "+type);
			}
		});
	}
}

function click_audio_queue_button() {
	//what is the currently_selected_audio item?
	var reference = $(current_selected_audio).attr("track_id");
	if (reference) {
		var txt = $(current_selected_audio).text();
		var type = 'audio'
		var r = confirm("Add "+txt+" to the queue?");
		if (r == true) {
			var url = "/queue/add/"+type+"/"+reference;
			do_get_request(url);
		}
	}
}

function click_video_queue_button() {
	//what is the currently_selected_video item?
	var reference = $(current_selected_video).attr("file_path");
	if (reference) {
		var txt = $(current_selected_video).text();
		var type = 'video'
		var r = confirm("Add "+txt+" to the queue?");
		if (r == true) {
			var url = "/queue/add/"+type+"/"+reference;
			do_get_request(url);
		}
	}
}

function click_episode_queue_button() {
	//what is the currently_selected_video item?
	var reference = current_selected_episode_id;
	if (reference) {
		var txt = $(current_selected_episode).find(" > .episode_title")[0].innerHTML;
		var type = 'cast'
		var r = confirm("Add "+txt+" to the queue?");
		if (r == true) {
			var url = "/queue/add/"+type+"/"+reference;
			do_get_request(url);
		}
	}
}

//
function do_get_request(url){
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :"+url);
		}
	});
}

function remove_queue_id(id) {
	$("#queue_list [queue_id='"+id+"']").remove();
}

function click_queue_item() {
	var item = this;
	selected_queue_id = $(item).attr("queue_id");
	if (current_selected_queue) {
		button_deselect(current_selected_queue);
	}
	current_selected_queue = item;
	button_select(current_selected_queue);
	//enable the playbutton
	button_enable(queue_delete_button);
}

function add_queued( item_string ) {
	var item = JSON.parse( item_string );
	queue_list_add(item);
}


function update_download( dl ) {
	//what is the percent?
	var p = dl['percent'];
	if ( p > 100 ) {
		p=0;
	}
	var id = dl['id'];
	var did = "download_"+id;
	var didp = did+"_progress"
	var didpw = didp+"_wrapper"
	//does this exist?
	if( $("#"+did).length==0) {
		//create the download div
		var dl_div = $("<div class='download_info'>");
		dl_div.attr("id", did);

		dl_div.attr("click_id", id);
		var title = $("<div class='download_title'>");
		title.html( dl['feed-title']+": "+dl['title'] );
		var progw = $("<div class='progress_frame'>");
		var prog = $("<div id='"+didp+"' class='progress_bar'>");
		prog.width(p+'%');
		progw.append(prog);
		dl_div.append( title );
		dl_div.append( progw );
		var kill_dl_div = $("<button class='kill_download'>");
		kill_dl_div.html("kill");
		kill_dl_div.attr("click_id", id);
		kill_dl_div.click( clicked_kill );
		dl_div.append(kill_dl_div);
		$("#downloads_list").append( dl_div );
	}
	else{
		$("#"+didp).width(p+"%");
	}
}

function reset_media_content_top() {
	//what is the bottom of the media_menus

	var menu_bottom = $("#media_menus").outerHeight() + $("#media_menus").offset().top;
	//only set the current_selected_media top
	$("#"+current_selected_media+"_content").css("top", menu_bottom+5+"px");
}

function file_info_should_scroll() {
	//what is the width of the #file_info element?
	var fi_width = file_info.width();
	//what is the width of the window?
	var win_width = $(window).width();
	return win_width < fi_width ;
}

function scroll_file_info(){
	if (!file_info_is_scrolling) {
		file_info_is_scrolling = true;
		var fi_width = file_info.width();
		//what is the width of the window?
		var pi_width = $("#player_info").width();
		var offset = fi_width - pi_width
		var diff = -(offset);
		var speed = offset*30;
		if ( parseInt(file_info.css('left')) < 0 ) {
			//we should scroll right
			diff = 0;
		}
		file_info.animate(
			{ left: diff+"px" },
			speed,
			function() {
				file_info_is_scrolling = false;
				check_file_info_for_scrolling();
			}
		);
	}
}

function remove_unplayed_episode(id){
	if (current_selected_feed_id == 0) {
		$("#casts_list [id='episode_"+id+"']").remove();
	}
}

//we should detect localstorage
function has_local_storage() {
	try {
		return 'localStorage' in window && window['localStorage'] !== null;
	} catch(e){
		return false;
	}
}
