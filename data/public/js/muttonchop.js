/* Part of MuttonChop
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */

$(function(){	
  //set some variables
  current_selected_video = undefined;
  current_selected_stream = undefined;
  current_volume = undefined;
  player_timer_speed = 2000;
  download_status_speed = 2500;
  has_event_source = false;
  first_player_status = true;
  //set some default global variables for feeds
	selected_feed_id = null;
	selected_episode_id = null;
	playing_episode_id = null;
  //what is the last audio function?
  last_audio_view_function = null;
  // create the tabs
  create_tabs();
  //connect some the buttons we will need
  connect_buttons();
  //set up the player
  setup_player();
  //get the video content
  get_video_content('','');
  //get the audio artists
  get_audio_artists();
  //get the stream list
  get_stream_list();
  //get the queue list
  get_queue_list();
  //set up the casts
  setup_casts();
  
  //can we do SSE?
  if (window.EventSource) {
		has_event_source = true;
		//get the status once
		get_player_status();
		//handle the event source
		set_up_sse();
	} else {
		// bummer dude, use the polling
		timer_get_player_status();
	}
});

function set_up_sse() {
	//what is the source of our events?
	var source = new EventSource('/events');
	
	//handle position duration 
	source.addEventListener("positionduration", function(e) {
		var bits = e.data.split(":");
		update_duration(bits[0], bits[1]);
	});
	
	//handle volume 
	source.addEventListener("volume", function(e) {
		var vol = e.data;
		update_volume(vol);
	});
	
	source.addEventListener("artist", function(e) {
		waiting_for_tags = false;
		$("#artist").text( e.data );
		$("#artist").show();
		$("#filename").hide();
	});
	
	source.addEventListener("title", function(e) {
		waiting_for_tags = false;
		$("#title").text( ": "+e.data );
		$("#title").show();
		$("#filename").hide();
	});
	
	source.addEventListener("tagread", function(e) {
		$("#audio_status").text( e.data );		
	});
	
	source.addEventListener("newfile", function(e) {
		waiting_for_tags = true;
		$("#filename").text( cleaner_filename( e.data ) );
		//add a timeout for showing the filename
		setTimeout(showfilename, 100);
	});
	
	source.addEventListener("playtypechange", function(e) {
		if (e.data != "PLAY_TYPE_AUDIO") {
			//enable the audio play button
			audio_play_button.removeAttr("disabled");
		}
	});
	
	source.addEventListener("playerstate", function(e) {
		switch (e.data) {
			case "PLAYING" :
			$("#play").hide();
			$("#pause").show();
			break;
			
			case "PAUSED" :
			$("#play").show();
			$("#pause").hide();
			break;
		}
 
	});

	source.addEventListener("queue_remove", function(e) {
		//remove the id
		remove_queue_id(e.data);
	});
	
	source.addEventListener("queued", function(e) {
		//remove the id
		add_queued(e.data);
	});
	
	//handle audio updating 
	source.addEventListener("audio_update", function(e) {
		switch(e.data) {
			case "start" :
				audio_update_button.attr('disabled',true);
				$("#audio_status").show();
			break;
			case "finish" :
				audio_update_button.removeAttr("disabled");
				$("#audio_status").hide();
			break;
			
		}
	});
	
	//handle player random 
	source.addEventListener("audiorandom", function(e) {
		var checked = (e.data=="true")? true : false;
		document.getElementById("audio_random_checkbox").checked = checked;
	});	
	
	//handle the subtitles
	source.addEventListener("subtitles", function(e) {
		var bits = e.data.split(":");
		process_subtitle_data(parseInt(bits[0]), parseInt(bits[1]));
	});	

	//handle the mute unmute
	source.addEventListener("muted", function() {
		process_mute(true);
	});
	
	source.addEventListener("unmuted", function() {
		process_mute(false);
	});

}

function showfilename() {
	if (waiting_for_tags) {
		$("#artist").hide();
		$("#title").hide();
		$("#filename").show();
	}
}


function get_video_content(path) {
	//do some ajaxy poo to get the episodes
  var url = (path) ? "/video/content/"+path : "/video/content";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //clear the current selected video
      process_video_data(body);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_video_content: "+type);
    } 
  });
}

function get_audio_artists() {
	last_audio_view_function = "audio_artists";
	current_selected_audio=undefined;
	var url = "/audio/artists";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //clear the current selected video
      set_audio_view(body,"artists");
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_audio_artists: "+type);
    } 
  });
}

function get_player_status() {
  var url = "/player/status";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			var info_string = "";
			if( body['title']!="") {
				$("#title").text(": "+ body['title'] );
				$("#title").show();
				$("#filename").hide();
				if (body['artist']!="") {
					$("#artist").text( body['artist'] );
					$("#artist").show();
				}
			} else {
				$("#filename").text( cleaner_filename( body['filename'] ) );
				$("#filename").show();
				$("#artist").hide();
				$("#title").hide();
			}
			
			var vol = body['volume'];
			update_volume(vol);
      update_duration(body['position'], body['duration']);
      // handle the state 
      var state = body['state'];
      switch (state) {
        case "PLAYING" :
          $("#play").hide();
          $("#pause").show();
          break;
        case "PAUSED" :
          $("#play").show();
          $("#pause").hide();
          break;
        default:
          break;
      }
      //handle the subtitle stuff
      process_subtitle_data( body["subtitle-count"], body["current-subtitle"] );
      
      //TODO: move the play-type and random logic to their own functino
      //handle the play-type
      switch(body['play-type']) {
				case "PLAY_TYPE_AUDIO":
					//disable the audio play button
					if(!current_selected_audio) {
						audio_play_button.attr("disabled", true);
					}
					break;
				default:
					audio_play_button.removeAttr("disabled");
			}
			//what's the story with the audio_play_random?
			document.getElementById("audio_random_checkbox").checked = body["audio-play-random"];
			
			//handle mute
			process_mute( body['muted'] );
			
			//TODO: this should be in a muttonchop status function
			if (!body['audio-updating']) {
				if ( audio_update_button.attr('disabled') && last_audio_view_function =="audio_artists" ) {
					get_audio_artists();
				}
				audio_update_button.removeAttr('disabled');
				$("#audio_status").text("");
			} else {
				audio_update_button.attr('disabled',true);
				$("#audio_status").text("Updating audio database: "+body['last-audio-file-path']);
			}
			
			if ( first_player_status ) {
				first_player_status = false;
				switch (body["play-type"]) {
						case "PLAY_TYPE_VIDEO" :
							$("#tab_video").click();
						break;
						case "PLAY_TYPE_AUDIO" :
							$("#tab_audio").click();
						break;
						case "PLAY_TYPE_CAST" :
							$("#tab_casts").click();
						break;
						case "PLAY_TYPE_STREAM" :
							$("#tab_streams").click();
						break;
						default:
							$("#tab_casts").click();
				}
			}
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_player_status: "+type);
    } 
  });
}


/* set the tab content
 * var tab = id of tab content div as string
 * var content = element or string to be the new content
 */
function set_tab_data(tab, data) {
	var target = $(tab+" .data");
  target.empty();
  target.append(data);
}

function generate_audio_artist_view(data) {
	var view = $("<div class='artist_list'>");
	var artist_count = data.length;
	for (var i = 0 ; i < artist_count ; i++ ) {
		var artist = $("<div class='list_item'>");
		var artist_name = not_empty_value(data[i],"UNKNOWN");
		//handle clicks on the list_item
		artist.click( click_artist );
		artist.attr('name',data[i]);
		artist.html(artist_name);
		view.append(artist);    
	}
	return view;
}

function generate_streams_view(data) {
	var view = $("<div class='stream_list'>");
	var streams_count = data.length;
	for (var i = 0 ; i < streams_count ; i++ ) {
		var stream = data[i];
		var stream_text = "";
		var stream_div = $("<div class='list_item'>");
		if ( not_empty_value(stream['title']) ) {
			stream_text = stream['title'];
		} else {
			stream_text = stream['source'];
		}
		//handle clicks on the list_item
		stream_div.click( click_stream );
		stream_div.attr('stream_id',stream['id']);
		stream_div.html(stream_text);
		view.append(stream_div);    
	}
	return view;
}

function generate_audio_artist_tracks_view(data) {
	var view = $("<div class='artist_tracks_view'>");
	var back = $("<div class='list_item'>");
	back.html("<<<<");
	back.click(get_audio_artists);
	view.append(back);
	var artist = $("<div class='artist_name title'>");
	artist.html(data['artist']);
	view.append(artist);
	var track_count = data.files.length;
	for (var i = 0 ; i < track_count ; i++ ) {
		file = data.files[i];
		var track = $("<div class='list_item'>");

		//handle clicks on the list_item
		track.click( click_audio_track );
		track.attr('track_id',file['id']);
		//what text should we show?
		var album = not_empty_value( file['album'], "Unknown");
		var title = not_empty_value( file['title'], "Unknown");
		var track_text = album + " / " + title;
		if( album == "Unknown" && title == "Unknown" ) {
			track_text = file['path'];
		}
		
		track.html(track_text);
		view.append(track);    
	}
	return view;
}

function generate_audio_search_view(data) {
	var view = $("<div class='search_view'>");
	
	var back = $("<div class='list_item'>");
	back.html("<<<<");
	back.click(get_audio_artists);
	view.append(back);
	var search_string = $("<div class='search_string title'>");
	search_string.html(data.message);
	view.append(search_string);
	var track_count = data.files.length;
	for (var i = 0 ; i < track_count ; i++ ) {
		file = data.files[i];
		var track = $("<div class='list_item'>");

		//handle clicks on the list_item
		track.click( click_audio_track );
		track.attr('track_id',file['id']);
		//what text should we show?
		var track_text = file['artist']+" : "+file['album']+" / "+file['title'];
		track.html(track_text);
		view.append(track);    
	}
	return view;
}

function process_video_data(data) {
  //create a new directory view
  var dview = $("<div class='directory_view'>");
  var dir_separator = data['dir-separator'];
  var path = data['path'];
  //if there is a path, separate out the base dir
  var path_parts = rsplit(path, dir_separator);
  var base_dir_path = path_parts[0];
  var base_dir = (path_parts[1])?path_parts[1]:path;
  var dirs = path.split(dir_separator);
  //make a video refresh button
	var video_button = $('<span class="faux_button">');     
	video_button.attr('path','');
	//handle clicks on the list_item
	video_button.click( click_video_directory );
	video_button.html( 'Video' );
	var breadcrumbs = $("<div class='directory_dir'>");
	breadcrumbs.append( video_button );
	dview.append(breadcrumbs);
	// do we have path parts?
	if (dirs.length > 0 && dirs != "" ) {
		for ( var i=0; i< dirs.length; i++ ){
			var crumb_path = dirs.slice(0,i+1).join(dir_separator);
			var title = dirs[i];
			var crumb_button = $('<span class="faux_button">');     
			crumb_button.attr('path',crumb_path);
			//handle clicks on the list_item
			crumb_button.click( click_video_directory );
			crumb_button.html( title );
			breadcrumbs.append(crumb_button);
			
		}
	}
	
  //handle the directories
  var d = data['directories'];
  var dlen = d.length;
  if(d.length > 0 || path) {
    d.sort();
    var dt = $("<div class='title'>");
    dt.html("Directories:");
    dview.append(dt);
    //loop through the directories
    var directory_div = $("<div class='directories'>");
    //if there is a path, we need a .. link
    if (path) {
      var dd = $("<div class='list_item'>");
      dd.attr('path', base_dir_path);
      //handle clicks on the list_item
      dd.click( click_video_directory );
      dd.html('<<<<');
      directory_div.append(dd);
    }
    
    var dir_path = (path)? path+dir_separator:"";
    //loop through the directories
    for (var i = 0; i < dlen ; i++) {
      var dd = $("<div class='list_item'>");      
      dd.attr('path', dir_path+d[i]);
      //handle clicks on the list_item
      dd.click( click_video_directory );
      var title = d[i];
      dd.html( cleaner_filename( title ) );
      directory_div.append(dd);
    }
    dview.append(directory_div);
  }
  //handle the files
  var files = data['files'];
  var flen = files.length;
  var file_path="";
  //is there a directory path?
  if(path) {
		file_path+=path+dir_separator;
	}
  if(files.length > 0) {
    //sort
    files.sort();
    var ft = $("<div class='title'>");
    ft.html("Files:");
    dview.append(ft);
    //loop through the files
    var files_div = $("<div class='files'>");
    for (var i = 0; i < flen ; i++) {
      var fd = $("<div class='list_item'>");
      fd.attr('file_path', file_path+files[i]);
      //add the "cleaner" name as the divs text
      fd.html( cleaner_filename(files[i]));
      fd.click( click_video_file );
      files_div.append(fd);
    }
    dview.append(files_div);
  }
  $("#video_data").empty();
  $("#video_data").append(dview);
}

function change_subtitle_select() {
	if (ignore_subtitle_select_change ) {
		return;
	}
	var val = $("#subtitle_select").val();
	var url = "/player/subtitle/"+val;
	$.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :change_subtitle_select: "+type);
    } 
  });
}

function click_video_directory() {
  var path = $(this).attr('path');
	$(this).addClass('selected_item');
	get_video_content(path);
}

function click_video_file() {
	if (current_selected_video) {
		$(current_selected_video).removeClass('selected_item');
	}
	current_selected_video = this;
	$(current_selected_video).addClass('selected_item');
	//enable the playbutton
	video_play_button.removeAttr('disabled');        
}

function click_audio_track() {
	if (current_selected_audio) {
		$(current_selected_audio).removeClass('selected_item');
	}
	current_selected_audio = this;
	$(current_selected_audio).addClass('selected_item');
	//enable the playbutton
	audio_play_button.removeAttr('disabled');      
}

function click_stream() {
	if (current_selected_stream) {
		$(current_selected_stream).removeClass('selected_item');
	}
	current_selected_stream = this;
	$(current_selected_stream).addClass('selected_item');
	//enable the playbutton
	stream_play_button.removeAttr('disabled');      
	stream_edit_button.removeAttr('disabled');      
	stream_delete_button.removeAttr('disabled');      
}

function click_audio_play_button() {
	var track_id;
	var url = "/audio/play";
	if(current_selected_audio){
		track_id = $(current_selected_audio).attr('track_id');
		var url = "/audio/play/"+track_id;
	}
	
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { 
      //disable the audio play button
			audio_play_button.attr("disabled",true);
    }, 
    error: function(xhr, type) { 
      log("error :click_audio_play_button: "+type);
    } 
  });
}

function disable_audio_search() {
	audio_search_button.attr('disabled',true);
	audio_search_text.attr('disabled',true);
}

function enable_audio_search() {
	audio_search_button.attr('disabled',false);
	audio_search_text.attr('disabled',false);
}

function click_audio_search_button() {
	last_audio_view_function = "audio_search";
	//disable the search components
	disable_audio_search();
	var search_text = audio_search_text.val();
	var text = encodeURIComponent(search_text);
	var url = "/audio/search/"+text;
	
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { 
			var view = generate_audio_search_view( body );
      set_tab_data("#tab_audio_content", view);
      enable_audio_search();
    }, 
    error: function(xhr, type) { 
      log("error :click_audio_search_button: "+type);
      enable_audio_search();
    } 
  });
}

function click_audio_update_button() {
	//disable the audio play button
	audio_update_button.attr("disabled",true);
	var url = "/audio/update";
	//this could take a while, confirm with the user
	var result = confirm("This could take a while. Continue?");
	if( !result ) {
		return;
	}
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { 
			
    }, 
    error: function(xhr, type) { 
      log("error :click_audio_update_button: "+type);
      audio_update_button.attr("disabled",false);
    } 
  });
}

function click_audio_random_checkbox() {
	var checked = audio_random_checkbox.is(":checked");
	var url = "/audio/random/"+checked;
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { 
      //disable the audio play button
			audio_play_button.attr("disabled",true);
    }, 
    error: function(xhr, type) { 
      log("error :click_audio_random_checkbox: "+type);
    } 
  });
}

function click_video_play_button() {
  //what is the current
  var p = value_or_empty_string($(current_selected_video).attr('file_path'));
  //disable the play button
  video_play_button.attr('disabled',true);
  var url = "/video/play/"+p;
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anytdirpathing?
    }, 
    error: function(xdirpathr, type) { // type is a string ('error' for dirpathTTP errors, 'parsererror' for invalid JSON)
      log("error :click_video_play_button: "+type);
    } 
  });
}

function click_play() {
  var url = "/player/play";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_play: "+type);
    } 
  });
}
function click_pause() {
  var url = "/player/pause";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_pause: "+type);
    } 
  });
}

function click_mute() {
  var url = "/player/mute";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_mute: "+type);
    } 
  });
}
function click_unmute() {
  var url = "/player/unmute";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_unmute: "+type);
    } 
  });
}
function click_stop() {
  var url = "/player/stop";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_stop: "+type);
    } 
  });
}
function click_next() {
  var url = "/player/next";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_next: "+type);
    } 
  });
}
function click_previous() {
  var url = "/player/previous";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_previous: "+type);
    } 
  });
}

function click_forward() {
  var url = "/player/forward";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_forward: "+type);
    } 
  });
}

function click_reverse() {
  var url = "/player/reverse";
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_reverse: "+type);
    } 
  });
}

/* stream click handlers */
function click_stream_play_button() {
	var id = $(current_selected_stream).attr("stream_id");
	var url = "/stream/play/"+id;
	stream_play_button.attr("disabled", true);
	$.ajax({
			url: url,
			dataType: 'text', 
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

			}, 
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :play stream: "+type);
			} 
		});
	
}

function click_stream_delete_button() {
	var r = confirm("Really delete?");
	if (r == true) {
		disable_stream_buttons();
		var id = $(current_selected_stream).attr("stream_id");
		var url = "/stream/delete/"+id;
		$.ajax({
			url: url,
			dataType: 'text', 
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//refresh the streams list
				get_stream_list();
			}, 
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :delete stream: "+type);
			} 
		});
	} 
	stream_add_button.attr('disabled', false);
}

function click_stream_update_button() {
	stream_update_button.attr('disabled', true);
	stream_cancel_button.attr('disabled', true);
	//get some data
	var source = $("#stream_source").val();
	var title = $("#stream_title").val();
	var id = $(current_selected_stream).attr("stream_id");
	//form a URL
	var url = "/stream/edit/"+encodeURIComponent(id);
	url += "?title="+encodeURIComponent(title);
	url += "&source="+encodeURIComponent(source);
	$.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //refresh the streams list
      get_stream_list();
      //hide stuff
      $("#stream_blocker").hide();
      $("#stream_add_edit").hide();
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :update stream: "+type);
      //hide stuff
      $("#stream_blocker").hide();
      $("#stream_add_edit").hide();
    } 
  });
	stream_add_button.attr('disabled', false);
}

function click_stream_add_button() {
	//show and hide some buttons
	stream_create_button.show();
	stream_update_button.hide();
	//enable buttons
	stream_create_button.attr('disabled', false);
	stream_cancel_button.attr('disabled', false);
	//show the form and blocker
	$("#stream_add_edit .title").text("Add Stream");
	$("#stream_add_edit").show();
	$("#stream_blocker").show();
	disable_stream_buttons();
	
}

function disable_stream_buttons() {
	stream_add_button.attr('disabled', true);
	stream_edit_button.attr('disabled', true);
	stream_delete_button.attr('disabled', true);
	stream_play_button.attr('disabled', true);
}

function click_stream_edit_button() {
	//disable buttons
	 disable_stream_buttons();
	//get the id of the current_selected_stream
	var id = $(current_selected_stream).attr("stream_id");
	url = "/stream/info/"+id;
		$.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			$("#stream_title").val( body['title'] );
			$("#stream_source").val( body['source'] );
			stream_update_button.show();
			stream_create_button.hide();
			//enable buttons
			stream_update_button.attr('disabled', false);
			stream_cancel_button.attr('disabled', false);
			//show the form and blocker
			$("#stream_add_edit .title").text("Edit Stream");
			$("#stream_add_edit").show();
			$("#stream_blocker").show();

    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :stream edit: "+type);

    } 
  });
	
	
}

function click_stream_create_button() {
	stream_create_button.attr('disabled', true);
	stream_cancel_button.attr('disabled', true);
	//get some data
	var source = $("#stream_source").val();
	var title = $("#stream_title").val();
	//form a URL
	var url = "/stream/add/"+encodeURIComponent(source)+"?title="+encodeURIComponent(title);
	$.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //refresh the streams list
      get_stream_list();
      //hide stuff
      $("#stream_blocker").hide();
      $("#stream_add_edit").hide();
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :add stream: "+type);
      //hide stuff
      $("#stream_blocker").hide();
      $("#stream_add_edit").hide();
    } 
  });
  stream_add_button.attr('disabled', false);
}

function click_stream_cancel_button() {
	$("#stream_source").val("");
	$("#stream_title").val("");
	$("#stream_add_edit").hide();
	$("#stream_blocker").hide();
	stream_add_button.attr('disabled', false);
}

function get_stream_list() {
	var url = "/stream/list";
	$.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //process the json
      var view = generate_streams_view(body['streams']);
			set_tab_data('#tab_streams_content',view);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_previous: "+type);
    } 
  });
}


function create_tabs() {
  tab_clickables = $("#tabs > ul > li");
  tab_contents = new Array();
  for (var i = 0; i < tab_clickables.length; i++) {
    var tab = $(tab_clickables[i]);
    //what happens when a tab is clicked?
    tab.bind('click', click_tab );
    var id = tab.attr('id');
    var tab_content = $('#'+id+"_content");
    tab_content.addClass('tab_content');
    tab_contents[i] = tab_content;
    //get the associated tab content
    tab.addClass('tab');
    if (i==0) {
      tab.addClass('selected_tab');
      tab_content.addClass('selected_tab_content');
      current_selected_tab = tab;
      current_selected_tab_content = tab_content;
    }
  }
}

function connect_buttons() {
  video_play_button = $("#video_play_button");
  video_play_button.click( click_video_play_button );
	audio_play_button = $("#audio_play_button");
	audio_play_button.click( click_audio_play_button );
	audio_update_button = $("#audio_update_button");
	audio_update_button.click( click_audio_update_button );
	audio_random_checkbox = $("#audio_random_checkbox");
	audio_random_checkbox.click( click_audio_random_checkbox );
	audio_search_button = $("#audio_search_button");
	audio_search_text = $("#audio_search_text");
	audio_search_text.keydown( function(e){
		var key = e.which;
		if (key == 13) {
			click_audio_search_button();
		}
	});
	audio_search_button.click( click_audio_search_button );
	//disable some buttons
	audio_play_button.attr('disabled',true);
	audio_update_button.attr('disabled',true);
	video_play_button.attr('disabled',true);
	//set up the stream buttons
	stream_play_button = $("#stream_play_button");
  stream_play_button.click( click_stream_play_button );
  stream_play_button.attr('disabled',true);
	stream_edit_button = $("#stream_edit_button");
  stream_edit_button.click( click_stream_edit_button );
  stream_edit_button.attr('disabled',true);
	stream_delete_button = $("#stream_delete_button");
  stream_delete_button.click( click_stream_delete_button );
  stream_delete_button.attr('disabled',true);
	stream_add_button = $("#stream_add_button");
  stream_add_button.click( click_stream_add_button );
  
  stream_create_button = $("#stream_create_button");
  stream_cancel_button = $("#stream_cancel_button");
  stream_update_button = $("#stream_update_button");
  stream_create_button.click( click_stream_create_button );
  stream_cancel_button.click( click_stream_cancel_button );
  stream_update_button.click( click_stream_update_button );
  
}

function setup_player() {
  $("#volume_background").bind('click', click_volume );
  $("#duration_background").bind('click', click_duration );
  $("#play").bind('click', click_play );
  $("#pause").bind('click', click_pause );
  $("#stop").bind('click', click_stop );
  $("#previous").bind('click', click_previous );
  $("#next").bind('click', click_next );
  $("#forward").bind('click', click_forward );
  $("#reverse").bind('click', click_reverse );
  $("#subtitle_select").bind('change', change_subtitle_select);
  $("#mute").bind('click', click_mute);
  $("#unmute").bind('click', click_unmute);
  
}

function set_audio_view( body, type ){
	var view;
	switch(type) {
		case "artists":
		view = generate_audio_artist_view(body);
		break;
	}
	//set the tab content
  set_tab_data('#tab_audio_content',view);
}

function click_artist() {
	last_audio_view_function = "artist";
	current_selected_audio=undefined;
	var artist = $(this);
	var name = artist.text();
	var url = "/audio/artist_tracks/"+name;
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      var view = generate_audio_artist_tracks_view( body );
      set_tab_data("#tab_audio_content", view);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_artist: "+type);
    } 
  });
}

function click_tab() {
  //what was clicked?
  var tab = $(this);
  var id = tab.attr('id');
  if(id =="tab_casts") {
		//start checking the downloads
		continue_get_download_status = true;
		timer_get_download_status();
	} else {
		//stop checking the download status
		continue_get_download_status = false;
	}
  //is this tab the current one?
  if (id == $(current_selected_tab).attr('id') ) {
    return;
  }
  //remove some styles
  $(current_selected_tab).removeClass('selected_tab');
  $(current_selected_tab_content).removeClass('selected_tab_content');
  
  //handle the new selection
  current_selected_tab = tab;
  current_selected_tab_content = $('#'+id+"_content");
  
  current_selected_tab.addClass('selected_tab');
  current_selected_tab_content.addClass('selected_tab_content');  
}

function timer_get_player_status() {
	get_player_status();
	setTimeout("timer_get_player_status()", player_timer_speed);
}

function click_volume(event) {
	//get the position of the event
	var clientX = event.clientX;
	var left = event.currentTarget.offsetLeft;
	var clickoffset = clientX - left;
	var new_volume = Math.ceil(clickoffset/event.currentTarget.offsetWidth*100);
  var url = "/player/volume/"+new_volume;
   $.ajax({
    url: url,
    dataType: 'json', 
    success: function(vol) { // body is a string (or if dataType is 'json', a parsed JSON object)
			update_volume( vol );
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_volume: "+type);
    } 
  });
}

function update_volume( vol ) {
	updating_volume = true;
	if (current_volume != vol ) {
		$("#volume_bar").width( vol+'%');
		$("#volume_value").text(vol);
		current_volume = vol;
	}
	updating_volume = false;
}

function update_duration(position, duration) {
  var percent = 0;
  if ( duration ) {
    percent = (position/duration)*100;
    var text = cleaner_time(position) +" / "+cleaner_time(duration);
    $("#duration_bar").html( text );
    $("#duration_background_text").html( text );
  }
  $("#duration_bar").width(percent+'%');
}

function click_duration(event) {
	//get the position of the event
	var clientX = event.clientX;
	var left = event.currentTarget.offsetLeft;
	var clickoffset = clientX - left;
	var new_position = (clickoffset/event.currentTarget.offsetWidth)*100;
  var url = "/player/percent/"+new_position;
   $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      update_duration(body['position'], body['duration']);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_duration: "+type);
    } 
  });
}

/* some cast specific stuff */

function setup_casts(){	
	//hide the episodes
	//$("#episode_block").hide();
	//attach some button actions
	$("#add_feed_button").click( show_add_feed_div );
	$("#update_feeds_button").click( clicked_update_feeds_button );
	$("#update_feed_button").click( clicked_update_feed_button );
	$("#delete_feed_button").click( clicked_delete_feed_button );
	$("#submit_feed_button").click( clicked_submit_feed_button );
	$("#cancel_feed_button").click( hide_add_feed_div );
	$("#cancel_available_episodes_button").click( clicked_cancel_available_episodes_button );
	$("#download_available_episodes_button").click(clicked_download_available_episodes_button);
	$("#feeds_button").click( show_feeds );
	$("#play_episode_button").click( clicked_play_episode_button );
	$("#download_episode_button").click( clicked_download_episode_button );
	//disable some stuff
	$("#update_feeds_button").attr('disabled', true);
	$("#update_feed_button").attr('disabled', true);
	$("#delete_feed_button").attr('disabled', true);

	reset_episode_buttons();

	//hide some stuff
	hide_add_feed_div();
	hide_available_episodes_div();
	//get the feeds
	get_feeds();
}

function reset_episode_buttons() {
	$("#play_episode_button").attr('disabled', true);
	$("#download_episode_button").attr('disabled', true);
}

function timer_get_download_status() {
	get_download_status();
	if ( continue_get_download_status ) {
		setTimeout("timer_get_download_status()", download_status_speed);
	}
}

function clicked_kill() {
	//get the id of the clicked kill
	var id = $(this).attr("click_id");
	//get rid of the download div
	$(".download").each( function() {
		var click_id = $(this).attr('click_id');
		if ( click_id == id) {
			//get rid of this div
			$(this).remove();
		}
	});
	//ajax query to kill the dl
	var url = "/catcher/kill/"+id;
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do nothing
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :clicked_kill: "+type);
    } 
  });
}

function get_feeds() {
	var url = "/catcher/feeds";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			len = body.length;
			if(len > 0 ) {			
				//clear any feeds that may exist
				clear_feeds();
				$("#update_feeds_button").removeAttr('disabled');
				//create and add the 'unplayed'
				var unplayed = {'id':0, 'title':'Unplayed'}
				//connect the unplayed
				connect_feed( unplayed );
				for(var i=0; i<len; i++) {
					feed = body[i];
					connect_feed( feed );
				}
			}
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_feeds: "+type);
    } 
  });
}

function mark_played( id ) {
	//what target do we need to change?
	var target_ref = "#episode_"+id+" .played";
	var element = $(target_ref);
	element.html("*");
}

function mark_downloaded( id ) {
	//what target do we need to change?
	var target = $("#episode_"+id+" .downloaded");
	target.html("*");
}

function get_download_status() {
	var url = "/catcher/download_status";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			len = body['downloads'].length;
			var good_ids = new Array();
			if(len > 0 ) {			
				for(var i=0; i<len; i++) {
					dl = body['downloads'][i];
					var id = dl["id"];
					good_ids.push ( id );
					update_download( dl );
				}
				//show the downloads div
				$("#download_elements").show();
			} else {
				$("#download_elements").hide();
			}
			//remove downloads that don't have a status
			$(".download_info").each( function() {
				var click_id =  $(this).attr('click_id');
				if($.inArray(click_id, good_ids)==-1 ){
					//get rid of this div
					$(this).remove();
					//mark as downloaded
					mark_downloaded( click_id );
					//do we need to show the play episode button?
					if(click_id == selected_episode_id) {
						$("#play_episode_button").attr("disabled",false);
					}
				}
			});
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_download_status: "+type);
    } 
	});
}

function get_episode(id) {
	var url = "/catcher/episode/"+id;
	$.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			if( body ) {			
				//clear any feeds that may exist
				reset_episode_buttons();
				// determine if this episode exists locally
				if (body['downloaded'] && body['exists']) {
					//enable the play button
					$("#play_episode_button").attr('disabled',false);
				} 
				else {
					//check if the episode is currently being downloaded
					var did = "download_"+id;
					if ( $("#"+did).length ){
						//the episode is being downloaded

					}
					else{
						$("#download_episode_button").attr('disabled',false);
					}
				}
			}
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_episode: "+type);
    } 
  });
}

function get_episodes(id) {
	var url = "/catcher/episodes/"+id;
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			len = body.length;
			//clear any feeds that may exist
				clear_episodes();
			if(len > 0 ) {			
				
				for(var i=0; i<len; i++) {
					episode = body[i];
					connect_episode( episode );
				}
				//show the episodes
				//$("#episode_block").show();
				//$("#feed_block").hide();
			}
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get_episodes: "+type);
    } 
  });
}
			
function hide_add_feed_div() {
	$("#add_feed_div").hide();
	$("#casts_blocker").hide();
}

function hide_available_episodes_div() {
	$("#available_episodes_div").hide();
	$("#casts_blocker").hide();
}

function show_feeds() {
	//$("#episode_block").hide();
	//$("#feed_block").show();
}


function show_add_feed_div() {
	$("#add_feed_div").show();
	$("#casts_blocker").show();
	$("#add_feed_input").val('');
	$("#add_feed_input").attr('disabled', false);
	$("#submit_feed_button").attr('disabled', false);
	$("#cancel_feed_button").attr('disabled', false);
	$("#add_feed_input").focus();
}
			
function show_available_episodes_div( episodes ) {
	$(".available_episode_tr").remove();
	ep_len = episodes.length;
	if (ep_len==0) {
		alert("No new episodes");
	}
	else {
		for(i=0; i< ep_len; i++) {
			var	episode = episodes[i];
			var id = episode['id'];
			var title = episode['title'];
			var date = date_from_pubdate( episode['pubdate'] );
			var cb = $("<input type='checkbox' class='available_episode_checkbox' name='"+id+"' id='cb_"+id+"' value=1>");
			var tr = $("<tr class='available_episode_tr'>");
			var td = $("<td>");
			td.append( cb );
			tr.append( td );
			tr.append("<td><label for='cb_"+id+"'>"+title+"</label></td>");
			tr.append("<td><label for='cb_"+id+"'>"+date+"</label></td>");
			
			$("#available_episodes_table").append(tr);
		}
		$("#available_episodes_div").show();
		$("#casts_blocker").show();
	}
}
			
function clicked_update_feeds_button() {
	$("#update_feeds_button").attr('disabled', true);
	var url = "/catcher/update/all";
	$.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      process_update_feed_data(body);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :clicked_update_feeds_button: "+type);
    } 
  });
}
function clicked_update_feed_button() {
	$("#update_feed_button").attr('disabled', true);
		var url = "/catcher/update/"+selected_feed_id;
	$.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      process_update_feed_data(body);
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :clicked_update_feed_button: "+type);
    } 
  });
}

function clicked_delete_feed_button() {
	var r = confirm("Really delete "+selected_feed_name+"?");
	if (r == true) {
		$("#delete_feed_button").attr('disabled', true);
		var id = selected_feed_id;
		var url = "/catcher/delete/"+id;
		//do a jquery to remove the feed
		$.ajax({
			url: url,
			dataType: 'text', 
			success: function(body) {
				
				//we should reset the feeds list
				get_feeds();
				clear_episodes();
			},
			error: function() {
				//what should be done on error?
			}
		});
	} 
}

function clicked_download_available_episodes_button() {
	$("#available_episodes_div").hide();
	$("#casts_blocker").hide();
	var dls = new Array();	
	$(".available_episode_checkbox:checked").each( function(){
		var name = $(this).attr('name');
		dls.push( name );
	});
	id_string = dls.join(":");
	//make a list of the available episodes checkboxes
	//collect the IDs of the checked boxes
	//send AJAXy data so shit can get downloaded
	var url = "/catcher/download/"+encodeURIComponent(id_string);
	$.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :clicked_download_available_episodes_button: "+type);
    } 
  });
}

function clicked_submit_feed_button() {
	
	var feed_url = $.trim ($("#add_feed_input").val() );
	if( feed_url!="" )
	{
		$("#submit_feed_button").attr('disabled', 'disabled');
		$("#cancel_feed_button").attr('disabled', 'disabled');
		$("#add_feed_input").attr('disabled', 'disabled');
		var url = "/catcher/add/"+encodeURIComponent( feed_url );
		$.ajax({
			url: url,
			dataType: 'json', 
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//what should we do?
				hide_add_feed_div();
				process_add_feed_data(body);
			}, 
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :clicked_submit_feed_button: "+type);
				hide_add_feed_div();
			} 
		});
	}
}

function clicked_cancel_available_episodes_button() {
	$("#available_episodes_div").hide();
	$("#casts_blocker").hide();
}

function clicked_play_episode_button() {
		//disable the download button
	$("#play_episode_button").attr('disabled', true);
	var id = selected_episode_id;
	playing_episode_id = id;
	var url = "/catcher/play/"+id;
	$.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :clicked_play_episode_button: "+type);
    } 
  });
}

function clicked_download_episode_button() {
	//disable the download button
	$("#download_episode_button").attr('disabled', true);
	var id = selected_episode_id;
	
	var url = "/catcher/download/"+id;
	$.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :clicked_download_episode_button: "+type);
    } 
  });
}

function process_update_feed_data(data) { 
	$("#update_feeds_button").removeAttr('disabled'); 
	//are there episodes?
	if( data['episodes'].length > 0) {
		show_available_episodes_div( data['episodes'] );
	} else {
		alert( "No New Episodes" );
	}
	if (data['error'] ) {
		alert( data['error'] );
	}
}

function process_subtitle_data(count, current_selected) {
	ignore_subtitle_select_change = true;
	if (count==0 || count==undefined) {
		//hide the subtitle control block
		$("#subtitle_control_block").hide();
	} else {
		$("#subtitle_select").empty();
		//clear the subtitle select
		
		//add the subtitle options
		selected = (current_selected==-1) ? "selected" : "";
		$("#subtitle_select").append( $("<option value='-1' "+selected+">None</option>") );
		// make sure the current_selected is selected
		for (var i=0; i < count; i++) {
			//add the subtitle options
			selected = (current_selected==i) ? "selected" : "";
			$("#subtitle_select").append( $("<option value='"+i+"' "+selected+">"+i+"</option>") );
		}
		//show the subtitle control block
		$("#subtitle_control_block").show();
	}
	ignore_subtitle_select_change = false;
}

function process_mute( muted ) {
	if (muted) {
		//show unmute
		$("#unmute").show();
		//hide mute
		$("#mute").hide();
	} else {
		//show mute
		$("#mute").show();
		//hide unmute
		$("#unmute").hide();
	}
}

function process_add_feed_data(data) {

	if (data['episodes'].length > 0 ) {
		//the feeds were updated, show the new feeds
		get_feeds();
		//show the available episodes
		show_available_episodes_div( data['episodes'] );
	}else{
		alert("Could not process the RSS feed");
	}
}

function clear_feeds() {
	$(".feed_block").remove();
}
			
function connect_feed(feed) {
	//start making the feed object
	var id = feed['id'];
	var feed_div = $("<div>");
	feed_div.attr("id", "feed_"+id);
	feed_div.attr("click_id", id);
	feed_div.addClass("feed_block");
	feed_div.addClass("list_item");
	//create the title 
	div_title = $("<div class='feed_title'>");
	div_title.html(feed['title']);
	//div_description = $("<div class='feed_description'>");
	//div_description.html(feed['description']);
	feed_div.append(div_title);
	//feed_div.append(div_description);
	//feed_div might get clicked
	feed_div.click( function(){
		var id = $(this).attr("click_id");
		select_feed( id );
	});
	//append the feed div
	$("#feeds_holder").append(feed_div);
}
		
function select_feed(id) {
	//disable the episode buttons
	reset_episode_buttons();
	if (selected_feed_id != null) {
		$("#feed_"+selected_feed_id).removeClass("selected_item");
	}
	$("#feed_"+id).addClass("selected_item");
	selected_feed_name = $("#feed_"+id+" .feed_title").html();
	selected_feed_id = id;
	//is this an actual feed?
	if(id>0) {
		$("#update_feed_button").removeAttr('disabled');
		$("#delete_feed_button").removeAttr('disabled');
	} else {
		$("#update_feed_button").attr('disabled', true);
		$("#delete_feed_button").attr('disabled', true);
	}
	//update the title of the episodes list
	$("#episode_list .title").html( selected_feed_name+" Episodes");
	//get the episodes for this feed
	get_episodes( id );
}
function select_episode(id) {
	//get the episode data
	get_episode( id );
	//highlight some div
	if (selected_episode_id!=null) {
		$("#episode_"+selected_episode_id).removeClass("selected_item");
	}
	$("#episode_"+id).addClass("selected_item");
	selected_episode_id = id;
}
						
function clear_episodes() {
	$(".episode_block").remove();
}

function reset_player_buttons() {
	$("#play_button").attr('disabled', true);
	$("#pause_button").attr('disabled', true);
}

function connect_episode(e) {
	//start making the episode object
	var id = e['id'];
	var episode = $("<tr>");
	episode.attr("id", "episode_"+id);
	episode.attr("click_id", id);
	episode.addClass("episode_block");
	episode.addClass("list_item");
	//create the title 
	var div_title = $("<div class='episode_title'>");
	div_title.html(e['title']);
	var td = $("<td>");
	td.append(div_title);
	//div_description = $("<div class='episode_description'>");
	//div_description.html(e['description']);
	episode.append(td);
	var date = $("<td class='align_right'>");
	date.html( date_from_pubdate(e['pubdate'])	);
	episode.append(date);
	
	var downloaded = $("<td class='downloaded'>");
	var downloaded_string = (e['downloaded']==1)?"*":"";
	downloaded.html( downloaded_string );
	episode.append(downloaded);
	
	var played = $("<td class='played'>");
	var played_string = (e['played']==1)?"*":"";
	played.html( played_string );
	episode.append(played);
	
	
	//episode.append(div_description);
	//episode might get clicked
	episode.click( function(){
		var id = $(this).attr("click_id");
		select_episode( id );
	});
	//append the episode div
	$("#episodes_holder").append(episode);
}

function date_from_pubdate( pubdate ) {
	var date_parts = pubdate.split(" ");
	return date_parts[0];
}

function update_download( dl ) {
	//what is the percent?
	var p = dl['percent'];
	if ( p > 100 ) {
		p=0;
	}
	var id = dl['id'];
	var did = "download_"+id;
	var didp = did+"_progress"
	var didpw = didp+"_wrapper"
	//does this exist?
	if( $("#"+did).length==0) {
		//create the download div
		var dl_div = $("<div class='download_info'>");
		dl_div.attr("id", did);

		dl_div.attr("click_id", id);
		var title = $("<div class='download_title'>");
		title.html( dl['feed-title']+": "+dl['title'] );
		var progw = $("<div class='download_progress_wrapper'>");
		var prog = $("<div id='"+didp+"' class='download_progress'>");
		prog.width(p+'%');
		progw.append(prog);
		dl_div.append( title );
		dl_div.append( progw ); 
		var kill_dl_div = $("<button class='kill_download'>");
		kill_dl_div.html("kill");
		kill_dl_div.attr("click_id", id);
		kill_dl_div.click( clicked_kill );
		
		dl_div.append(kill_dl_div);
		
		var dl_holder = $("#download_holder");
		dl_holder.append( dl_div );
	}
	else{
		$("#"+didp).width(p+"%");
	}
}





function log( string ) {
	html = $("#log").html();
	d = new Date();
	html+= d.toTimeString() +": "+ string + "\n";
	$("#log").html( html);
}

function clear_log() {
	
}

