//global variables

selected_queue_id = null;
current_selected_queue = null;

$(function() {
	//set up the queue queue buttons
  queue_delete_button = $("#queue_delete_button");
  button_disable(queue_delete_button);
  queue_delete_button.click( click_queue_delete_button );
  //set up the audio queue button
  audio_queue_button = $("#audio_queue_button");
  button_disable(audio_queue_button);
  audio_queue_button.click( click_audio_queue_button );
  //set up the video queue button
  video_queue_button = $("#video_queue_button");
  button_disable(video_queue_button);
  video_queue_button.click( click_video_queue_button );
  //set up the cast queue button
  episode_queue_button = $("#casts_queue_episode_button");
  button_disable(episode_queue_button);
  episode_queue_button.click( click_episode_queue_button );
});

function get_queue_list() {
	var url = "/queue/list";
	$.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //process the json
			process_queue_data(body);
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :get/queue/list: "+type);
    }
  });
}

function process_queue_data(queue_items) {
	$("#queue_list").empty();
	var queue_count = queue_items.length;
	for (var i = 0 ; i < queue_count ; i++ ) {
		var item = queue_items[i];
		queue_list_add(item);
	}
}

function queue_list_add(item) {
	var item_text = "";
	var sli = $("<div class='faux_button'>");
	if ( not_empty_value(item['title']) && not_empty_value(item['artist']) ) {
		item_text = item['artist']+" : "+item['title'];
	} else if(item['play_type']=='cast') {
		item_text = item['feed']+" : "+item['title'];
	} else {
		item_text = item['play_type']+" : "+item['reference'];
	}
	//handle clicks on the list_item
	sli.click( click_queue_item );
	sli.attr('queue_id',item['id']);
	sli.html(item_text);
	$("#queue_list").append(sli);

}

function click_queue_delete_button() {
		var r = confirm("Really delete this queued item?");
	if (r == true) {
		var url = "/queue/delete/"+selected_queue_id;
		$.ajax({
			url: url,
			dataType: 'json',
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//process the json
				remove_queue_id(selected_queue_id);
				//disable the delete button
				button_disable(queue_delete_button);
			},
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :get/queue/list: "+type);
			}
		});
	}
}

function click_audio_queue_button() {
	//what is the currently_selected_audio item?
	var reference = $(current_selected_audio).attr("track_id");
	if (reference) {
		var txt = $(current_selected_audio).text();
		var type = 'audio'
		var r = confirm("Add "+txt+" to the queue?");
		if (r == true) {
			var url = "/queue/add/"+type+"/"+reference;
			do_get_request(url);
		}
	}
}

function click_video_queue_button() {
	//what is the currently_selected_video item?
	var reference = $(current_selected_video).attr("file_path");
	if (reference) {
		var txt = $(current_selected_video).text();
		var type = 'video'
		var r = confirm("Add "+txt+" to the queue?");
		if (r == true) {
			var url = "/queue/add/"+type+"/"+reference;
			do_get_request(url);
		}
	}
}

function click_episode_queue_button() {
	//what is the currently_selected_video item?
	var reference = current_selected_episode_id;
	if (reference) {
		var txt = $(current_selected_episode).find(" > .episode_title")[0].innerHTML;
		var type = 'cast'
		var r = confirm("Add "+txt+" to the queue?");
		if (r == true) {
			var url = "/queue/add/"+type+"/"+reference;
			do_get_request(url);
		}
	}
}

//
function do_get_request(url){
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :"+url);
		}
	});
}

function remove_queue_id(id) {
	$("#queue_list [queue_id='"+id+"']").remove();
}

function click_queue_item() {
	var item = this;
	selected_queue_id = $(item).attr("queue_id");
	if (current_selected_queue) {
		button_deselect(current_selected_queue);
	}
	current_selected_queue = item;
	button_select(current_selected_queue);
	//enable the playbutton
	button_enable(queue_delete_button);
}

function add_queued( item_string ) {
	var item = JSON.parse( item_string );
	queue_list_add(item);
}
