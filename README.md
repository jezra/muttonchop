# MuttonChop

MuttonChop is a media player designed to be controlled through a web interface or by a JSON API (the web interface uses the JSON API). Technically, MuttonChop is two separate applications; a daemon like player/server and a default HTML/JavaScript/CSS interface.

## Compiling
MuttonChop is written in [Vala](http://live.gnome.org/Vala) and requires valac to compile.  
To compile, run the following:   
make build

To Compile headless, run the following:  
make build-no-vid

## Dependencies
* gtk3 //not needed when compiling headless
* glib
* json-glib
* gio
* sqlite3
* gstreamer-0.10
* gee
* libxml-2.0

## Configuration
MuttonChop will look for a configuration file named "muttonchop.conf" in one of two places:

1. The current directory of the user launching MuttonChop
2. In a directory named ".config/muttonchop" in the current user's home directory 

The configuration file is a simple text file in [key][whitespace][value] format with few options. The options are:

* video_dir - the path to a directory containing video files
* audio_dir - the path to a directory containing audio files
* web_server_public_dir - where the WebUI files are located
* cast_dir - where to download caught casts
* audio_auto_play - boolean [true, false] if audio should auto play on startup
* audio_play_random - boolean [true, false] play randomly selected audio
* audio_continuous_play - boolean [true, false]... um, I don't know what this does or was supposed to do
* video_disable_subtitles - boolean [true, false] if subtitles should be disabled when a video with subtitles is started
* start_up_volume - integer value between 0 and 100, if excluded, defaults to 50
* days_to_save_played_casts - integer value representing the number of days until played casts are deleted
* server_port - the port for MuttonChop to use for server data. defaults to 2876
* server_request_timeout - the timeout limit for a server request. defaults to 5

An example config file might look like this:

    video_dir /storage/video
    audio_dir /storage/music
    web_server_public_dir /storage/projects/muttonchop/public
    cast_dir /storage/casts
    audio_auto_play false
    audio_play_random true
    audio_continuous_play true
    start_up_volume 70
    days_to_save_played_casts 7
    server_port 2877
    server_request_timeout 15


## Basic Usage
Run MuttonChop on a computer.  Direct a web browser to port 2876 on the machine running MuttonChop.   
**NOTE:** on first run, MuttonChop will create a directory named ".muttonchop" in the current user's home directory, and create an sqlite3 database in the new directory. MuttonChop will then try to read the tags of audio files in the audio directory.
 

## the API
The API is still in an experimental phase and it will most certainly change in the future. (it has been a decade since the last api change :) jezra 

### Video
#### Get video directory contents
HOST:PORT/video/content
return value: a JSON representation of directory info with some additional information  
similarly, to get the content of a directory within the video directory:    
HOST:PORT/video/content/URLENCODED_PATH_OF_DIRECTORY

#### Play a video
HOST:PORT/video/play/URLENCODED_PATH_TO_FILE

### Audio
#### List Artists
HOST:PORT/audio/artist
returns a JSON array of strings

#### List an Artist's tunes
HOST:PORT/audio/artist_tracks/URLENCODED_ARTIST_NAME   
returns an array of track information from the database

#### Search artist,album, track name for a string
HOST:PORT/audio/search/URLENCODED_STRING

#### Display information for a track based on the track id in the database
HOST:PORT/audio/track/TRACK_ID

#### Play Audio
HOST:PORT/audio/play    
to play a specific track   
HOST:PORT/audio/play/TRACK_ID

#### Set random to true or false
HOST:PORT/audio/random/true    
or    
HOST:PORT/audio/random/false
or
HOST:PORT/audio/random/toggle to toggle the randomness 

#### Get the state of the audo randomness as true or false
HOST:PORT/audio/random 

#### Update the database
HOST:PORT/audio/update

### Control the playing audio or video
#### Get the status of the player
HOST:PORT/player/status    
returns basic information about the player (this will definitely be changing)  

#### open a stream to get the Server Sent Events
HOST:PORT/events  
NOTE: this will return data as a Server Sent Event. see <http://dev.w3.org/html5/eventsource/>

#### jump the playback to a percentile location in the time of the file
HOST:PORT/player/percent/PERCENT_AMOUNT

#### set the playback volume
HOST:PORT/player/volume/NUMBER_BETWEEN_0_AND_100

#### change the relative playback volume with a set increment
HOST:PORT/player/volume_change/POSITIVE_0R_NEGATIVE_NUMBER_BETWEEN_0_AND_100
Example to Change the volume down by 7:  HOST:PORT/player/volume_change/-7

#### basic playback commands
HOST:PORT/player/play    
HOST:PORT/player/pause   
HOST:PORT/player/stop   
HOST:PORT/player/next   
HOST:PORT/player/previous   
HOST:PORT/player/forward : just forward 1%    
HOST:PORT/player/reverse : jump back 1%   

### Catcher
#### add a new feed 
HOST:PORT/catcher/add/URLENCODED_FEED_URL

#### get a list of feeds
HOST:PORT/catcher/feeds

#### update all feeds
HOST:PORT/catcher/update/all : this will return a list of all new episodes of all feeds

#### update a single feed
HOST:PORT/catcher/update/FEED_ID

#### get a list of a feeds episodes
HOST:PORT/catcher/episodes/FEED_ID

#### get info about a specific episode
HOST:PORT/catcher/episode/EPISODE_ID

#### play a specific episode
HOST:PORT/catcher/play/EPISODE_ID

#### download episodes
HOST:PORT/catcher/download/EPISODE_ID1:EPISODE_ID2:EPISODE_ID3

#### get information about active download
HOST:PORT/catcher/download_status

#### delete a feed
HOST:PORT/catcher/delete/FEED_ID

### Streams
#### add a new stream
HOST:PORT/stream/add/URLENCODED_SOURCE_OF_STREAM?title=OPTIONAL_TITLE_NAME_OF_STREAM  
returns the ID of the added stream

#### list all streams
HOST:PORT/stream/list

#### view info about one stream
HOST:PORT/stream/info/STREAM_ID

#### play a stream
HOST:PORT/stream/play/STREAM_ID or
HOST:PORT/stream/play/STREAM_SOURCE_URL

#### edit a stream 
HOST:PORT/stream/edit/STREAM_ID?title=NEW_TITLE&source=NEW_SOURCE

#### delete a stream
HOST:PORT/stream/delete/STREAM_ID  
NOTE: it is up to the client to verify if the user really want to delete

### Queue
#### add an item to the queue
HOST:PORT/queue/add/TYPE/IDENTIFIER  
TYPE can be one of "audio", "video", "cast", or "stream"  
IDENTIFIER is the reference the item to be played  
If TYPE is "audio", IDENTIFIER is the audio file's database id  
If TYPE is "video", IDENTIFIER is the video file's path  
If TYPE is "cast", IDENTIFIER is the episode's database id  
If TYPE is "stream", IDENTIFIER is the stream's database id or the URL of the stream
 
#### remove an item from teh queue
HOST:PORT/queue/delete/QUEUE_ID

